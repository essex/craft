#include "cpHelperFuncs.hpp"
#include "craftDebugFuncs.hpp"

#include <string>
#include "craftConf.h"
using namespace cpHelperFuncs;

int craftDebug                      = CRAFT_DEBUG;
int craftDebugLog                   = CRAFT_DEBUG_LOG;
int craftDebugProc                  = CRAFT_DEBUG_PROC;
int craftReadCpOnRestart            = CRAFT_READ_CP_ON_RESTART;
std::string craftCpPath             = CRAFT_CP_PATH;
int craftEnabled                    = CRAFT_ENABLE;
int craftUseSCR                     = CRAFT_USE_SCR;                  // TODO: can be removed perhaps? now the CRAFT_SCR_CP_NAME already covers this function.
int craftTiming                     = CRAFT_TIMING;
int craftWriteAsync                 = CRAFT_WRITE_ASYNC;              // default = false
int craftWriteAsyncZeroCopy         = CRAFT_WRITE_ASYNC_ZERO_COPY;    // default = false
int craftMakeAsyncCopy              = false;
int craftNumBufferCp                = 2;
bool craftInvalidateNestedCp        = true;
bool craftSignalForCp               = false;
std::string craftCommSpawnPolicy    = CRAFT_COMM_SPAWN_POLICY;
std::string craftCommRecoveryPolicy = CRAFT_COMM_RECOVERY_POLICY;
std::string nodeFile                = "CRAFT_allNodeFile";
std::string craftCommRecoveryCpReadPolicy = CRAFT_COMM_RECOVERY_CP_READ_POLICY;
std::string craftScrCpName          = "";

void CRAFT_getEnvParam(){
  char* envIn;
 
  envIn = getenv ("CRAFT_DEBUG");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftDebug= stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_DEBUG: %d", craftDebug);
  }

  envIn = getenv ("CRAFT_DEBUG_PROC");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftDebugProc = stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_DEBUG_PROC: %d", craftDebugProc);
  }

  envIn = getenv ("CRAFT_ENABLE");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftEnabled = stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_ENABLE: %d", craftEnabled);
  }

  envIn = getenv ("CRAFT_READ_CP_ON_RESTART");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftReadCpOnRestart = stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_READ_CP_ON_RESTART: %d", craftReadCpOnRestart);
  }

  envIn = getenv ("CRAFT_CP_PATH");
  if (envIn!=NULL){
    craftCpPath = envIn; 
    craftDbg(4, "CRAFT_CP_PATH: %s", (craftCpPath).c_str());
  }

/*  envIn = getenv ("CRAFT_USE_SCR");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftUseSCR = stringToNumber<int>(envInStr);
    craftDbg(4, "craftUseSCR : %d", craftUseSCR);
    if(craftUseSCR){
#ifndef SCR
      craftDbg(1, "craft is not compiled with SCR, thus env. variable CRAFT_USE_SCR can not be enabled(1)");
      craftUseSCR = 0;
#endif
    }
  }
*/
  envIn = getenv ("CRAFT_COMM_RECOVERY_POLICY");
  if (envIn!=NULL){
    craftCommRecoveryPolicy = envIn; 
    craftDbg(4, "CRAFT_COMM_RECOVERY_POLICY: %s", (craftCommRecoveryPolicy).c_str());
  }

  envIn = getenv ("CRAFT_COMM_SPAWN_POLICY");
  if (envIn!=NULL){
    craftCommSpawnPolicy= envIn; 
    craftDbg(4, "CRAFT_COMM_SPAWN_POLICY: %s", (craftCommSpawnPolicy).c_str());
  }

  envIn = getenv ("CRAFT_COMM_RECOVERY_CP_READ_POLICY");
  if (envIn!=NULL){
    craftCommRecoveryCpReadPolicy = envIn;
    craftDbg(4, "CRAFT_COMM_RECOVERY_CP_READ_POLICY: %s", craftCommRecoveryCpReadPolicy.c_str() );
  }

  envIn = getenv ("PBS_NODEFILE");
  if (envIn!=NULL){
    nodeFile = envIn; 
    craftDbg(4, "nodeFile: %s", (nodeFile).c_str());
  }
  envIn = getenv ("SLURM_JOB_NODELIST");
  if (envIn!=NULL){
    char * cmd = new char[1024];
    sprintf( cmd , "scontrol show hostname $SLURM_JOB_NODELIST > %s", nodeFile.c_str());
		system ( cmd );
    delete[] cmd;
  }

  envIn = getenv ("CRAFT_TIMING");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftTiming= stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_TIMING: %d", craftTiming);
  }

  envIn = getenv ("CRAFT_WRITE_ASYNC");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftWriteAsync= stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_WRITE_ASYNC: %d", craftWriteAsync);
    craftWriteAsyncZeroCopy = 0;                // By default the zero copy behavior is disabled.
    if(craftWriteAsync == true){
      int provided =-1;
      MPI_Query_thread(&provided);
      if(provided != MPI_THREAD_MULTIPLE){
        craftWarning(1, "CRAFT_WRITE_ASYNC: MPI_THREAD_MULTIPLE (provided=%d) is not availble. Initiate the MPI_Init_thread using MPI_THREAD_MULTIPLE. CRAFT will now make SYNC checkpoints.", provided);
        craftWriteAsync = false;
      }
      if(provided == MPI_THREAD_MULTIPLE){
        craftDbg(4, "CRAFT_WRITE_ASYNC test: (provided=MPI_THREAD_MULTIPLE=%d)", provided);
      }
    }
    if(craftWriteAsync == true && craftWriteAsyncZeroCopy == false){
      craftMakeAsyncCopy  = true; 
        craftDbg(4, "craftMakeAsyncCopy = true");
    }
    else{
      craftMakeAsyncCopy  = false; 
        craftDbg(4, "craftMakeAsyncCopy = false");
    }
  }

  envIn = getenv ("CRAFT_WRITE_ASYNC_ZERO_COPY");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftWriteAsyncZeroCopy = stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_WRITE_ASYNC_ZERO_COPY: %d, ", craftWriteAsyncZeroCopy);
    if(craftWriteAsyncZeroCopy == true && craftWriteAsync == true){
        craftWarning(1, "CRAFT_WRITE_ASYNC_ZERO_COPY is true. The checkpoints will be made directly from data ASYNCHRONOUSLY. The user has to make sure (by using Checkpoint::wait()) that data is properly checkpointed before modifying.");
    }
    // decide whether to make ASYNC copy or not, depeding on CRAFT_WRITE_ASYNC & CRAFT_WRITE_ASYNC_ZERO_COPY
    if (craftWriteAsync == true && craftWriteAsyncZeroCopy == false ){
      craftMakeAsyncCopy  = true; 
        craftDbg(4, "craftMakeAsyncCopy = true");
    }
    else{
      craftMakeAsyncCopy  = false; 
    }
  }

  envIn = getenv ("CRAFT_SCR_CP_NAME");       // by asking SCR checkpoint from the USER's in the environment variable, it is also made sure that only one Checkpoint can use the SCR-library.
  if (envIn!=NULL){
    craftScrCpName = envIn; 
    craftDbg(4, "CRAFT_SCR_CP_NAME: %s", (craftScrCpName).c_str());
  }

  envIn = getenv ("CRAFT_NUM_BUFFER_CP");
  if (envIn!=NULL){
    std::string envInStr;
    envInStr = envIn; 
    craftNumBufferCp = stringToNumber<int>(envInStr);
    craftDbg(4, "CRAFT_NUM_BUFFER_CP: %d", craftNumBufferCp);
  }
  return;
}


