/*
********************************************************************************
*                                                                              *
*     C functions for pinning processes on Linux with PLPA                     *
*                                                                              *
*     AUTHORS: Thomas ZEISER, RRZE, Universitaet Erlangen (Germany)            *
*              ( - 2010)            thomas.zeiser@rrze.uni-erlangen.de         *
*              Markus WITTMANN, RRZE, Universitaet Erlangen (Germany)          *
*              (2010)             markus.wittmann@rrze.uni-erlangen.de         *
*     Modified by: Faisal Shahzad, RRZE, Universitaet Erlangen (Germany)       *
*              (2017)             faisal.shahzad@fau.de                        *
*                                                                              *
*     Acknowledgements:                                                        *
*                      based on an example from Georg Hager, RRZE              *
*                                                                              *
********************************************************************************
*
*  THIS SOURCE FILE CONTAINS EXTENSIONS FROM RRZE WHICH DO NOT BELONG
*  TO THE DC-SOFTWARE-CORE !!
*  SPECIAL CONDIIONS MAY THEREFORE APPLY TO THE PRESENT SOURCES.
*  CONTACT hpc@rrze.uni-erlangen.de FOR DETAILS IF NECESSARY.
*
********************************************************************************
*/
#include <mpi.h>
#include <stdio.h>
#include <cstring>
#include <iomanip>

#ifdef WIN32

  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>

#elif defined(__sun)

  #include <stdlib.h>
  #include <sys/types.h>
  #include <sys/procset.h>
  #include <sys/processor.h>

#else

  #ifdef HAVE_PLPA
      #include <plpa.h>
  #else

    #ifndef _GNU_SOURCE
      #define _GNU_SOURCE
    #endif

    #include <stdlib.h>
    #include <sched.h>
    #include <errno.h>

//     #ifdef __PGI
//       // PGI brings its own sched.h stub and defines CPU_SET/CPU_ZERO incorrect
//       // here we provide a correction
//
//       #define __CPU_ZERO(set)   __CPU_ZERO_S(sizeof(cpu_set_t), set)
//       #define __CPU_SET(cpu, set)  __CPU_SET_S(cpu, sizeof(cpu_set_t), set)
//     #endif

  #endif // HAVE_PLPA

#endif // WIN32
#include "include/pinning.h"

#define ERROR_PIN_BASE              (-1000)
#define ERROR_PIN_NOT_IMPLEMTED     (ERROR_PIN_BASE - 1)

#include "craftDebugFuncs.hpp"

int env_var_to_i(const char * env, int * env_set, int default_value)
{
  char * env_var = getenv(env);

  if (env_var == NULL) {
    return default_value;
  }

  *env_set = 1;
  return atoi(env_var);
}

int pin_current_thread_to_cpu(int * cpu)
{
	int err = ERROR_PIN_NOT_IMPLEMTED;
	
  #ifdef WIN32

    SetThreadAffinityMask(GetCurrentThread(), 1 << *cpu);
    // TODO: check return value / GetLastError()

  #elif defined(__sun)

    err = processor_bind(P_LWPID, P_MYID, *cpu, NULL);
    if (err != 0) {
      printf("pinning thread to cpu %d failed (processor bind error code %d: %s).\n", cpu, err, strerror(err));
      THROW("Pinning failed.");
    }

  #else

    #ifdef HAVE_PLPA
      plpa_cpu_set_t cpu_set;
      PLPA_CPU_ZERO(&cpu_set);
      PLPA_CPU_SET(*cpu, &cpu_set);

      err = plpa_sched_setaffinity((pid_t)0, sizeof(plpa_cpu_set_t), &cpu_set);
    #else
      cpu_set_t cpu_set;
      CPU_ZERO(&cpu_set);
      CPU_SET(*cpu, &cpu_set);

      err = sched_setaffinity((pid_t)0, sizeof(cpu_set_t), &cpu_set);
    #endif

    if (err != 0) {
//       ErrorPrint("pinning thread to cpu %d failed (plpa error code %d: %s).\n", cpu, err, strerror(err));
       craftErr("pinnig thread to cpu %d failed (plpa error code %d: %s).\n", cpu, err, strerror(err));
//       THROW("Pinning failed.");
    }
  #endif

  return err;
}

// --------------------------------------------------------------------------
//  pins the calling/current thread to the specified cpu number
//  or computes the cpu number out of environment variables
// --------------------------------------------------------------------------

// 0,1,2_6,7,8
int pin_current_thread(int rank, int local_rank, int * cpu)
{
  char * cpu_list = getenv("CRAFT_ASYNC_THREAD_PIN_CPULIST");
  char * c = cpu_list;
  int i = 0;

  *cpu = -1;

  if (cpu_list == NULL) {
    return 0;
  }

  // cpu list is in the format of 0,1,2_3,4,5
  while (((*c >= '0' && *c <= '9') || *c == ',' || *c == '_')) {
    ++c;
  }

  if (*c != 0x00) {
    // invalid character detected
    return -1000;
  }

  c = cpu_list;

  while (i < local_rank && *c != 0x00) {
    if (*c == '_') ++i;
    ++c;
  }

  if (i != local_rank || *c < '0' || *c > '9') {
    // local list not found
    return -1001;
  }

  *cpu = atoi(c);
  craftDbg(4, "localRank/globalRank: %d/%d, cpu_to_pin: %d", local_rank, rank, *cpu);

  return pin_current_thread_to_cpu(cpu);
}

int doPinning(MPI_Comm * comm){
	//////////////////////////////////////////////////////////////////
	//		CREATE COMMUNICATOR WITHIN NODE			//
	////////////////////////////////////////////////////////////////// 
	MPI_Comm comm_node = MPI_COMM_NULL;
	char *node_name= new char[MPI_MAX_PROCESSOR_NAME];
	int len;

	MPI_Get_processor_name(node_name, &len);
// 	cout << "my node name is: " << node_name << endl;
	char * node_name_number = node_name+1;
//  cout << "my node name number  is: " << node_name_number << endl;
//====================== COMM_NODE =========================
	int color_my_node_num=atoi(node_name_number);
	
// 	cout << "my node num is: " << color_my_node_num << endl;
	MPI_Comm_split( *comm, color_my_node_num, 0, &comm_node );

	int rank_within_node = -1;
	int numprocs_within_node = -1;
	
	MPI_Comm_rank( comm_node, &rank_within_node );
	MPI_Comm_size( comm_node, &numprocs_within_node );
 int numprocs, rank; 
	MPI_Comm_rank( *comm, &rank);
	MPI_Comm_size( *comm, &numprocs);
//  printf("rank/numProcs: %d/%d, \t locRank/locNumProcs: %d/%d\n", rank, numprocs, rank_within_node, numprocs_within_node);

	int cpu = -1;
  int err = 0;
  err = pin_current_thread(rank, rank_within_node, &cpu);
  if(err != 0){ 
    printf("ERROR in pin_current_thread\n");
  }

  return 0;
}

int pin_get_current_cpu(int * cpu)
{
  int i;

  int err = ERROR_PIN_NOT_IMPLEMTED;
  *cpu = -1;

  #ifdef WIN32
    // GetThreadAffinityMask(GetCurrentThread(), 1 << coreNumber);
  #elif defined(__sun)
    // not implemented
  #else

    #ifdef HAVE_PLPA

      plpa_cpu_set_t cpu_set;
      PLPA_CPU_ZERO(&cpu_set);
      PLPA_CPU_SET(coreNumber, &cpu_set);

      err = plpa_sched_getaffinity((pid_t)0, sizeof(plpa_cpu_set_t), &cpu_set);

      for (i = 0; i < PLPA_BITMASK_CPU_MAX; ++i) {
        if (PLPA_CPU_ISSET(i, &cpu_set)) {
          *cpu = i;
          break;
        }
      }

    #else
      cpu_set_t cpu_set;
      CPU_ZERO(&cpu_set);

      err = sched_getaffinity((pid_t)0, sizeof(cpu_set_t), &cpu_set);

      // constant CPU_SETSIZE is one larger than the maximum CPU
      // number that can be stored in a CPU set
      for (i = 0; i < CPU_SETSIZE; ++i) {
        if (CPU_ISSET(i, &cpu_set)) {
          *cpu = i;
          break;
        }
      }
    #endif
  #endif

  return err;
}
