#include <string>
#include <cstring>
#include <fstream>
#include <signal.h>
#include <unistd.h>
#include <sys/stat.h>
#include <set>

#include "cpBase.hpp"
#include "checkpoint.hpp"
#include "cpHelperFuncs.hpp"
#include "craftDebugFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "pinning.h"

#ifdef AFT
  #include "aft.h"
#endif

#ifdef SCR
extern "C"{
	#include <scr.h>
}
#endif

using namespace cpHelperFuncs;

int Checkpoint::addToMapDirectly(std::string label, std::shared_ptr<CpBase> const objToAdd) // this is called if user's data type is already derived from CpBase
{
  
  checkpointablesMap[label] = objToAdd;
  craftDbg(0, "Checkpoint::addToMapDirectly(): this data is direcly added: %s", label.c_str());
  return EXIT_SUCCESS;
}

int Checkpoint::addToMap(std::string label, std::shared_ptr<CpBase> const objToAdd) 
{
  checkpointablesMap[label] = objToAdd;
  return EXIT_SUCCESS;
}

void CRAFT_signalCpHandler(int signum)
{
  int myrank_ = -1, numprocs_ = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank_);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs_);
  printf("%d/%d: ==== Caught signal %d\n", myrank_, numprocs_, signum);
  craftSignalForCp = true;
}

Checkpoint::Checkpoint( const std::string name_,
                        const MPI_Comm cpMpiCommExternal)
{
  static bool firstRun=true;          // This makes sure that CRAFT_getEnvParam, SCR_Init, etc are called only once for multiple Checkpoints.
  if(firstRun){
#ifndef AFT
  CRAFT_getEnvParam();
#endif  
  }
  static std::set<std::string> nameKeys;    // to determine the uniqueness of different checkpoint names.
  std::pair<std::set<std::string>::iterator,bool> ret;
  ret         =  nameKeys.insert( name_);
  if(ret.second==false){
		craftDbg(3, "%s: checkpoint with name '%s' already exists.", __func__, name_.c_str());
  } 
	cpName 				= name_;
  checkDirectoryName(&cpName);                        // checks if the given name makes a valid directory name.
	cpMpiCommInternal = cpMpiCommExternal;	
	cpBasePath 	= craftCpPath;
	cpPath 			= cpBasePath + "/" + "Checkpoint-" + cpName;
	craftDbg(3, "%s: path is: %s", __func__, cpPath.c_str());
	cpVersionPrefix = "v-";
	cpCommitted = false;
	cpVersion 	= 0;
  restartStatus = false; 

//  cpUseSCR    = craftUseSCR ;
  cpUseSCR = false;
  if ( cpName == craftScrCpName)  
  {
    cpUseSCR = true;
  }
  int myranktemp;
  MPI_Comm_rank(cpMpiCommInternal, &myranktemp);
  MPI_Barrier(cpMpiCommInternal); 
  if (cpUseSCR){
  #ifdef SCR
      #ifdef AFT
  MPI_Barrier(cpMpiCommInternal); 
      craftDbg(3, "commit: SCR_Init(&cpMpiCommInternal) use of SCR is with AFT: %d", cpUseSCR);
      SCR_Init(&cpMpiCommInternal);     // SCR has to be initialized only once. It is important in case of multilevelCP
  MPI_Barrier(cpMpiCommInternal); 
      #else 
      craftDbg(3, "commit:SCR_Init() use of SCR is without AFT: %d", cpUseSCR);
  //    cpMpiCommInternal = MPI_COMM_WORLD;
      SCR_Init(&cpMpiCommInternal);
      #endif
  #endif
  }

	if(cpMpiCommInternal == MPI_COMM_WORLD) {
		craftDbg(3, "%s: cpMpiCommInternal = MPI_COMM_WORLD", __func__);
  }
  if (firstRun){
#ifdef CPVIASIGNAL
  craftDbg(3, "%s: via signal is enabled", __func__);
  signal(SIGALRM, CRAFT_signalCpHandler);
#endif
//    firstRun = false;
  }
}	

Checkpoint::~Checkpoint(){
  craftDbg(4, "%s: ~Checkpoint f1", __func__);
  craftDbg(1, "Checkpoint::~Checkpoint() %s total update time = %f", cpName.c_str(), cpUpdateTime);
  craftDbg(1, "Checkpoint::~Checkpoint() %s total write time  = %f", cpName.c_str(), cpWriteTime);
//  craftDbg(1, "Checkpoint::~Checkpoint() %s total cp time     = %f", cpName.c_str(), cpUpdateTime + cpWriteTime);
/*    //NOTE: Deprecated since usage of shared_ptr for CpBase()
  intVector::iterator itvec = cpBaseObjsOwnedBy.begin();
  cpMap_t::iterator it = checkpointablesMap.begin();
  for(it = checkpointablesMap.begin() ; it != checkpointablesMap.end(); ++it, ++itvec)
  {
    if((*itvec) == USER){
	    craftDbg(4, "User is responsible to destroy this object.%s ", it->first.c_str());
    }
    if( (*itvec) == CRAFT)
    {
	    craftDbg(4, "deleting %s", it->first.c_str());
//      delete it->second;
    }
  }
*/
  // if asynchronous checkpoints are enabled, wait for the last one to complete here, if that is still valid. 
  if(fut.valid())              // If there is a previous checkpoint to be written; then wait for previous CP to finish
  {
    craftDbg(3, "~Checkpoint: fut state is valid at ~Checkpoint, thus waiting here.");
    this->wait();
  }
  craftDbg(4, "%s: ~Checkpoint f1", __func__);
  if(cpUseSCR){
  #ifdef SCR
//    craftDbg(3, "~Checkpoint: ====== SCR_Finalize() ====== ");
    craftDbg(-1, "~Checkpoint: ====== SCR_Finalize() ====== ");
    SCR_Finalize();  // TODO: FIXIT check if SCR_FINALIZE should be called during AFT failure-recovery or not.
  #endif
  }
  checkpointablesMap.clear();  

  craftDbg(4, "Checkpoint::~Checkpoint: f2");
}

int Checkpoint::commit(){
  if( !craftEnabled ){
		craftDbg(0, "Checkpoint::commit(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  if (cpUseSCR){
/*
#ifdef SCR
    #ifdef AFT
    craftDbg(3, "commit: SCR_Init(&cpMpiCommInternal) use of SCR is with AFT: %d", cpUseSCR);
    SCR_Init(&cpMpiCommInternal);     // SCR has to be initialized only once. It is important in case of multilevelCP
    #else 
    craftDbg(3, "commit:SCR_Init() use of SCR is without AFT: %d", cpUseSCR);
//    cpMpiCommInternal = MPI_COMM_WORLD;
    SCR_Init(&cpMpiCommInternal);
    #endif
#endif
*/
  }
  else{
    mkCpDir(cpPath, cpMpiCommInternal);	
    MPI_Barrier(cpMpiCommInternal);			// makes sure that cp-directory is created before any other process tries to write in it.
  }
  craftDbg(0, "Checkpoint::commit(): complete: %d", craftEnabled);
  cpCommitted = true;	
  return EXIT_SUCCESS;
}

int Checkpoint::deleteBackupCp(MPI_Comm comm_){
  int myrank = -1;	
  MPI_Barrier(comm_);
  MPI_Comm_rank(comm_, &myrank);
  if(myrank == 0){
    std::string toRmDir = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion-craftNumBufferCp-1);
    std::string cmd = "rm -r " + toRmDir;	
    struct stat sb;
    if (stat(toRmDir.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
    {
      craftDbg(5, "deleteBackupCp: removing directory command is  %s ", cmd.c_str());
      system ( cmd.c_str());
    }
  }
  MPI_Barrier(comm_);
  return EXIT_SUCCESS;
}

// ==== UPDATE & WRITE ====== //
int Checkpoint::updateAndWrite(const int iter, const int cpfreq)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::updateAndWrite(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  if (cpfreq != 0){
    if(iter % cpfreq == 0){
//      CRAFT_getWalltime(&updateT1);
//      craftTime("Checkpoint::updateAndWrite: update begin()", &cpMpiCommInternal );
      CRAFT_CHECK_ERROR(  update()  );
//      craftTime("Checkpoint::updateAndWrite: update end()", &cpMpiCommInternal );
//      CRAFT_getWalltime(&updateT2);

//      MPI_Barrier(cpMpiCommInternal);
//      craftDbg(1, "updateAndWrite: checkpoint:%s update time is : %f", cpName.c_str(), updateT2 - updateT1);
      
//      CRAFT_getWalltime(&writeT1);
//      craftTime("Checkpoint::updateAndWrite: write begin()", &cpMpiCommInternal );
      CRAFT_CHECK_ERROR(  write() ); 
//      craftTime("Checkpoint::updateAndWrite: write end()", &cpMpiCommInternal );
//      CRAFT_getWalltime(&writeT2);

//      craftDbg(1, "updateAndWrite: checkpoint:%s write time is : %f", cpName.c_str(), writeT2 - writeT1);

    }
  }
  return EXIT_SUCCESS;
}

// ===== UPDATE & WRITE AT SIGNAL ===== // 

int Checkpoint::updateAndWriteAtSignal(const int iter)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::updateAndWriteAtSignal(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  // check if all processes have same iter
  if ( craftSignalForCp == true ) {
    int myrank_ = -1, numprocs_ = -1;
    MPI_Comm_rank(cpMpiCommInternal, &myrank_);
    MPI_Comm_size(cpMpiCommInternal, &numprocs_);
//    craftDbg(4,  "%d/%d: ======= Cp signal is received at iter = %d\n", myrank_, numprocs_, iter);
    printf("%d/%d: Cp signal is received at iter = %d\n", myrank_, numprocs_, iter);
    unSkewIter = getUnskewIter(cpMpiCommInternal, iter); 
//    craftDbg(4, "%d/%d: Unskew (synchronized) iter is = %d\n", myrank_, numprocs_, unSkewIter);
    printf("%d/%d: Cp Unskew(synchronized) iter is = %d\n", myrank_, numprocs_, unSkewIter);
        
    craftSignalForCp = false;
    takeCp=true;
  }

  if ( takeCp == true && iter == unSkewIter) {
    int myrank_ = -1, numprocs_ = -1;
    MPI_Comm_rank(cpMpiCommInternal, &myrank_);
    MPI_Comm_size(cpMpiCommInternal, &numprocs_);

//    printf( "%d/%d: before MPI_Barri %d\n", myrank_, numprocs_, iter);
    MPI_Barrier(cpMpiCommInternal);
//    printf( "%d/%d: after MPI_Barri %d\n", myrank_, numprocs_, iter);
    updateAndWrite();
    // TODO: check why takeCp is not set to false here?
  }
  return EXIT_SUCCESS;
}



// ===== UPDATE ===== //
int Checkpoint::update(){
  if( !craftEnabled ){
    craftDbg(1, "Checkpoint::update(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  craftDbg(1, "update() : begin");
  if( craftMakeAsyncCopy == false)
  {
    craftDbg(1, "Checkpoint::update() The Async feature is disabled. No update is required. The data will be written directly from the buffer.");
    return EXIT_SUCCESS;
  }

  if ( craftWriteAsync == true && craftWriteAsyncZeroCopy == true){
   craftWarning(1, "Checkpoint::update() CRAFT_WRITE_ASYNC = true && CRAFT_WRITE_ASYNC_ZERO_COPY = true, Thus there is no additional(asynchronous) data copy to be updated. The data will be direcly written from the buffer. User must call Checkpoint::wait() before modifying the checkpoint data.");
    return EXIT_SUCCESS;
  }

  if (cpCommitted != true){
    craftDbg(1, "update(): cpCommitted = %d, commit the checkpoint before update, write, read operations", cpCommitted);
    return EXIT_FAILURE;
  }
  
  if( craftWriteAsync == true && fut.valid()==true )    // If there is a previous checkpoint to be written; then wait before creating the copy
  {
    craftDbg(3, "Checkpoint::update(): last async fut state is valid, thus waiting before updating.");
    this->wait();
  }

  craftDbg(1, "update() : updating Checkpoint...");
  static double TU1=0.0, TU2=0.0, deltaTU=0.0;
  CRAFT_getWalltime(&TU1); 
  for(cpMap_t::iterator it = checkpointablesMap.begin(); it != checkpointablesMap.end(); ++it)
  {
    CRAFT_CHECK_ERROR(  it->second->update()  );
  }	
  CRAFT_getWalltime(&TU2); 
  deltaTU = TU2-TU1;
  craftDbg(1, "Checkpoint::update() %s-version-%d update time is = %f", cpName.c_str(), cpVersion, deltaTU);
  cpUpdateTime += deltaTU;
	MPI_Barrier(cpMpiCommInternal);
  craftDbg(1, "update() : update() completed successfully");
  return EXIT_SUCCESS;
}

// ===== WRITE ===== //
int Checkpoint::write()
{	
  craftDbg(1, "Checkpoint::write() : begin");
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::write(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }

  if (cpCommitted != true){
    craftDbg(1, "Checkpoint::write(): cpCommitted = %d, commit the checkpoint before update, write, read operations", cpCommitted);
    return EXIT_FAILURE;
  }
  				
  craftDbg(1, "write(): writing Checkpoint...");
  static double TW1=0.0, TW2=0.0, deltaTW=0.0;
  CRAFT_getWalltime(&TW1); 
    
  if( cpUseSCR ) {
#ifdef SCR
    CRAFT_CHECK_ERROR(  SCRwrite()  );
#endif
  }
  else{
    CRAFT_CHECK_ERROR(  PFSwrite()  );
  }
  if ( craftInvalidateNestedCp == true ) {    // invalidate all nested CPs if any are there. // this is true all the time. every checkpoint invalidates its child checkpoints.
    CRAFT_CHECK_ERROR ( invalidateNestedCp() );
  }
  CRAFT_getWalltime(&TW2); 
  deltaTW = TW2-TW1;
  craftDbg(1, "Checkpoint::write() %s-version-%d write time is = %f", cpName.c_str(), cpVersion-1, deltaTW);
  cpWriteTime += deltaTW;

  craftDbg(1, "write(): writing completed successfully");
  return EXIT_SUCCESS;
}

int Checkpoint::wait(){
  if(fut.valid())
  { 
    craftDbg(4, "wait(): waiting for std::future with fut.get()");
    ret = fut.get();
    if (ret == EXIT_FAILURE){
      craftDbg(4, "wait(): The return of previous fut.get() is EXIT_FAILURE");
      return EXIT_FAILURE;
    }
    else
    {
      craftDbg(4, "wait(): fut.get() is EXIT_SUCCESS");
      return EXIT_SUCCESS;
    }
  }
  else
  {
    craftDbg(4, "wait(): no valid std::future to wait for");
  }
  return EXIT_SUCCESS;
}


// ===== READ ===== //
int Checkpoint::read(const std::string toReadCpable)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::read(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }

  if (cpCommitted != true){
    craftDbg(1, "Checkpoint::read(): cpCommitted = %d, commit the checkpoint before update, write, read operations", cpCommitted);
    craftErr("%s: not successful @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__);
    return EXIT_FAILURE;
  }

  craftDbg(1, "Checkpoint::read() reading checkpoint...");
  if( cpUseSCR ) {
#ifdef SCR
    int isScrRestart;
    char name[SCR_MAX_FILENAME];
    SCR_Have_restart(&isScrRestart, name);
//    isScrRestart = true;
    craftDbg(3, "Checkpoint::read(): isScrRestart= %d, name= %s",isScrRestart, name); 
    if (isScrRestart){
      craftDbg(3, "Checkpoint::read(): isScrRestart is true %s", name); 
      SCR_Start_restart(name);
      CRAFT_CHECK_ERROR(  SCRread(toReadCpable) );
      int valid=1;
      SCR_Complete_restart(valid);
      return EXIT_SUCCESS;        // SCR only has one level of Cp. So if it is requesting a restart, app needs restart
    }
    else
    {
      craftDbg(3, "Checkpoint::read(): isScrRestart is false"); 
      return EXIT_FAILURE;
    }
#endif
  }
  else{
    CRAFT_CHECK_ERROR(  PFSread(toReadCpable) );
  }
  craftDbg(1, "Checkpoint::read() completed");
  if(toReadCpable != ""){     // if a specific checkpointable is read, cpVersion is even not updated. it will only be non-zero once the complete checkpoint is read. This is to ensure that needRestart could be called again.
    cpVersion = 0;
  }
  return EXIT_SUCCESS;
}

int Checkpoint::SCRwrite(){
#ifdef SCR
  SCR_Start_checkpoint();
  int myrank_ = -1;
  MPI_Comm_rank(cpMpiCommInternal, &myrank_);
  std::string myrank = numberToString(myrank_);
  std::string filename;
  cpPathVersion = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion);

  for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
  {
    filename = cpPathVersion + "/" + (it->first) + myrank + ".ckpt"; 
    char * fNameScrPath = new char[1024]; 
    char * tmpFilename  = new char[1024];
    strcpy(tmpFilename, filename.c_str()); 
    SCR_Route_file(tmpFilename, fNameScrPath);
    filename = fNameScrPath;
    craftDbg(2, "SCRwrite: writing file: %s", filename.c_str());
    if(EXIT_SUCCESS != it->second->write(filename)){
      craftErr("%s: SCRwrite not successful @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
          );
      return EXIT_FAILURE;
    }
    delete[] fNameScrPath;
    delete[] tmpFilename;
  }
// === writeMetaData file === //  NOTE: writing metadata is not necessary for SCR. SCR manages its own cpVersions.
/*
  filename = cpPath + "/" + "metadata" + myrank + ".ckpt"; 
  char * fNameScrPath = new char[1024]; 
  char * tmpFilename  = new char[1024];
  strcpy(tmpFilename, filename.c_str()); 
  SCR_Route_file(tmpFilename, fNameScrPath);
  filename = fNameScrPath;
  craftDbg(2, "SCRwrite: writing file: %s", filename.c_str());
//  writeCpMetaData(filename);
  delete[] fNameScrPath;
  delete[] tmpFilename;
*/
  int valid = 1;
  SCR_Complete_checkpoint(valid);
  ++cpVersion;
#endif
  return EXIT_SUCCESS;
}

int Checkpoint::PFSwrite(){
  if(craftWriteAsync==true){            // This will initiate async, be it from copy of data or original data.
    craftDbg(3, "PFSwrite()::calling asynPFSwrite()");
    CRAFT_CHECK_ERROR(  asynPFSwrite()  );
  }
  else
  {
    craftDbg(3, "PFSwrite()::calling syncPFSwrite()");
    CRAFT_CHECK_ERROR(  syncPFSwrite()  ); 
  }
  return EXIT_SUCCESS;
}

int Checkpoint::syncPFSwrite(){
  int myrank_ = -1;
  MPI_Comm_rank(cpMpiCommInternal, &myrank_);

  std::string myrank = numberToString(myrank_);
  std::string filename;
  cpPathVersion = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion);
  mkCpDir(cpPathVersion, cpMpiCommInternal);	
  MPI_Barrier(cpMpiCommInternal);			// makes sure that cpVersion directory is created before any process tries to write in it.
  for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
  {
    if(it->second->cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      filename = cpPathVersion + "/" + (it->first) + ".ckpt"; 
      craftDbg(2, "syncPFSwrite CP: WITH MPIIO %s", filename.c_str());
    }
    else	                       																													// Serial PFS IO
    {
      filename = cpPathVersion + "/" + (it->first) + myrank + ".ckpt"; 
      craftDbg(2, "syncPFSwrite CP: SERIALIO %s", filename.c_str());
    }
    if(EXIT_SUCCESS != it->second->write(filename)){
        craftErr("%s, PFSwrite not successful @ %s:%d",
                __PRETTY_FUNCTION__, __FILE__, __LINE__
        );
				return EXIT_FAILURE;
    }
	  MPI_Barrier(cpMpiCommInternal);
	}
	MPI_Barrier(cpMpiCommInternal);			// TODO: do MPI_Gather here 
  // === writeMetaData file === // 
	if(myrank_ == 0){
		filename = cpPath + "/" + "metadata.ckpt"; 
	  CRAFT_CHECK_ERROR(  writeCpMetaData(filename) );
	}
	MPI_Barrier(cpMpiCommInternal);
	++cpVersion;
	deleteBackupCp(cpMpiCommInternal);
	MPI_Barrier(cpMpiCommInternal);
  return EXIT_SUCCESS;
}

int Checkpoint::asynPFSwrite(){
  static int firstRun = true; 
  if(firstRun==true){
    MPI_Comm_dup(cpMpiCommInternal, &asynComm);
    for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
    {
      it->second->cpMpiComm = asynComm;         // duplicate of the old communicator, So async thread could call MPI collectives on its orginal communicator. 
    }
    firstRun = false;
  }

  if(fut.valid())              // If there is a previous checkpoint to be written; then wait for previous CP to finish
  {
    craftDbg(3, "asynPFSwrite(): last async fut state is valid, thus waiting for last CP to finish.");
    this->wait();
  }
  else
  {
    craftDbg(4, "asynPFSwrite(): fut state is NOT valid. it is safe to write new CP");
  }
//  futures.push_back(std::async(std::launch::async, &Checkpoint::doAsynPFSwrite, this, &asynComm));
  fut = std::async(std::launch::async, &Checkpoint::doAsynPFSwrite, this, &asynComm);
  craftDbg(4, "asynPFSwrite: async write function is assigned to std::async() funtion");
  return EXIT_SUCCESS;
}


int Checkpoint::doAsynPFSwrite(MPI_Comm * asynComm){      //NOTE: Do NOT use cpMpiCommInternal here, because this routine is being called asynchronously and both threads cant call collectives using the same communicators.
  int myrank_ = -1;
  MPI_Comm_rank(*asynComm, &myrank_);
  doPinning(asynComm);

  std::string myrank = numberToString(myrank_);
  std::string filename;
  cpPathVersion = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion);
  mkCpDir(cpPathVersion, *asynComm);
  MPI_Barrier(*asynComm);			// makes sure that cpVersion directory is created before any process tries to write in it.
  for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
  {
    if(it->second->cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      filename = cpPathVersion + "/" + (it->first) + ".ckpt"; 
      craftDbg(2, "doAsyncPFSwrite CP: WITH MPIIO %s", filename.c_str());
    }
    else																														                      // Serial PFS IO
    {
      filename = cpPathVersion + "/" + (it->first) + myrank + ".ckpt"; 
      craftDbg(2, "AsyncPFSwrite CP: SERIALIO %s", filename.c_str());
    }
    if(EXIT_SUCCESS != it->second->write(filename)){
        craftErr("%s: PFSwrite not successful @ %s:%d",
                __PRETTY_FUNCTION__, __FILE__, __LINE__
        );
				return EXIT_FAILURE;
    }
	  MPI_Barrier(*asynComm);
	}
	MPI_Barrier(*asynComm);
  // === writeMetaData file === // 
	if(myrank_ == 0){
		filename = cpPath + "/" + "metadata.ckpt"; 
	  CRAFT_CHECK_ERROR(  writeCpMetaData(filename) );
	}
	MPI_Barrier(*asynComm);
	++cpVersion;
	deleteBackupCp(*asynComm); // TODO: sometimes it gives problem with async checkpointing. check why.
	MPI_Barrier(*asynComm);
  craftDbg(4, "doAsynPFSwrite(): finished with the actual async. checkpoint doAsynPFSwrite");
  craftTime("finished with actual async checkpoint in doAsynPFSwrite()", asynComm);
  return EXIT_SUCCESS;
}


int Checkpoint::writeCpMetaData(const std::string filename){			// writes a tmp file first and then replaces it after complete write
  std::string filenameTmp;
	filenameTmp = filename + ".tmp"; 

  craftDbg (2, "writeCpMetaData: cpVersion number is %d: ", cpVersion);
  CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameTmp, &cpVersion) );
//	std::string cmd = "mv " + filenameTmp + " " + filename;
  int ret=-99;
  ret = rename(filenameTmp.c_str(), filename.c_str());
  if( ret != 0){
    craftDbg(1, "writeCpMetaData: rename failed for %s. ret_val:%d", filenameTmp.c_str(), ret);
  }
//	system (cmd.c_str());
  return EXIT_SUCCESS;
}

int Checkpoint::PFSread(const std::string toReadCpable){
  int ret=EXIT_FAILURE;
	CRAFT_CHECK_ERROR(  readCpMetaData()  );									// cpVersion is updated here
  int rankToRead = -1;                                      // Rank of the process, i have to read.

#ifdef AFT
  if ( craftCommRecoveryCpReadPolicy == "NEW_RANK")
  {
  	MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
  } 
  else if ( craftCommRecoveryCpReadPolicy == "OLD_RANK")
  {
    if ( AFT_isRecoveryRun() == true) {
      rankToRead  = AFT_getPastRank();
    }
    else // in case if craftCommRecoveryCpReadPolicy is "OLD_RANK", but this is the first run of application and previous checkpoints are to be read.
    {
  	  MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
    }
  }
  else 
  {
    craftErr("%s: PFSread: Not a valid read policy. %s:%d",
                __PRETTY_FUNCTION__, __FILE__, __LINE__
        );
				return EXIT_FAILURE;
  }
#else 
	MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
#endif
  
	std::string rankStr = numberToString(rankToRead);
	std::string filename;
	cpPathVersion = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion); 

  cpMap_t::iterator itBegin, itEnd;
  if(toReadCpable == ""){                       // Not a specific toRead-Checkpointable is mensioned, thus read all checkpointables of the map
    itBegin = checkpointablesMap.begin(); 
    itEnd   = checkpointablesMap.end();
    craftDbg(2, "Checkpoint::PFSread: defining iterators for all the cpAbles%s", toReadCpable.c_str());
  }
  else      // if a specific checkpointable is to be read
  {
    craftDbg(2, "Checkpoint::PFSread: defining iterators for 1 cpAble %s", toReadCpable.c_str());
    itBegin = checkpointablesMap.find(toReadCpable);
    itEnd   = std::next(itBegin, 1);
  }
  
//  for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
  for (cpMap_t::iterator it=itBegin; it!=itEnd; it++)
 	{			
    if(it->second->cpAbleProp.getIOType() == MPIIO)                               // Parallel PFS IO
    {
		  filename = cpPathVersion + "/" + (it->first) + ".ckpt";				
      craftDbg(2, "read CP: WITH MPIIO %s", filename.c_str());
    }
    else                                                          // Serial PFS IO 
    { 
		  filename = cpPathVersion + "/" + (it->first) + rankStr + ".ckpt"; 
      craftDbg(2, "read CP: WITH SERIALIO %s", filename.c_str());
    }
	  MPI_Barrier(cpMpiCommInternal);
    craftDbg(2, "PFSread(): reading file: %s", filename.c_str());
		if(EXIT_SUCCESS != it->second->read(filename))	{
      craftErr("%s: PFSread not successful @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
      );
			return EXIT_FAILURE;
    }
  }
	MPI_Barrier(cpMpiCommInternal);
	++cpVersion;
	return EXIT_SUCCESS;
}

int Checkpoint::SCRread(const std::string toReadCpable){
#ifdef SCR
//	int myrank_ = -1;
//	MPI_Comm_rank(cpMpiCommInternal, &myrank_);
  int rankToRead = -1;                                      // Rank of the process, i have to read.
#ifdef AFT
  if ( craftCommRecoveryCpReadPolicy == "NEW_RANK")
  {
  	MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
  } 
  else if ( craftCommRecoveryCpReadPolicy == "OLD_RANK")
  {
    if ( AFT_isRecoveryRun() == true) {
      rankToRead  = AFT_getPastRank();
    }
    else // in case if craftCommRecoveryCpReadPolicy is "OLD_RANK", but this is the first run of application and previous checkpoints are to be read.
    {
  	  MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
    }
  }
  else 
  {
    craftErr("%s Not a valid read policy. %s:%d",
                __PRETTY_FUNCTION__, __FILE__, __LINE__
        );
				return EXIT_FAILURE;
  }
#else
	MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
#endif

	std::string rankStr = numberToString(rankToRead);
	std::string filename;
	cpPathVersion = cpPath + "/" + cpVersionPrefix + SSTR(cpVersion); 

  cpMap_t::iterator itBegin, itEnd;
  if(toReadCpable == ""){                       // Not a specific toRead-Checkpointable is mensioned, thus read all checkpointables of the map
    itBegin = checkpointablesMap.begin(); 
    itEnd   = checkpointablesMap.end();
    craftDbg(2, "Checkpoint::SCRread: defining iterators for all the cpAbles%s", toReadCpable.c_str());
  }
  else      // if a specific checkpointable is to be read
  {
    craftDbg(2, "Checkpoint::SCRread: defining iterators for 1 cpAble %s", toReadCpable.c_str());
    itBegin = checkpointablesMap.find(toReadCpable);
    itEnd   = std::next(itBegin, 1);
  }
  
//  for (cpMap_t::iterator it=checkpointablesMap.begin(); it!=checkpointablesMap.end(); it++)
  for (cpMap_t::iterator it=itBegin; it!=itEnd; it++)
 	{			
		filename = cpPathVersion + "/" + (it->first) + rankStr + ".ckpt"; 
		char * fNameScrPath = new char[256]; 
		char * tmpFilename = new char[256];
		strcpy(tmpFilename, filename.c_str()); 
		SCR_Route_file(tmpFilename, fNameScrPath);
		filename = fNameScrPath;
		craftDbg(2, "SCRread: reading file %s", filename.c_str());
		if(EXIT_SUCCESS != it->second->read(filename))	{
      craftErr("%: SCRread not successful @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
      );
			return EXIT_FAILURE;
    }
  }

	MPI_Barrier(cpMpiCommInternal);
	++cpVersion;
#endif

	return EXIT_SUCCESS;
}


int Checkpoint::readCpMetaData(){
	std::string filename, line;
//	filename = cpPath + "/" + "metadata.ckpt"; 
  filename = generateFullFileName(cpPath, "metadata");
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filename, &cpVersion) );
//  cpVersion = CRAFT_readASCIICounterFromFile<int>(filename);
	craftDbg(1, "readCpMetaData: cpVersion to read: %d", cpVersion);
  return EXIT_SUCCESS;	
}
/*
int Checkpoint::SCRreadCpMetaData(){
#ifdef SCR
  // === readMetaData file === // 
//	int myrank_ = -1;	
//	MPI_Comm_rank(cpMpiCommInternal, &myrank_);
  int rankToRead = -1;                                      // Rank of the process, i have to read.
  if ( craftCommRecoveryCpReadPolicy == "NEW_RANK")
  {
  	MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
  } 
#ifdef AFT
  else if ( craftCommRecoveryCpReadPolicy == "OLD_RANK")
  {
    if ( AFT_isRecoveryRun() == true) {
      rankToRead  = AFT_getPastRank();
    }
    else // in case if craftCommRecoveryCpReadPolicy is "OLD_RANK", but this is the first run of application and previous checkpoints are to be read.
    {
  	  MPI_Comm_rank(cpMpiCommInternal, &rankToRead);
    }
  }
#endif
  else 
  {
    craftErr("%s: Not a valid read policy. %s:%d",
                __PRETTY_FUNCTION__, __FILE__, __LINE__
        );
				return EXIT_FAILURE;
  }
  craftDbg(1, "Checkpoint::SCRreadCpMetaData(): cpVersion to read: %d", cpVersion);
	std::string rankStr = numberToString(rankToRead);
	std::string filename = cpPath + "/" + "metadata" + rankStr + ".ckpt"; 
  char * fNameScrPath = new char[256]; 
	char * tmpFilename  = new char[256];
	strcpy(tmpFilename, filename.c_str()); 
	SCR_Route_file(tmpFilename, fNameScrPath);
	filename = fNameScrPath;
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filename, &cpVersion) );
//  cpVersion = CRAFT_readASCIICounterFromFile<int>(filename);
  craftDbg(1, "SCRreadCpMetaData(): cpVersion to read: %d", cpVersion);
  return EXIT_SUCCESS;	
#endif
}
*/

int Checkpoint::restartIfNeeded(int * iter)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::restartIfNeeded(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  } 
  bool ret = 0;
  craftDbg(3, "Checkpoint::checking needRestart()"); 
  if ( needRestart() != true ){
    craftDbg(3, "Checkpoint::restartIfNeeded needRestart() returned false"); 
    return EXIT_SUCCESS;
  }
  else{
    craftDbg(1, "restartIfNeeded: needRestart is true for checkpoint: %s. Now reading checkpoint", cpName.c_str());
    CRAFT_CHECK_ERROR(  read()  );
    craftDbg(1, "restartIfNeeded: checkpoint %s is read successfully", cpName.c_str());
    if(iter!=NULL){
      *iter = *iter +1;
      craftDbg(1, "restartIfNeeded: The increased counter value is = %d", *iter);
    }
  } 
  return EXIT_SUCCESS;
}

bool Checkpoint::needRestart(){
  if( craftReadCpOnRestart == 0 ){    // in case, that checkpoints exist but user wants to restart from the beginning. This flag is set by ENV variables.
    craftDbg(3, "needRestart: Environment variable CRAFT_READ_CP_ON_RESTART is disabled(0) => Not restarting from last checkpoint");
    return false;   
  }

  int ret=-1;
  if(cpVersion!=0) { 
    craftDbg(3, "needRestart: %s::cpVersion != 0 cpVersion= %d", cpName.c_str(), cpVersion);
    return false;     // cpVersion is set to 0 at initialization. This means that if the inner L2 loop rotates. The checkpoint will not be read further.
  }

  if(cpVersion==0)   // means either its the 1st-run or restarted-run. Now we differentiate between them
  {     //  In case of 1st-run, there should be no Cp, else there should be Cp

// === SCR case === // 
#ifdef SCR
  if(cpUseSCR){
    char name[SCR_MAX_FILENAME];
    int isScrRestart;
    SCR_Have_restart(&isScrRestart, name);
    craftDbg(3, "Checkpoint::needRestart(): isScrRestart= %d, name= %s",isScrRestart, name); 
    
    if (isScrRestart){
      craftDbg(3, "Checkpoint::needRestart(): isScrRestart is true %s", name); 
      return true;
    }
    else
    {
        craftDbg(3, "Checkpoint::needRestart(): isScrRestart is false"); 
        return false;
    }
  
  }
#endif

// === PFS case === // 
    ret = readCpMetaData();
    if(ret == EXIT_FAILURE){  
      craftDbg(3, "needRestart: No PFScpMetaData file found, thus its 1st-run.");
      return false;
    }
    else {
      craftDbg(3, "needRestart: PFScpMetaData file is found,thus Checkpoint exists.");
      cpVersion = 0;   
      return true;
    }
  }
}

MPI_Comm Checkpoint::getCpComm(){
  return cpMpiCommInternal;
}

int Checkpoint::nestedCp(Checkpoint * parentCp_)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::nestedCp(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  
  parentCp = parentCp_;
  parentCp->addChild(this);
  craftDbg(2, "The checkpoint %s is child checkpoint of checkpoint %s", (this->getCpName()).c_str(), (parentCp->getCpName()).c_str()  ); 
  return EXIT_SUCCESS;
}


int Checkpoint::addChild(Checkpoint * child)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::addChild(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }

  children.push_back(child);
  return EXIT_SUCCESS;
}

std::string Checkpoint::getCpName(){
  return this->cpName;
}

int Checkpoint::invalidateNestedCp()
{
  std::vector<Checkpoint *>::iterator it = children.begin(); 
  for( it=children.begin(); it!=children.end(); ++it){
    craftDbg(1, "XXXXXX Now invalidating %s XXXXXX", ((*it)->getCpName()).c_str());
    (*it)->invalidate();
  }
  return EXIT_SUCCESS;
}

int Checkpoint::invalidate()
{
  int myrank_ = -1;
  MPI_Comm_rank(cpMpiCommInternal, &myrank_);
	std::string myrank = numberToString(myrank_);
  std::string filename, newFilename;
  filename = generateFullFileName(cpPath, "metadata");
  newFilename = generateFullFileName(cpPath, "invalidCP-metadata");

  craftDbg(2, "invalidate: old Filename: %s", filename.c_str());
  craftDbg(2, "invalidate: invalidated Filename: %s", newFilename.c_str());
  int ret=-99;
  ret = rename(filename.c_str(), newFilename.c_str());
  if( ret != 0){
    craftDbg(1, "invalidate(): error occured in invalidating");
  }
  return EXIT_SUCCESS;
}

std::string Checkpoint::generateFullFileName( const std::string cpPath, const std::string filename)
{
  std::string fullFilename;
#ifdef SCR
  if(cpUseSCR){
    int myrank_ = -1;
    MPI_Comm_rank(cpMpiCommInternal, &myrank_);
	  std::string myrank = numberToString(myrank_);
    fullFilename    = cpPath + "/" + filename + myrank + ".ckpt"; 
  }
  else{
    fullFilename    = cpPath + "/" + filename + ".ckpt"; 
  }
#else
    fullFilename    = cpPath + "/" + filename + ".ckpt"; 
#endif
  return fullFilename;
}

int CRAFT_DeleteCheckpointData(const std::string toRm){
// XXX: The toRm argument is a string as the real Checkpoint to delete may be out of scope (like in Cheb-dos-kpm example). Therefore, there is no way to find-out if checkpoint is made at using SCR or PFS. For this reason, both locations are checked and deleted.
  int myrank=0;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  std::string fullDirPath;
  std::string cpBasePath = "";
  char* envIn;
  envIn = getenv ("CRAFT_CP_PATH");
  if (envIn!=NULL){
    cpBasePath = envIn; 
    craftDbg(4, "craftDeleteCheckpointData::CRAFT_CP_PATH: %s", (cpBasePath).c_str());
  }
  
	fullDirPath = cpBasePath + "./Checkpoint-" + toRm;
  craftDbg(1, "craftDeleteCheckpointData::to delete checkpoint is %s", (fullDirPath).c_str());

  std::string cmd;
  struct stat sb;

    cmd = "rm -r " + fullDirPath;	
    if (stat(fullDirPath.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode))
    {
      craftDbg(5, "craftDeleteCheckpointData:: removing directory command is  %s ", cmd.c_str());
      if(myrank==0) system ( cmd.c_str());
      return EXIT_SUCCESS;    // if PFS-delete is successful, no need to carry on and delete scr-data.
    }

#ifdef SCR        // in case of SCR, this command will delete all SCR node-level Checkpoints 

  cmd = "pbsdsh -u rm -r $SCRATCH/$USER/scr.defjobid/scr.dataset.*";
  craftDbg(1, "craftDeleteCheckpointData::to delete cmd is %s", cmd.c_str());
  if(myrank==0) system (cmd.c_str());

#endif
  return EXIT_SUCCESS;
}
//  int addCpBaseOwnership(const int val);
//int Checkpoint::addCpBaseOwnership(const int val){ // NOTE: Deprecated since usage of shared_ptr for CpBase()
//  cpBaseObjsOwnedBy.push_back(val);
//  return 0;
//}
