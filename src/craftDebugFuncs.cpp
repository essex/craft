#include "craftDebugFuncs.hpp"
#include "craftConf.h"                    // contains craftDebug etc. values
#include <cpHelperFuncs.hpp>

using namespace cpHelperFuncs;

#include <iostream>
#include <sstream>
#include <stdarg.h> 
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>

#include <stdlib.h>
#include <sys/time.h>

void craftErr(const char *fmt, ...)
{
  va_list argp;
    if(mpiCommWorldRank() == craftDebugProc){
      fprintf(stderr, ANSI_COLOR_RED "%d: CRAFT_ERROR: ", mpiCommWorldRank());
      va_start(argp, fmt);
      vfprintf(stderr, fmt, argp);
      va_end(argp);
      fprintf(stderr, "\n" ANSI_COLOR_RESET);
    }
}

void craftDbg(int level, const char *fmt, ...)
{
  va_list argp;
  if (craftDebug > 0 && level == -1) {
      fprintf(stdout, "%d: CRAFT_DEBUG -1 :", mpiCommWorldRank());
      va_start(argp, fmt);
      vfprintf(stdout, fmt, argp);
      va_end(argp);
      fprintf(stdout, "\n");
      return;
  }
  if ((craftDebug > 0 && craftDebug >= level)) {
    if(mpiCommWorldRank() == craftDebugProc){
      fprintf(stdout, "%d: CRAFT_DEBUG :", mpiCommWorldRank());
      va_start(argp, fmt);
      vfprintf(stdout, fmt, argp);
      va_end(argp);
      fprintf(stdout, "\n");
    }
  }
}

void craftWarning(int level, const char *fmt, ...)
{
  va_list argp;
  if ((craftDebug > 0 && craftDebug >= level)) {
    if(mpiCommWorldRank() == craftDebugProc){
      fprintf(stdout, ANSI_COLOR_RED "%d: WARNING: CRAFT_DEBUG :", mpiCommWorldRank());
      va_start(argp, fmt);
      vfprintf(stdout, fmt, argp);
      va_end(argp);
      fprintf(stdout, "\n" ANSI_COLOR_RESET);
    }
  }
}

void craftAbort(const char *fmt, ...)
{
  va_list argp;
  fprintf(stderr, "%d: CRAFT_ABORT: ", mpiCommWorldRank());
  va_start(argp, fmt);
  vfprintf(stderr, fmt, argp);
  va_end(argp);
  fprintf(stderr, "\n");
  MPI_Abort(MPI_COMM_WORLD, 0);
}

int craftLog(MPI_Comm const * comm, const char *fmt, ...)
{
  sprintf(craftLogFile, "%s", "craftLog");
  int myrank=-1;
  MPI_Comm_rank(*comm, &myrank);
  if(myrank == craftDebugProc){
    if (( craftDebug > 0 )) {
      FILE * fstrL;
      fstrL = fopen(craftLogFile, "a");
      // ===== WRITE RESCUE LIST ===== // 
      va_list argp;
      va_start(argp, fmt);
      vfprintf(fstrL, fmt, argp);
      va_end(argp);
      fclose(fstrL); 
    }
  }
  return EXIT_SUCCESS;
}

void craftTime(const std::string str)
{ 
  if(craftTiming){
    double t=0.0;
    CRAFT_getWalltime(&t);
    va_list argp;
    MPI_Comm parent;
	  MPI_Comm_get_parent( &parent );
    if(mpiCommWorldRank() == craftDebugProc ){ 
      printf("craftTime:: %s: %f\n", str.c_str(), t);
    }
  }
}

void craftTime(const std::string str, const MPI_Comm * const comm)
{ 
  if(craftTiming){
    int myrank = -1;
    MPI_Comm_rank(*comm, &myrank);
    double t=0.0;
    CRAFT_getWalltime(&t);
    va_list argp;
    MPI_Comm parent;
	  MPI_Comm_get_parent( &parent );
    if(myrank == craftDebugProc ){      // only processes of original MPI_COMM_WORLD can do craft Time. This is to avoid having multiple entries after spawning
      printf("craftTime:: %s: %f\n", str.c_str(), t);
    }
  }
}

void CRAFT_getWalltime_(double* wcTime) {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  *wcTime = (double)(tp.tv_sec + tp.tv_usec/1000000.0);
}

void CRAFT_getWalltime(double* wcTime) {
  CRAFT_getWalltime_(wcTime);
}
