#include <iostream>
#include <sstream>
#include <stdarg.h> 
#include <cstdlib>
#include <unistd.h>
#include <sys/stat.h>

#include <mpi.h>

#include "cpHelperFuncs.hpp"
#include "craftDebugFuncs.hpp"


char * craftLogFile = new char[256];

int cpHelperFuncs::mpiCommWorldRank(){
  int myRank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);  
  return myRank;
}

int cpHelperFuncs::mpiCommWorldNumProcs(){
  int numProcs=-1;
  MPI_Comm_size(MPI_COMM_WORLD, &numProcs);  
  return numProcs;
}



void cpHelperFuncs::checkDirectoryName(std::string* const s)
{
  std::string::iterator it;
  for (it = s->begin() ; it < s->end() ; ++it){
    switch(*it){
      case '/':case '\\':case ':':case '?':case '"':case '<':case '>':case '|':
      {
        craftAbort("Invalid character in Checkpoint-directroy name. Please check the name of the Checkpoints %s", s->c_str() );
      }
    }
  }
}
/*
void cpHelperFuncs::checkPathName(std::string* const s)
{
  std::string::iterator it;
  for (it = s->begin() ; it < s->end() ; ++it){
    switch(*it){
      case '\\':case ':':case '?':case '"':case '<':case '>':case '|':
      {
		    craftDbg(0, "Invalid character in directroy name: %s", s );
        craftAbort(0, "Invalid character in directroy name: %s", s );
      }
    }
  }
}
*/

std::string cpHelperFuncs::exec(const char* cmd){ 
  char buffer[128];
  std::string result = "";
  FILE* pipe = popen(cmd, "r");
//  if (!pipe) 
//				throw std::runtime_error("popen() failed!");
  try {
    while (!feof(pipe)) {
       if (fgets(buffer, 128, pipe) != NULL){
         result += buffer;
       }
  	}
  } 
	catch (...) {
    pclose(pipe);
  	throw;
	}
	pclose(pipe);
	result.erase(result.end()-1);
	return result;
}

int cpHelperFuncs::calcSequentialOffset( MPI_Offset * offset, size_t numBytes, MPI_Comm comm)
{
  int myrank, numprocs;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &numprocs);
 
  *offset=0; 
  MPI_Scan(&numBytes, offset, 1 , MPI_COUNT, MPI_SUM, comm);
  *offset = *offset - numBytes;
/*
  for(int i=0; i<numprocs; ++i){
    if (myrank==i)
    {
      std::cout << "=========== myrank: " << myrank << " numBytes: " << numBytes << ", file offset is: " << *offset << std::endl;
    }
    MPI_Barrier(comm);
  }
*/
  return EXIT_SUCCESS;
}

int cpHelperFuncs::getUnskewIter(MPI_Comm comm_, int n)
{
  int myrank_ = -1;
  MPI_Comm_rank(comm_, &myrank_);
  int maxN;
  MPI_Allreduce(&n, &maxN, 1, MPI_INT, MPI_MAX, comm_);
//  craftDbg(4, "%d: UNSKEW: maxN = %d\n", myrank_, maxN);
  printf("%d: UNSKEW: maxN = %d\n", myrank_, maxN);
  return maxN; 
}

int cpHelperFuncs::mkCpDir(std::string path, MPI_Comm comm){

  int myrank_ = -1;
  MPI_Comm_rank(comm, &myrank_);
  craftDbg(4, "1mkCpDir is making: %s" , path.c_str());
  int ret=-99;
  struct stat st = {0};
  if(myrank_==0){
    if(stat(path.c_str(), &st) == -1) {
      for(int rep = 0; ret != 0 && rep < 10; ++rep ){
        ret = mkdir(path.c_str(), S_IRWXU);
        if( ret != 0){
          craftDbg(4, "mkCpDir failed for %s. ret_val:%d, rep: %d", path.c_str(), ret, rep);
        }
      }
      if (ret != 0)
      {
        craftDbg(1, "The following checkpoint directory could not be created. %s. ret_val:%d", path.c_str(), ret );
        exit(0);
      }
    } 
    else{
     craftDbg(4, "mkCpDir: The checkpoint directory already exists: %s" , path.c_str());
    }
  }
  MPI_Barrier(comm); 
  sync();
  return EXIT_SUCCESS;
}


/*
int calcNodeRankNumProcs(const MPI_Comm * const comm, int * rankNode, int * numprocsNode){
  MPI_Comm shmcomm;
  MPI_Comm_split_type( *comm , MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &shmcomm);
  int rank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  MPI_Comm_rank(shmcomm, rankNode);
  MPI_Comm_size(shmcomm, numprocsNode);
  printf("calcNodeRankNumProcs::rank=%d/%d\n", *rankNode, rank); 
  printf("calcNodeRankNumProcs::numprocs=%d/%d\n", *numprocsNode, numprocs); 
  return 0;
}
*/

/*

void getEnvVal(int &var, const char * str){
  int myrank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  if(myrank==0) 
  {
    if(const char* env_p = std::getenv(str)){
      var = stringToNumber<int>(env_p);
      std::cout << str << " val is : " << var << '\n';
    }else{
      std::cout << "Env. val of " << str << " is not set.\n";
    }
  }
  return;
}
*/
/*
void getEnvStr(char * varVal, const char * str){
  int myrank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  if(myrank==0) 
  {
    if(const char* env_p = std::getenv(str)){
      sprintf(varVal, "%s", env_p);
      std::cout << str << " valVal is : " << var << '\n';
    }else{
      std::cout << "Env. val of " << str << " is not set.\n";
    }
  }
  return;
}
*/


/*#define MAX_PATH_LEN 256

#define IN
#define OUT

typedef int Bool_T;

static
int GetCharsNumInPath( IN const char * path )
{
int cnt;

int i;
int n = strlen( path );

for( i = 0; i < n; i++ )
{
if( ( path[i] < 0x80 ) || ( path[i] > 0xbf ) )
cnt++;
}

return cnt;
}

Bool_T PathValid( IN const char * path )
{
  if( path != NULL )
  {
    if( GetCharsNumInPath( IN path ) <= MAX_PATH_LEN )
    {
      int i;
      int n = strlen( path );

      for( i = 0; i < n; i++ )
      {
        switch( path[i] )
        {
        // ? " / < > * |
        // these characters can not be used in file or folder names
        //
          case '?':
          case '\"':
          case '/':
          case '<':
          case '>':
          case '*':
          case '|':
              return false;
          
          // Can meet only between a local disk letter and full path
          // for example D:\folder\file.txt
          //
          case ':':
          {
              if( i != 1 )
              {
                return false;
              }
              else{
              break;
              }
          }
// Space and point can not be the last character of a file or folder names
//
          case ' ':
          case '.':
          {
            if( ( i + 1 == n ) || ( path[i+1] == PATH_SEPERATOR_CHAR ) )
            {
               return false;
            }
            else{
              break;
            }
          }
// two backslashes can not go straight
//
          case PATH_SEPERATOR_CHAR:
          {
            if( i > 0 && path[i - 1] == PATH_SEPERATOR_CHAR )
            {
              return false;
            }else{
              break;
            }
          }
        }
      }
    return true;
    }
    else{ // if( GetCharsNumInPath( IN path ) <= MAX_PATH_LEN )
      LOG_ERROR( "PathValid FAILURE --> path is too long" );
      return false;
    }
  }else{ // if( path != NULL )
// wrong argument
//
  return false;
  }
}  
*/
