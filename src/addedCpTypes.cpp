#include "include/addedCpTypes.hpp"

// specialized implementation of add functions
// NOTE: here a new object is created that will not be deleted unless we have some
//       memory management in the background, the "normal" add function just adds 
//       the pointer to the map and the person who created the object is responsible
//       for deleting it.


// ===== Objects added of cpBase class ===== //

int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::shared_ptr<CpBase> const obj
             )
{
  craftDbg(4, "addCpType of CpBase");
  rc = cp->addToMapDirectly(label,obj);
  if(rc){return EXIT_FAILURE;}
  return EXIT_SUCCESS;
}

// ===== MPI DERIVED DATA TYPES ===== //
int addCpType(Checkpoint * cp, 
              std::string label, 
              void * bufferPtr, 
              MPI_Datatype * mpiDataType, 
              int numElem)
{
  rc = cp->addToMap(label, std::make_shared<CpMpiDatatype>(bufferPtr, numElem, mpiDataType, cp->getCpComm()));
  if(rc){return EXIT_FAILURE;}
  return EXIT_SUCCESS;
}

#ifdef PHIST_CP 
int addCpType(Checkpoint * cp, 
              std::string label, 
              void * phistPtr, 
              const int PHIST_TYPE_ID,
              CpAbleProp cpAbleProp 
             )
{
  craftDbg(3, "addCpType \n %s \n", __PRETTY_FUNCTION__);
  switch(PHIST_TYPE_ID)
  {
    case PHIST_MVEC_S: 
      craftDbg(4, "addCpType::phist switch is PHIST_MVEC_S");
        rc = cp->addToMap(label, std::make_shared<CpPhistMvec<float> >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );    
      break;
    case PHIST_MVEC_C: 
      craftDbg(4, "addCpType::phist switch is PHIST_MVEC_C");
       rc = cp->addToMap(label, std::make_shared<CpPhistMvec<std::complex<float> > >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );     

      break;
    case PHIST_MVEC_D: 
      craftDbg(4, "addCpType::phist switch is PHIST_MVEC_D");
      rc = cp->addToMap(label, std::make_shared<CpPhistMvec<double>>
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;
    case PHIST_MVEC_Z: 
      craftDbg(4, "addCpType::phist switch is PHIST_MVEC_Z");
         rc = cp->addToMap(label, std::make_shared<CpPhistMvec< std::complex<double> > >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;


    case PHIST_SDMAT_S: 
      craftDbg(4, "addCpType::phist switch is PHIST_SDMAT_S");
        rc = cp->addToMap(label, std::make_shared<CpPhistSdMat<float> >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;

    case PHIST_SDMAT_C: 
      craftDbg(4, "addCpType::phist switch is PHIST_SDMAT_C");
        rc = cp->addToMap(label, std::make_shared<CpPhistSdMat<std::complex<float> > >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;

    case PHIST_SDMAT_D: 
      craftDbg(4, "addCpType::phist switch is PHIST_SDMAT_D");
        rc = cp->addToMap(label, std::make_shared<CpPhistSdMat<double> >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;

    case PHIST_SDMAT_Z: 
      craftDbg(4, "addCpType::phist switch is PHIST_SDMAT_Z");
        rc = cp->addToMap(label, std::make_shared<CpPhistSdMat<std::complex<double> > >
                                                      (phistPtr, 
                                                       cp->getCpComm(), 
                                                       cpAbleProp) 
                       );
      break;

  }
  if(rc){return EXIT_FAILURE;}
  return EXIT_SUCCESS;
}

#endif  // PHIST_CP

#ifdef MKL_CP
// ===== MKL_Complex8 ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex8 * const dataPtr,
              CpAbleProp cpAbleProp 
             )
{
    rc = cp->addToMap(label, std::make_shared<CpMklComplex8> ( dataPtr, cp->getCpComm(), cpAbleProp) );
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== MKL_Complex16 ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex16 * const dataPtr,
              CpAbleProp cpAbleProp 
             )
{
    rc = cp->addToMap(label, std::make_shared<CpMklComplex16> ( dataPtr, cp->getCpComm(), cpAbleProp ) );
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== MKL_Complex8 * (Array) ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex8 * const dataPtr, 
              const size_t nElem,
              CpAbleProp cpAbleProp 
             )
{
    rc = cp->addToMap(label, std::make_shared<CpMklComplex8Array> ( dataPtr, nElem, cp->getCpComm(), cpAbleProp) );
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== MKL_Complex16 * (Array) ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex16 * const dataPtr, 
              const size_t nElem,
              CpAbleProp cpAbleProp 
             )
{
    rc = cp->addToMap(label, std::make_shared<CpMklComplex16Array> ( dataPtr, nElem, cp->getCpComm(), cpAbleProp ) );
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}
#endif // MKL_CP

#ifdef GHOST_CP
// ===== GHOST DENSE MATRIX ===== //
int addCpType(Checkpoint * cp, std::string label, 
              ghost_densemat * const GDM,
              CpAbleProp cpAbleProp
             )
{
    rc = cp->addToMap(label, std::make_shared<CpGhostDenseMat>(GDM, cp->getCpComm(), cpAbleProp ));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

int addCpType(Checkpoint * cp, std::string label, 
              ghost_densemat ** const GDMArray, 
              const size_t nDenseMat_, 
              const int toCpDenseMat_,
              CpAbleProp cpAbleProp 
             )
{
    rc = cp->addToMap(label, std::make_shared<CpGhostDenseMatArray>(GDMArray, nDenseMat_, toCpDenseMat_, cp->getCpComm() , cpAbleProp) );
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== GHOST SPARSE MATRIX ===== // TODO: add this functionality if needed by users
//int Checkpoint::addCpType(Checkpoint * cp, std::string label, ghost_sparsemat * const GSM)
//{
//}

#endif // GHOST_CP





