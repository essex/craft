#include <cpHelperFuncs.hpp>
#include "phist_types.hpp"

template <typename ST>
class CpPhistSdMat : public CpBase
{
typedef phist::ScalarTraits<ST> st;

public:
	CpPhistSdMat(){}
	~CpPhistSdMat(){
    if( craftMakeAsyncCopy == true){
		  iflag = 0;
      phist::kernels<ST>::sdMat_delete(writePtr,&iflag);
    }
  }

	CpPhistSdMat<ST>( typename phist::types<ST>::sdMat_ptr dataPtr_, 
                      const MPI_Comm cpMpiComm_,
                      const CpAbleProp cpAbleProp_)
                      : dataPtr(dataPtr_),
                        CpBase(cpAbleProp_)
  {
    cpMpiComm = cpMpiComm_;
    
    if( craftMakeAsyncCopy == true){
		  iflag = 0;
		  int nr, nc;	
//		  phist_comm_ptr comm= NULL;				// TODO: comm needs to be different in the case of FT.
		  phist_comm_ptr comm= &cpMpiComm;				// TODO: comm needs to be different in the case of FT.
		  phist_comm_create(&comm, &iflag);
	
		  phist::kernels<ST>::sdMat_get_nrows(dataPtr,&nr,&iflag);
		  phist::kernels<ST>::sdMat_get_ncols(dataPtr,&nc,&iflag);
		  phist::kernels<ST>::sdMat_create(&asynData, nr, nc, comm, &iflag);
    }
    else{
      writePtr = dataPtr;
    }
	}

private:

	typename phist::types<ST>::sdMat_ptr dataPtr;
	typename phist::types<ST>::sdMat_ptr asynData;
	typename phist::types<ST>::sdMat_ptr writePtr;

	int iflag;

	int update(){
		craftDbg(3, "CpPhistSdMat::update() \n");
    PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_add_sdMat(st::one(),dataPtr,st::zero(),asynData,&iflag), iflag);
		return iflag;
	}

	int write( const std::string filename){
		craftDbg(3, "CpPhistSdMat:: i will write now %s\n", (filename).c_str());
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
		  PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_write_bin(writePtr, (filename).c_str(), cpMpiComm, &iflag), iflag);
		  return iflag;
    }
    else
    {
		  PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_write_bin(writePtr, (filename).c_str(), MPI_COMM_SELF, &iflag), iflag);
		  return iflag;
    }
	}
	
	int read(const std::string filename){
		craftDbg(3, "CpPhistSdMat:: i will read now %s\n", (filename).c_str());
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      //NOTE: Read and write operations are dependent on MPI_Comm. dataPtr and asynData have different MPI_Comm. Thus only that vector can be read, which has written it.k
  		PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_read_bin(writePtr, (filename).c_str(), cpMpiComm, &iflag), iflag);   
      if( craftMakeAsyncCopy == true){
        PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_add_sdMat(st::one(),writePtr,st::zero(),dataPtr,&iflag), iflag);   // check above note.
      }
  		return iflag;
    }
    else
    {
      PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_read_bin(writePtr, (filename).c_str(), MPI_COMM_SELF, &iflag), iflag);   
      if( craftMakeAsyncCopy == true){
        PHIST_ICHK_IERR(phist::kernels<ST>::sdMat_add_sdMat(st::one(),writePtr,st::zero(),dataPtr,&iflag), iflag);   // check above note.
      }
  		return iflag;
    }
	}

	
};

