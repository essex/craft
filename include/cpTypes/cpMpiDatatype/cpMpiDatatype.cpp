#include "cpMpiDatatype.hpp"
#include "../../cpEnum.h"
#include "include/craftConf.h"
#include "cpHelperFuncs.hpp"

CpMpiDatatype::CpMpiDatatype(void * buffPtr_, int numElem_, MPI_Datatype * mpiDataType_, const MPI_Comm cpMpiComm_)
{
  buffPtr = buffPtr_;
  numElem = numElem_;               // NOTE: if datatype is created via MPI_Type_create_struct. In an array of that datatype, each object can have different data-displacements. In that case, numElem should be 1. If in the array of that datatype, each object has static arrays, the data-displacements will be the same of each element of the datatype array. In that case numElems can be provided.
  mpiDataType = mpiDataType_;
	cpMpiComm = cpMpiComm_;
  MPI_Type_size(*mpiDataType, &typesize); 
	packedAsynDataSize = typesize*numElem;	
  craftDbg(4, "CpMpiDatatype::CpMpiDatatype: dataType size: %d, totalDataType size: %d", typesize, packedAsynDataSize); 
  if( craftMakeAsyncCopy == true){
    packedAsynData = new char[packedAsynDataSize];    // NOTE: only in case of ASYNC
  }

  cpHelperFuncs::calcSequentialOffset( &os, (typesize*numElem), cpMpiComm);
  craftDbg(4, "CpMpiDatatype::CpMpiDatatype: os : %d", os); 
}

 CpMpiDatatype::~CpMpiDatatype()
{
  if( craftMakeAsyncCopy == true){
    delete[] packedAsynData;      // NOTE: only in case of ASYNC
  }
}

int CpMpiDatatype::update(){
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  int position = 0 ; 
  MPI_Pack(buffPtr, numElem, *mpiDataType, packedAsynData, packedAsynDataSize, &position, cpMpiComm);
  
  craftDbg(4, "CpMpiDatatype::update: sizeof(packedAsynDataSize): %d, position: %d", packedAsynDataSize, position);

  return EXIT_SUCCESS;
}

int CpMpiDatatype::write( const std::string filename){
  
  MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  
  char * fname = new char[1024]; 
  sprintf(fname, "%s",  (filename).c_str());
	MPI_Barrier(cpMpiComm);
  

	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);

  if(rc) { 
    craftErr("CpMpiDatatype::write: unable to open file @ %s:%d",
              __FILE__, __LINE__
            );
    return EXIT_FAILURE; 
  }

  else{
    craftDbg(4, "CpMpiDatatype::write(): offset= %d", os);
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    
    if( craftMakeAsyncCopy == false){
      MPI_File_write(fh, buffPtr, numElem, *mpiDataType, &status);
    }
    else if( craftMakeAsyncCopy == true) {
      MPI_File_write(fh, packedAsynData, packedAsynDataSize, MPI_PACKED, &status); 
    }
	  MPI_File_close(&fh);
  }

	MPI_Barrier(cpMpiComm);

  sync();
  delete[] fname; 
  return EXIT_SUCCESS;
}

int CpMpiDatatype::read( const std::string filename){
  
  MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  
  char * fname = new char[1024]; 
  sprintf(fname, "%s",  (filename).c_str());
	MPI_Barrier(cpMpiComm);
  

	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);

  if(rc) { 
    craftErr("CpMpiDatatype::read: unable to open file @ %s:%d",
              __FILE__, __LINE__
            );
    return EXIT_FAILURE; 
  }

  else{
    craftDbg(4, "CpMpiDatatype::read(): offset= %d", os);
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    
    if( craftMakeAsyncCopy == false){
      MPI_File_read(fh, buffPtr, numElem, *mpiDataType, &status);
    }
    else if( craftMakeAsyncCopy == true) {
      MPI_File_read(fh, packedAsynData, packedAsynDataSize, MPI_PACKED, &status); 
      int position = 0;
      MPI_Unpack(packedAsynData, packedAsynDataSize, &position, buffPtr, numElem, *mpiDataType, cpMpiComm);
      craftDbg(4, "CpMpiDatatype::read(): position after read %d", position);
    }
    
	  MPI_File_close(&fh);
  }

	MPI_Barrier(cpMpiComm);

  sync();
  delete[] fname; 

  return EXIT_SUCCESS;
}

