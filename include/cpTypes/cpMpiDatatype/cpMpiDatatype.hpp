#ifndef __CPMPIDATATYPE_HPP__
#define __CPMPIDATATYPE_HPP__

#include <cstdlib>
#include <fstream>
#include <iostream>
#include "cpHelperFuncs.hpp"

#include "../../cpEnum.h"
#include "../../cpBase.hpp"

// ===== MPI_Datatype ===== //

class CpMpiDatatype: public CpBase
{
public: 
  CpMpiDatatype(void * buffPtr_, int numElem_, MPI_Datatype * mpiDataType_, const MPI_Comm cpMpiComm);
  ~CpMpiDatatype();

private: 
  MPI_Datatype * mpiDataType;
  char * packedAsynData;          // NOTE: data will only be defined in case of Async
  int numElem; 
  void * buffPtr;
  int update(); 
  int write( const std::string filename);
	int read(const std::string filename);
  
  int typesize;
  int packedAsynDataSize;

	MPI_Offset os;
  

};

#endif
