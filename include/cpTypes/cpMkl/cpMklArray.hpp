#ifndef __CPMKLARRAY_HPP__
#define __CPMKLARRAY_HPP__

#include <cstdlib>

#include "../../cpBase.hpp"
#include "mkl.h"

template <class T>
static void copyMklArray(T * const src, T * const dst, const size_t nElem_){
	for(size_t i= 0; i< nElem_; ++i){
    dst[i].real = src[i].real;    
    dst[i].imag = src[i].imag;    
	}
  return;
}

// ===== MKL_Complex8 * (Array) ===== //
class CpMklComplex8Array: public CpBase  
{
public:
	CpMklComplex8Array(MKL_Complex8 *  dataPtr_, const size_t nElem_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_ );
	~CpMklComplex8Array();
private:
	MKL_Complex8 * dataPtr;
	MKL_Complex8 * asynData;
	MKL_Complex8 * writePtr;
  size_t nElem; 

	int update();
	int write( const std::string filename);
	int read(const std::string filename);

  int readParallel(const std::string filename);
	int readSerial(const std::string filename);
	int writeParallel(const std::string filename);
	int writeSerial(const std::string filename);
	
  MPI_Offset os;
};

// ===== MKL_Complex16 * (Array) ===== //
class CpMklComplex16Array: public CpBase  
{
public:
	CpMklComplex16Array(MKL_Complex16 *  dataPtr_, const size_t nElem_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpMklComplex16Array();
	int update();
	int write( const std::string filename);
	int read(const std::string filename);

private:
	MKL_Complex16 * dataPtr;
	MKL_Complex16 * asynData;
	MKL_Complex16 * writePtr;
  size_t nElem; 

  int readParallel(const std::string filename);
	int readSerial(const std::string filename);
	int writeParallel(const std::string filename);
	int writeSerial(const std::string filename);
  
  MPI_Offset os;
};




#endif  // __CPMKLARRAY_HPP__
