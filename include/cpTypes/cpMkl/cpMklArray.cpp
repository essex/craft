#include "cpMklArray.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include "cpHelperFuncs.hpp"
#include "dataType.h"

#include "../../cpEnum.h"
#include "../../cpBase.hpp"
#include "mkl.h"


CpMklComplex8Array::CpMklComplex8Array( MKL_Complex8 *  dataPtr_, 
                                        const size_t nElem_, 
                                        const MPI_Comm cpMpiComm_,
                                        CpAbleProp cpAbleProp_)
                                        : dataPtr (dataPtr_), 
                                          nElem (nElem_),
                                          CpBase  (cpAbleProp_)
{
  cpMpiComm = cpMpiComm_;
  if( craftMakeAsyncCopy == true){
    asynData  = new MKL_Complex8[nElem];
    copyMklArray(dataPtr, asynData, nElem);
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
  int sizeofMKL_Complex8Array = 2*sizeof(float)*nElem;
  cpHelperFuncs::calcSequentialOffset( &os, sizeofMKL_Complex8Array, cpMpiComm);
}

CpMklComplex8Array::~CpMklComplex8Array()
{
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
}

int CpMklComplex8Array::update(){
  copyMklArray(dataPtr, asynData, nElem);
  return EXIT_SUCCESS;
}

int CpMklComplex8Array::read(const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  readParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  readSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex8Array::write( const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  writeParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  writeSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex8Array::writeParallel(const std::string filename){
	craftDbg(2, "CpMklComplex8Array::writeParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	MPI_File_open(cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
	MPI_File_seek(fh, os, MPI_SEEK_SET);
	for(size_t i= 0; i< nElem; ++i){
    MPI_File_write(fh, &(writePtr[i].real), 1, getMpiDataType(float), &status);
    MPI_File_write(fh, &(writePtr[i].imag), 1, getMpiDataType(float), &status);
  }
	MPI_File_close(&fh);
  sync();
	return EXIT_SUCCESS;
}

int CpMklComplex8Array::writeSerial(const std::string filename){ // TODO: check
	craftDbg(2, "CpMklComplex8Array::writeSerial()");
	std::ofstream fstr;
   
	fstr.open ((filename).c_str(), std::ofstream::binary );	
	if(fstr.is_open()){
	  for(size_t i= 0; i< nElem; ++i){
		  fstr.write( (char *)&(writePtr[i].real), sizeof (float) );
		  fstr.write( (char *)&(writePtr[i].imag), sizeof (float) );
    }
		fstr.close();
    sync();
	}
	else{
	  int myrank;
	  MPI_Comm_rank(cpMpiComm, &myrank);
		std::cerr << myrank << "Can't open file -- " << filename << std::endl;			
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}


int CpMklComplex8Array::readParallel(const std::string filename){
	craftDbg(2, "CpMklComplex8Array::readParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	MPI_File_open(cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
	MPI_File_seek(fh, os, MPI_SEEK_SET);
	for(size_t i= 0; i< nElem; ++i){
    MPI_File_read(fh, &(dataPtr[i].real), 1, getMpiDataType(float), &status);
    MPI_File_read(fh, &(dataPtr[i].imag), 1, getMpiDataType(float), &status);
  }
	MPI_File_close(&fh);
	return EXIT_SUCCESS;
}


int CpMklComplex8Array::readSerial(const std::string filename){
	craftDbg(2, "CpMklComplex8Array::readSerial()");
	std::ifstream fstr;
	fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
	if(fstr.is_open()){
	  for(size_t i= 0; i< nElem; ++i){
		  fstr.read( (char *)&(dataPtr[i].real), sizeof (float) );
		  fstr.read( (char *)&(dataPtr[i].imag), sizeof (float) );
    }
		fstr.close();
	}
	else{
		std::cerr << "Can't open file " << filename << std::endl;			
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

// ===== MKL_Complex16 * (Array) ===== //
CpMklComplex16Array::CpMklComplex16Array( MKL_Complex16 *  dataPtr_, 
                                          const size_t nElem_, 
                                          const MPI_Comm cpMpiComm_,
                                          CpAbleProp cpAbleProp_)
                                          : dataPtr (dataPtr_), 
                                            nElem (nElem_),
                                            CpBase  (cpAbleProp_)
{
  cpMpiComm = cpMpiComm_;
  
  if( craftMakeAsyncCopy == true){
    asynData  = new MKL_Complex16[nElem];
    copyMklArray(dataPtr, asynData, nElem);
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
  int sizeofMKL_Complex16Array = 2*sizeof(double)*nElem;
  cpHelperFuncs::calcSequentialOffset( &os, sizeofMKL_Complex16Array, cpMpiComm);

}

CpMklComplex16Array::~CpMklComplex16Array()
{
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
}


int CpMklComplex16Array::update(){
  copyMklArray(dataPtr, asynData, nElem);
  return EXIT_SUCCESS;
}

int CpMklComplex16Array::read(const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  readParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  readSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex16Array::write( const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  writeParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  writeSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex16Array::writeParallel(const std::string filename){
	craftDbg(2, "CpMklComplex16Array::writeParallel()");
	MPI_File fh; 
	int myrank;
	MPI_Comm_rank(cpMpiComm, &myrank);
	MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	MPI_File_open(cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
	MPI_File_seek(fh, os, MPI_SEEK_SET);
	for(size_t i= 0; i< nElem; ++i){
    MPI_File_write(fh, &(writePtr[i].real), 1, getMpiDataType(double), &status);
    MPI_File_write(fh, &(writePtr[i].imag), 1, getMpiDataType(double), &status);
  }
	MPI_File_close(&fh);
  sync();
	return EXIT_SUCCESS;
}

int CpMklComplex16Array::writeSerial(const std::string filename){ // TODO: check
	craftDbg(2, "CpMklComplex16Array::writeSerial()");
	std::ofstream fstr;
   
	fstr.open ((filename).c_str(), std::ofstream::binary );	
	if(fstr.is_open()){
	  for(size_t i= 0; i< nElem; ++i){
		  fstr.write( (char *)&(writePtr[i].real), sizeof (double) );
		  fstr.write( (char *)&(writePtr[i].imag), sizeof (double) );
    }
		fstr.close();
    sync();
	}
	else{
	  int myrank;
	  MPI_Comm_rank(cpMpiComm, &myrank);
		std::cerr << myrank << "Can't open file -- " << filename << std::endl;			
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}


int CpMklComplex16Array::readParallel(const std::string filename){
	craftDbg(2, "CpMklComplex16Array::readParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	MPI_File_open(cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
	MPI_File_seek(fh, os, MPI_SEEK_SET);
	for(size_t i= 0; i< nElem; ++i){
    MPI_File_read(fh, &(dataPtr[i].real), 1, getMpiDataType(double), &status);
    MPI_File_read(fh, &(dataPtr[i].imag), 1, getMpiDataType(double), &status);
  }
	MPI_File_close(&fh);
	return EXIT_SUCCESS;
}


int CpMklComplex16Array::readSerial(const std::string filename){
	craftDbg(2, "CpMklComplex16Array::readSerial()");
	std::ifstream fstr;
	fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
	if(fstr.is_open()){
	  for(size_t i= 0; i< nElem; ++i){
		  fstr.read( (char *)&(dataPtr[i].real), sizeof (double) );
		  fstr.read( (char *)&(dataPtr[i].imag), sizeof (double) );
    }
		fstr.close();
	}
	else{
		std::cerr << "Can't open file " << filename << std::endl;			
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
