#ifndef __CPMKL_HPP__
#define __CPMKL_HPP__

#include <cstdlib>


#include "../../cpBase.hpp"
#include "mkl.h"

// ===== MKL_Complex8 ===== //
class CpMklComplex8: public CpBase  
{
public:
	CpMklComplex8(MKL_Complex8 *  dataPtr_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpMklComplex8();
private:
	MKL_Complex8 * dataPtr;
	MKL_Complex8 * asynData;
	MKL_Complex8 * writePtr;

	int update();
	int write( const std::string filename);
	int read(const std::string filename);

  int readParallel(const std::string filename);
	int readSerial(const std::string filename);
	int writeParallel(const std::string filename);
	int writeSerial(const std::string filename);
};

// ===== MKL_Complex16 ===== //
class CpMklComplex16: public CpBase  
{
public:
	CpMklComplex16(MKL_Complex16 *  dataPtr_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpMklComplex16();
	int update();
	int write( const std::string filename);
	int read(const std::string filename);

private:
	MKL_Complex16 * dataPtr;
	MKL_Complex16 * asynData;
	MKL_Complex16 * writePtr;

  int readParallel(const std::string filename);
	int readSerial(const std::string filename);
	int writeParallel(const std::string filename);
	int writeSerial(const std::string filename);
};




#endif
