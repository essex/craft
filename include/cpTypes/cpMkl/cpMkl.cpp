#include "cpMkl.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include "cpHelperFuncs.hpp"
#include "dataType.h"


#include "../../cpEnum.h"
#include "../../cpBase.hpp"
#include "mkl.h"



CpMklComplex8::CpMklComplex8 (  MKL_Complex8 *  dataPtr_, 
                                const MPI_Comm cpMpiComm_, 
                                CpAbleProp cpAbleProp_)
                                : dataPtr (dataPtr_), 
                                  CpBase  (cpAbleProp_)
{
  cpMpiComm = cpMpiComm_;
  if( craftMakeAsyncCopy == true){
    asynData  = new MKL_Complex8[1];
	  *asynData = *dataPtr;
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
}

CpMklComplex8::~CpMklComplex8()
{
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
}

int CpMklComplex8::update(){
  *asynData = *dataPtr;
  return EXIT_SUCCESS;
}

int CpMklComplex8::read(const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  readParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  readSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex8::write( const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  writeParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  writeSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex8::writeParallel(const std::string filename){
	craftDbg(4, "CpMklComplex8::writeParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("CpMklComplex8::writeParallel: unable to open file @ %s:%d",
              __FILE__, __LINE__);
    return EXIT_FAILURE; 
  }
  else{
	  MPI_Offset os;
    int sizeofMKL_Complex8 = 2*sizeof(float);
	  os = myrank * sizeofMKL_Complex8;
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    MPI_File_write(fh, &(writePtr->real), 1, getMpiDataType(float), &status);
    MPI_File_write(fh, &(writePtr->imag), 1, getMpiDataType(float), &status);
	  MPI_File_close(&fh);
    sync();
  }
	return EXIT_SUCCESS;
}

int CpMklComplex8::writeSerial(const std::string filename){
	craftDbg(4, "CpMklComplex8::writeSerial()");
	std::ofstream fstr;
   
	fstr.open ((filename).c_str(), std::ofstream::binary );	
	if(fstr.is_open()){
		fstr.write( (char *)&(writePtr->real), sizeof (float) );
		fstr.write( (char *)&(writePtr->imag), sizeof (float) );
		fstr.close();
    sync();
	}
	else{
    craftErr("CpMklComplex8::writeSerial: unable to open file @ %s:%d",
              __FILE__, __LINE__);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int CpMklComplex8::readParallel(const std::string filename){
	craftDbg(4, "CpMklComplex8::readParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("CpMklComplex8::readParallel: unable to open file @ %s:%d",
              __FILE__, __LINE__);
    return EXIT_FAILURE; 
  }
  else{
	  MPI_Offset os;
    int sizeofMKL_Complex8 = 2*sizeof(float); // as MKL_Complex8 as 2 float data members.
	  os = myrank * sizeofMKL_Complex8;	
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    MPI_File_read(fh, &(dataPtr->real), 1, getMpiDataType(float), &status);
    MPI_File_read(fh, &(dataPtr->imag), 1, getMpiDataType(float), &status);
	  MPI_File_close(&fh);
  }
	return EXIT_SUCCESS;
}

int CpMklComplex8::readSerial(const std::string filename){
	craftDbg(4, "CpMklComplex8::readSerial()");
	std::ifstream fstr;
	fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
	if(fstr.is_open()){
		fstr.read( (char *)&(dataPtr->real), sizeof (float) );
		fstr.read( (char *)&(dataPtr->imag), sizeof (float) );
		fstr.close();
	}
	else{
    craftErr("CpMklComplex8::readSerial: unable to open file @ %s:%d",
              __FILE__, __LINE__);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

// ===== MKL_Complex16 ===== //
CpMklComplex16::CpMklComplex16 (  MKL_Complex16 *  dataPtr_, 
                                  const MPI_Comm cpMpiComm_,
                                  CpAbleProp cpAbleProp_)
                                  : dataPtr (dataPtr_), 
                                    CpBase  (cpAbleProp_)

{
  cpMpiComm = cpMpiComm_;
  
  if( craftMakeAsyncCopy == true){
    asynData  = new MKL_Complex16[1];
	  *asynData = *dataPtr;
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
}

CpMklComplex16::~CpMklComplex16()
{
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
}

int CpMklComplex16::update(){
  *asynData = *dataPtr;
  return EXIT_SUCCESS;
}

int CpMklComplex16::read(const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  readParallel(filename)  );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  readSerial(filename)  );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex16::write( const std::string filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR(  writeParallel(filename) );
  }
  else 
  {
    CRAFT_CHECK_ERROR(  writeSerial(filename) );
  }
  return EXIT_SUCCESS;
}

int CpMklComplex16::writeParallel(const std::string filename){
	craftDbg(4, "CpMklComplex16::writeParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("CpMklComplex16::writeParallel: unable to open file @ %s:%d",
              __FILE__, __LINE__);
    return EXIT_FAILURE; 
  }
  else{
	  MPI_Offset os;
    int sizeofMKL_Complex16 = 2*sizeof(double);
	  os = myrank * sizeofMKL_Complex16;
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    MPI_File_write(fh, &(writePtr->real), 1, getMpiDataType(double), &status);
    MPI_File_write(fh, &(writePtr->imag), 1, getMpiDataType(double), &status);
	  MPI_File_close(&fh);
    sync();
  }
	return EXIT_SUCCESS;
}

int CpMklComplex16::writeSerial(const std::string filename){
	craftDbg(4, "CpMklComplex16::writeSerial()");
	std::ofstream fstr;
   
	fstr.open ((filename).c_str(), std::ofstream::binary );	
	if(fstr.is_open()){
		fstr.write( (char *)&(writePtr->real), sizeof (double) );
		fstr.write( (char *)&(writePtr->imag), sizeof (double) );
		fstr.close();
    sync();
	}
	else{
    craftErr("CpMklComplex16::writeSerial: unable to open file @ %s:%d",
              __FILE__, __LINE__);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int CpMklComplex16::readParallel(const std::string filename){
	craftDbg(4, "CpMklComplex16::readParallel()");
	MPI_File fh; 
	MPI_Status status;
	int myrank, rc;
	MPI_Comm_rank(cpMpiComm, &myrank);
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open(cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("CpMklComplex16::readParallel: unable to open file @ %s:%d",
              __FILE__, __LINE__);
    return EXIT_FAILURE; 
  }
  else{
	  MPI_Offset os;
    int sizeofMKL_Complex16 = 2*sizeof(double); // as MKL_Complex16 as 2 double data members.
	  os = myrank * sizeofMKL_Complex16;	
	  MPI_File_seek(fh, os, MPI_SEEK_SET);
    MPI_File_read(fh, &(dataPtr->real), 1, getMpiDataType(double), &status);
    MPI_File_read(fh, &(dataPtr->imag), 1, getMpiDataType(double), &status);
	  MPI_File_close(&fh);
  }
	return EXIT_SUCCESS;
}

int CpMklComplex16::readSerial(const std::string filename){
	craftDbg(4, "CpMklComplex16::readSerial()");
	std::ifstream fstr;
	fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
	if(fstr.is_open()){
		fstr.read( (char *)&(dataPtr->real), sizeof (double) );
		fstr.read( (char *)&(dataPtr->imag), sizeof (double) );
		fstr.close();
	}
	else{
    craftErr("CpMklComplex16::readSerial: unable to open file @ %s:%d",
              __FILE__, __LINE__);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}
