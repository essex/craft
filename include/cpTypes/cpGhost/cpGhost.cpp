#include "cpGhost.hpp"

#include <cstdlib>
#include <fstream>
#include <iostream>

#include <ghost.h>
#include <ghost/types.h>

#include "../../cpEnum.h"
#include "../../cpBase.hpp"
#include "cpHelperFuncs.hpp"

CpGhostDenseMat::CpGhostDenseMat( ghost_densemat *  dataPtr_, 
                                  const MPI_Comm cpMpiComm_,
                                  CpAbleProp cpAbleProp_)
	                                : dataPtr   (dataPtr_),
                                    CpBase    (cpAbleProp_)
{
  cpMpiComm = cpMpiComm_;
  if( craftMakeAsyncCopy == true){
	  ghost_densemat_create(&asynData, dataPtr->map, dataPtr->traits);
	  ghost_densemat_init_densemat(asynData, dataPtr, 0, 0);
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
}

CpGhostDenseMat::~CpGhostDenseMat(){
  if( craftMakeAsyncCopy == true){
    delete asynData;
  }
}

int CpGhostDenseMat::update(){
//  ghost_densemat_init_densemat(asynData, dataPtr, 0, 0);    // TODO: NOTE: this copy operation seems to be very expensive. Thus using memcpy instead.
  craftDbg(3, "CpGhostDenseMat: updating now");
  memcpy(asynData->val, dataPtr->val, (dataPtr->map->dim) * dataPtr->elSize);
  return EXIT_SUCCESS;
}

int CpGhostDenseMat::write( const std::string filename){
  craftDbg(3, "CpGhostDenseMat: writing now in %s", (filename).c_str());
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr,(char *) (filename).c_str(), cpMpiComm));
//    GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr,(char *) (filename).c_str(), writePtr->map->mpicomm)); // writePtr->map->mpicomm should not be used, so that async threads as well as worker threads could call MPI collectives independently.
  }
  else
  {
    GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr,(char *) (filename).c_str(), MPI_COMM_SELF));
  }
  return EXIT_SUCCESS;
}

int CpGhostDenseMat::read(const std::string filename){
  craftDbg(3, "CpGhostDenseMat: reading now in %s", (filename).c_str());
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr,(char *) (filename).c_str(), cpMpiComm));
  }
  else 
  {
    GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr,(char *) (filename).c_str(), MPI_COMM_SELF));
  }
  return EXIT_SUCCESS;
}


CpGhostDenseMatArray::CpGhostDenseMatArray( ghost_densemat **  dataPtr_, 
                                            const size_t nDenseMat_, 
                                            const int toCpDenseMat_, 
                                            const MPI_Comm cpMpiComm_,
                                            CpAbleProp cpAbleProp_
                                          ):  dataPtr       (dataPtr_),
                                              nDenseMat     (nDenseMat_),
                                              toCpDenseMat  (toCpDenseMat_),
                                              CpBase        (cpAbleProp_)
{
  cyclicCpCounter = 0;
  cpMpiComm = cpMpiComm_;

  if( craftMakeAsyncCopy == true){
    asynData = new ghost_densemat*[nDenseMat];
    for(size_t i = 0; i < nDenseMat ; ++i)
    {
      ghost_densemat_create( &asynData[i], dataPtr[i]->map, dataPtr[i]->traits);
      ghost_densemat_init_densemat ( asynData[i], dataPtr[i], 0, 0);	
    }
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
}

CpGhostDenseMatArray::~CpGhostDenseMatArray(){
  if( craftMakeAsyncCopy == true){
    asynData = new ghost_densemat*[nDenseMat];
    for(size_t i = 0; i < nDenseMat ; ++i)
    {
      delete asynData[i];
    }
    delete asynData;
  }
}

int CpGhostDenseMatArray::update(){
  craftDbg(3, "CpGhostDenseMatArray: updating now");
  for(size_t i = 0; i < nDenseMat ; ++i)
  {
    GHOST_CALL_RETURN(ghost_densemat_init_densemat ( asynData[i], dataPtr[i], 0, 0));	
  }
  return EXIT_SUCCESS;
}

int CpGhostDenseMatArray::write( const std::string filename){
  craftDbg(3, "CpGhostDenseMatArray: writing now in %s", (filename).c_str());
  if(toCpDenseMat == ALL){
    craftDbg(3, "CpGhostDenseMatArray::write() toCpDenseMat is ALL");
    for(size_t i = 0; i < nDenseMat ; ++i)
    {
      std::string filename_ = filename + cpHelperFuncs::numberToString(i);
      if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
      {
        GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[i],(char *) (filename_).c_str(), cpMpiComm));
      }
      else
      {
        GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[i],(char *) (filename_).c_str(), MPI_COMM_SELF));
      }
    }
  }
  else if(toCpDenseMat == CYCLIC){		// TODO: testing to be done
    craftDbg(3, "CpGhostDenseMatArray::write() cyclicCpCounter = %d", cyclicCpCounter);
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[cyclicCpCounter],(char *) (filename).c_str(), cpMpiComm));
    }
    else
    {
      GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[cyclicCpCounter],(char *) (filename).c_str(), MPI_COMM_SELF));
    }
    // ===== write the metadata file for cyclicCpCounter ===== // 

    std::string filenameMD;
    filenameMD = filename + ".metadata";
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      CRAFT_MPI_File_write_POD(filenameMD, &cyclicCpCounter, cpMpiComm);
    }
    else
    {
      CRAFT_SERIAL_ASCII_File_write_POD(filenameMD, &cyclicCpCounter);

    }
    craftDbg(3, "CpGhostDenseMatArray::write(): cyclicCpCounter is = %d", cyclicCpCounter);
    ++cyclicCpCounter;
    if( cyclicCpCounter == nDenseMat ){
      cyclicCpCounter = 0;	
    }
  }
  else if(toCpDenseMat	>= 0){
    craftDbg(3, "CpGhostDenseMatArray::write() toCpDenseMat is: %d", toCpDenseMat);
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[toCpDenseMat],(char *) (filename).c_str(), cpMpiComm));
    }
    else
    {
      GHOST_CALL_RETURN(ghost_densemat_to_file(writePtr[toCpDenseMat],(char *) (filename).c_str(), MPI_COMM_SELF));
    }
  }
  else{  
    craftErr("CpGhostDenseMatArray::write: toCpDenseMat is not specified correctly @ %s:%d",
              __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

int CpGhostDenseMatArray::read(const std::string filename){
  craftDbg(3, "CpGhostDenseMatArray: reading now in %s", (filename).c_str());
  if(toCpDenseMat == ALL){
    craftDbg(3, "CpGhostDenseMatArray::read(): toCpDenseMat is ALL");
    for(size_t i = 0; i < nDenseMat ; ++i)
    {
      std::string filename_ = filename + cpHelperFuncs::numberToString(i);
      if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[i],(char *) (filename_).c_str(), cpMpiComm));
      }
      else 
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[i],(char *) (filename_).c_str(), MPI_COMM_SELF));
      }
    }
  }
  else if(toCpDenseMat == CYCLIC){			// TODO: testing to be done
      // ===== read the metadata file for cyclicCpCounter ===== // 
    std::string filenameMD;
    filenameMD = filename + ".metadata";
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
    
      CRAFT_MPI_File_read_POD(filename, &cyclicCpCounter, cpMpiComm);
    }
    else
    {
      CRAFT_SERIAL_ASCII_File_read_POD(filename, &cyclicCpCounter);
    }
      
      craftDbg(3, "CpGhostDenseMatArray::read(): cyclicCpCounter is = %d", cyclicCpCounter);
      if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[cyclicCpCounter],(char *) (filename).c_str(), cpMpiComm));
      }
      else 
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[cyclicCpCounter],(char *) (filename).c_str(), MPI_COMM_SELF));
      }
      ++cyclicCpCounter;
      if( cyclicCpCounter == nDenseMat ){
        cyclicCpCounter = 0;	
      }
  }
  else if( toCpDenseMat >= 0 ){
    craftDbg(3, "CpGhostDenseMatArray::read(): toCpDenseMat is = %d", toCpDenseMat);
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[toCpDenseMat],(char *) (filename).c_str(), cpMpiComm));
      }
      else 
      {
        GHOST_CALL_RETURN(ghost_densemat_init_file(dataPtr[toCpDenseMat],(char *) (filename).c_str(), MPI_COMM_SELF));
      }
  }
  else{
    craftErr("CpGhostDenseMatArray::read: toCpDenseMat is not specified correctly @ %s:%d",
              __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

CpGhostSparseMat::CpGhostSparseMat(ghost_sparsemat *  dataPtr_){
  dataPtr = dataPtr_;
  craftErr("CpGhostSparseMat checkpoints are not supported yet. @ %s:%d",
              __FILE__, __LINE__);
}

int CpGhostSparseMat::update(){
  craftErr("CpGhostSparseMat checkpoints are not supported yet. @ %s:%d",
              __FILE__, __LINE__);
  return EXIT_FAILURE;
}

int CpGhostSparseMat::write( const std::string filename){
  craftErr("CpGhostSparseMat checkpoints are not supported yet. @ %s:%d",
              __FILE__, __LINE__);
  return EXIT_FAILURE;
}

int CpGhostSparseMat::read(const std::string filename){
  craftErr("CpGhostSparseMat checkpoints are not supported yet. @ %s:%d",
              __FILE__, __LINE__);
  return EXIT_FAILURE;
}
