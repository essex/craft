#ifndef __CPGHOST_HPP__
#define __CPGHOST_HPP__

#include <cstdlib>
#include <fstream>
#include <iostream>

#include <ghost.h>
#include <ghost/types.h>

#include "../../cpEnum.h"
#include "../../cpBase.hpp"
#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"

class CpGhostDenseMat: public CpBase  
{
public:
	CpGhostDenseMat(ghost_densemat *  dataPtr_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_ );
	~CpGhostDenseMat();
	int update();
	int write( const std::string filename);
	int read(const std::string filename);

private:
	ghost_densemat * dataPtr;
	ghost_densemat * asynData;
	ghost_densemat * writePtr;
};


class CpGhostDenseMatArray: public CpBase  
{
public:
	CpGhostDenseMatArray(ghost_densemat **  dataPtr_, const size_t nDenseMat_, const int toCpDenseMat_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_ );
	~CpGhostDenseMatArray();

	int update();
	int write( const std::string filename);
	int read(const std::string filename);

private:
	ghost_densemat ** dataPtr;
	ghost_densemat ** asynData;
	ghost_densemat ** writePtr;
	size_t nDenseMat;
	int toCpDenseMat; 
	size_t cyclicCpCounter;
};


class CpGhostSparseMat: public CpBase  	// TODO: not fully implemented
{
public:
	CpGhostSparseMat(ghost_sparsemat *  dataPtr_);
	~CpGhostSparseMat(){}	
	int update();
	int write( const std::string filename);
	int read(const std::string filename);

private:;
	ghost_sparsemat * dataPtr;
	ghost_sparsemat * asynData;
};


#endif
