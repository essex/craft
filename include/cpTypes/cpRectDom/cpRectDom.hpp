#include "../../cpEnum.h"
#include "../../cpBase.hpp"
#include <rectDomain.h>

class cpRectDomain: public CpBase
{
public:
//  cpRectDomain();
  cpRectDomain  ( rectDomain * dataPtr_, 
                  const MPI_Comm cpMpiComm_=MPI_COMM_WORLD, 
                  CpAbleProp cpAbleProp_=CpAbleProp() )
                  : dataPtr(dataPtr_), 
                    CpBase (cpAbleProp_)
  {
//    asynData = new rectDomain[1];
    *asynData = *dataPtr_;
  }
	~cpRectDomain(){}	
private:
	rectDomain * dataPtr;
	rectDomain * asynData;

  int update();
	int write(const std::string * filename);
	int read(const std::string * filename);
};

int cpRectDomain::update(){
  if( craftMakeAsyncCopy == true){
    printf("cpRectDomain updating\n");  
  }
  else
  {
    printf("cpRectDomain::update() update is not needed -> craftMakeAsyncCopy = false \n");  
  }
  return 0;
}

int cpRectDomain::write(const std::string * filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  { 
    printf("cpRectDomain::write: write with MPIIO\n");  
  }
  else if (cpAbleProp.getIOType() == SERIALIO) 
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// read in Binary format.  default = BIN
    {
      printf("cpRectDomain::write: write with SERIALIO, BIN\n");  
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      printf("cpRectDomain::write: write with SERIALIO, ASCII \n");  
    }
  }
  return 0;
}

int cpRectDomain::read(const std::string * filename){
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  { 
    printf("cpRectDomain::read: read with MPIIO\n");  
  }
  else if (cpAbleProp.getIOType() == SERIALIO) 
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// read in Binary format.  default = BIN
    {
      printf("cpRectDomain::read: read with SERIALIO, BIN\n");  
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      printf("cpRectDomain::read: read with SERIALIO, ASCII \n");  
    }
  }
  return 0;
}
