#include <cpHelperFuncs.hpp>
#include "phist_types.hpp"

template <typename ST>
class CpPhistMvec : public CpBase
{

typedef phist::ScalarTraits<ST> st;

public:
	CpPhistMvec(){}
	~CpPhistMvec(){
    if( craftMakeAsyncCopy == true){
		  iflag = 0;
      PHIST_CHK_IERR(phist::kernels<ST>::mvec_delete(writePtr,  &iflag), iflag);
    }
  }	

	CpPhistMvec<ST>( typename phist::types<ST>::mvec_ptr dataPtr_, 
                      const MPI_Comm cpMpiComm_, 
                      const CpAbleProp cpAbleProp_)
                      :
                        CpBase(cpAbleProp_)

  {
    cpMpiComm = cpMpiComm_;
    dataPtr   = dataPtr_;

		craftDbg(3, "CpPhistMvec::CpPhistMvec \n %s\n", __PRETTY_FUNCTION__ );
    if( craftMakeAsyncCopy == true){
		  iflag = 0;
      int nVec=0;
		  phist::kernels<ST>::mvec_num_vectors(dataPtr,&nVec,&iflag);
		  phist_const_map_ptr map=NULL;
		  phist::kernels<ST>::mvec_get_map(dataPtr, &map, &iflag); 
  	  phist::kernels<ST>::mvec_create(&asynData,map,nVec,&iflag);
      phist::kernels<ST>::mvec_put_value(asynData,st::zero(),&iflag);
		  phist::kernels<ST>::mvec_add_mvec(st::one(),dataPtr,st::zero(),asynData,&iflag);
      writePtr = asynData;

    }
    else{
      writePtr = dataPtr;
    }
	}

private:

	typename phist::types<ST>::mvec_ptr dataPtr;
	typename phist::types<ST>::mvec_ptr asynData;
	typename phist::types<ST>::mvec_ptr writePtr;

	int iflag;

	int update(){
		craftDbg(3, "CpPhistMvec::update \n");
		PHIST_ICHK_IERR(phist::kernels<ST>::mvec_add_mvec(st::one(),dataPtr,st::zero(),asynData,&iflag),iflag);
		return iflag;
	}

	int write( const std::string filename){
    craftDbg(3, "CpPhistMvec: i will write now %s\n", (filename).c_str());
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
  		PHIST_ICHK_IERR(phist::kernels<ST>::mvec_write_bin(writePtr, (filename).c_str(), cpMpiComm, &iflag), iflag);  
  		return iflag;
    }
    else
    {
  		PHIST_ICHK_IERR(phist::kernels<ST>::mvec_write_bin(writePtr, (filename).c_str(), MPI_COMM_SELF, &iflag), iflag);  
  		return iflag;
    }
	}
	
	int read(const std::string filename){
		craftDbg(3, "CpPhistMvec: i will read now %s\n", (filename).c_str());
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      //NOTE: Read and write operations are dependent on MPI_Comm. dataPtr and asynData have different MPI_Comm. Thus only that vector can be read, which has written it.k
		  PHIST_ICHK_IERR(phist::kernels<ST>::mvec_read_bin(writePtr, (filename).c_str(), cpMpiComm ,&iflag),iflag); 
      if( craftMakeAsyncCopy == true){
        PHIST_ICHK_IERR(phist::kernels<ST>::mvec_add_mvec(st::one(),writePtr,st::zero(),dataPtr,&iflag),iflag);    // check above note.
      }
		  return iflag;
    }
    else
    {
      PHIST_ICHK_IERR(phist::kernels<ST>::mvec_read_bin(writePtr, (filename).c_str(), MPI_COMM_SELF, &iflag),iflag);   		  
      if( craftMakeAsyncCopy == true){
        PHIST_ICHK_IERR(phist::kernels<ST>::mvec_add_mvec(st::one(),writePtr,st::zero(),dataPtr,&iflag),iflag);    // check above note.
      }
    }
	}
};

