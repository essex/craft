#ifndef __CPARRAY_HPP__
#define __CPARRAY_HPP__

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <assert.h>

#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "cpEnum.h"
#include "cpBase.hpp"


// ===== POD ARRAY ===== // 
template <class T>
class CpArray: public CpBase
{
public:
 CpArray( T * const dataPtr_, size_t * const nElems_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpArray();

private:
	T * dataPtr;
	T * asynData;
	T * writePtr;
	size_t * nElems;
  size_t asyncNElems;
  size_t * writeNElems;

  int read(const std::string filename);
  int write(const std::string filename);
	int update();
	void print();
  
  MPI_Offset os;

};

// ===== POD ARRAY function implementations ===== // 
template <class T>
CpArray<T>::CpArray(  T * const dataPtr_, 
                      size_t * const nElems_, 
                      const MPI_Comm cpMpiComm_, 
                      CpAbleProp cpAbleProp_)
                      : dataPtr(dataPtr_),
                        nElems(nElems_),
                        CpBase(cpAbleProp_)
{
  craftDbg(4, "CpArray is initialized 1");
  cpMpiComm = cpMpiComm_;
  if( craftMakeAsyncCopy == true ){
    writePtr   = asynData; 
    asyncNElems = *nElems;
    writeNElems = &asyncNElems;
  }
  else{     // data will be written from the original buffer.
    writePtr = dataPtr; 
    writeNElems = nElems;
  }

  cpHelperFuncs::calcSequentialOffset( &os, (sizeof (T) * *nElems), cpMpiComm);
  craftDbg(4, "CpArray is initialized");
  int myrank, numprocs;
  MPI_Comm_rank(cpMpiComm, &myrank);
  std::cout << "=========== myrank: " << myrank << "  file offset is: " << os << std::endl;
}

template <class T>
CpArray<T>::~CpArray()
{
  craftDbg(4, "CpArray is deconstructed");
}

template <class T>
int CpArray<T>::update()
{
  asyncNElems  =  *nElems;
  asynData = new T[asyncNElems];
  CRAFT_CHECK_ERROR ( CRAFT_copyArray ( dataPtr, asynData, asyncNElems) ) ;
  writePtr   = asynData; 
//  print();
  return EXIT_SUCCESS;
}

template <class T>
int CpArray<T>::write(const std::string filename)
{
  if( craftMakeAsyncCopy == true ){
//    craftDbg(5, "CpArray::write: asyncNElems is %d", asyncNElems);
    craftDbg(5, "CpArray::write: *writeNElems is %d", *writeNElems);
  }
  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR( CRAFT_MPI_File_write_1D ( filename, writePtr, *writeNElems, os, cpMpiComm) );
  }
  else if (cpAbleProp.getIOType() == SERIALIO)
  {
    if(cpAbleProp.getFileFormat() == BIN)                     // write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR( CRAFT_SERIAL_BIN_File_write_1D ( filename, writePtr, *writeNElems ) ); 
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR( CRAFT_SERIAL_ASCII_File_write_1D ( filename, writePtr, *writeNElems ) );
    }
    sync();
  }
//  print();
  if( craftMakeAsyncCopy == true ){
    delete[] asynData;
  }
  return EXIT_SUCCESS;
}


template <class T>
int CpArray<T>::read(const std::string filename)
{
  int rc;
  if (cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_1D ( filename, dataPtr, *nElems, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// read in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_1D(filename, dataPtr, *nElems) );
    }
  
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_1D ( filename, dataPtr, *nElems ) );
    }  
  }
//  print();
  return EXIT_SUCCESS;
}

template <class T>
void CpArray<T>::print(){
  int myrank, numProc;  
  MPI_Comm_rank(cpMpiComm, &myrank);
  MPI_Comm_size(cpMpiComm, &numProc);
  for (int p = 0; p < numProc; ++p)
  {
    if(p == myrank)
    {
      std::cout << p << "_dataPtr:" << std::endl;
      for(size_t i = 0; i < *(this->nElems); ++i){
        std::cout << dataPtr[i] << std::endl;
      }
      if( craftMakeAsyncCopy == true ){
        std::cout << "asynData:" << std::endl;
        for(size_t i = 0; i < *(this->writeNElems); ++i){
          std::cout << asynData[i] << std::endl;
        }	
      }
    }
    usleep(10000);
    MPI_Barrier(cpMpiComm);
  }
  return;
}

#endif
