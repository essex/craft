#ifndef __CPPOD_HPP__
#define __CPPOD_HPP__

#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "craftDebugFuncs.hpp"
#include "cpBase.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>

template <class T>
class CpPOD: public CpBase
{
public:
	CpPOD( T * const dataPtr_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpPOD();

	
	
private:

	T * dataPtr;
	T * asynData;
	T * writePtr;                     // in case of Async Writes writePtr=asynData , for sync writes writePtr = dataPtr

  int read(const std::string filename);
	int write(const std::string filename);
	int update();

	MPI_Offset os;
};

template <class T>
CpPOD<T>::CpPOD(  T * const dataPtr_, 
                  const MPI_Comm cpMpiComm_, 
                  CpAbleProp cpAbleProp_)
                  : dataPtr(dataPtr_),
                    CpBase(cpAbleProp_)
{
//	craftDbg(4, "CpPOD is constructed with opts_ %s", opts_.c_str());
	craftDbg(4, "CpPOD::CpPOD ");
  if( craftMakeAsyncCopy == true){
	  craftDbg(4, "CpPOD craftMakeAsyncCopy is true ");
	  asynData  = new T[1];
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
	cpMpiComm = cpMpiComm_;
//	std::cout << "ADDED POD val is: " << *dataPtr << "  " << *asynData << std::endl;
  cpHelperFuncs::calcSequentialOffset( &os, sizeof(T), cpMpiComm);  // os is offset in case of MPI-IO.
}

template <class T>
CpPOD<T>::~CpPOD(){
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
	craftDbg(4, "CpPOD is destructed");
}

template <class T>
int CpPOD<T>::update()
{
	craftDbg(4, "CpPOD::update()");
  *asynData = *dataPtr;
	craftDbg(4, "CpPOD::update() complete");
  return EXIT_SUCCESS;
}

template <class T>
int CpPOD<T>::write(const std::string filename)				// filename is same if MPIIO is ON else filename is different
{
   
  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  { 
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_POD ( filename, writePtr, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_write_POD (filename, writePtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filename, writePtr) );
  	}
    sync(); 
  }
  return EXIT_SUCCESS;
}


template <class T>
int CpPOD<T>::read(const std::string filename)
{
  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_POD ( filename, dataPtr, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_read_POD (filename, dataPtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_read_POD (filename, dataPtr) );
  	}
    sync(); 
  }
  return EXIT_SUCCESS;
}


#endif
