#ifndef __AFT_MACROS_H__
#define __AFT_MACROS_H__
#define AFT_BEGIN(AFT_comm_working, AFT_myrank, AFT_argv) \
{ \
	int AFT_aftFailed = false; \
  do \
  { \
	  try{ \
      craftDbg(-1, "AFT_START try f1");\
      MPI_Comm AFT_parent; \
	    MPI_Errhandler AFT_errh; \
	    MPI_Comm_create_errhandler(&AFT_errhandlerRespawn, &AFT_errh); \
	    MPI_Comm_get_parent( &AFT_parent ); \
	    if( AFT_parent == MPI_COMM_NULL && AFT_aftFailed == false){ \
		    MPI_Comm_dup (MPI_COMM_WORLD, &AFT_comm_working); \
		    MPI_Comm_rank (AFT_comm_working, AFT_myrank); \
        CRAFT_getEnvParam(); \
        AFT_removeMachineFiles(&AFT_comm_working); \
        AFT_createResourceList(&AFT_comm_working); \
		    MPI_Comm_set_errhandler(AFT_comm_working, AFT_errh); \
	    } \
      if ( AFT_parent != MPI_COMM_NULL || AFT_aftFailed == true)	{ \
		    if( AFT_parent != MPI_COMM_NULL && AFT_aftFailed == false){ \
		      craftDbg(-1, "AFT_START try f2"); \
			    AFT_comm_working = MPI_COMM_NULL; \
          CRAFT_getEnvParam(); \
			    AFT_aftFailed = true;	\
		    } \
		    craftDbg(-1, "%d: calling AFT_appNeedsRepair ", *AFT_myrank); \
		    AFT_appNeedsRepair(&AFT_comm_working, AFT_argv); \
		    MPI_Comm_set_errhandler(AFT_comm_working, AFT_errh); \
		    MPI_Comm_rank(AFT_comm_working, AFT_myrank); \
        AFT_setAftRecoveryRun(AFT_aftFailed);\
        craftTime("repairTime", &AFT_comm_working);\
		    craftDbg(1, "%d i am back from repair", *AFT_myrank); \
      }

#define AFT_END() \
    AFT_aftFailed = false; \
	  }catch(int AFT_exception_val){ \
      craftTime("failTime");\
      craftDbg(-1, "AFT_END catch f1");\
		  AFT_aftFailed = true;	\
 	  } \
  } \
  while(AFT_aftFailed == true);\
} 
#endif


