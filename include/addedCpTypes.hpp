#ifndef __ADDEDCPTYPES_HPP__
#define __ADDEDCPTYPES_HPP__

#include <complex>
class Checkpoint;
#include "checkpoint.hpp"
#include "cpPOD.hpp"
#include "cpArray.hpp"
#include "cp2DArray.hpp"
#include "cpBase.hpp"
#include "cpVecPOD.hpp"
#include "cpVecPOD2D.hpp"
#include "cpComplex.hpp"
#include "cpComplexArray.hpp"

#ifdef GHOST_CP 
  #include "cpTypes/cpGhost/cpGhost.hpp"
#endif

#ifdef PHIST_CP 
  #include "cpTypes/cpPhistMvec/cpPhistMvec.cpp"
  #include "cpTypes/cpPhistSdMat/cpPhistSdMat.cpp"
#endif

#ifdef MKL_CP
  #include "mkl.h"
  #include "cpTypes/cpMkl/cpMkl.hpp"
  #include "cpTypes/cpMkl/cpMklArray.hpp"
#endif
#include "cpTypes/cpMpiDatatype/cpMpiDatatype.hpp"

// specialized implementation of add functions
// NOTE: here a new object is created that will not be deleted unless we have some
//       memory management in the background, the "normal" add function just adds 
//       the pointer to the map and the person who created the object is responsible
//       for deleting it.

static int rc; 
// ===== Objects added of cpBase class ===== //

extern int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::shared_ptr<CpBase> const obj
             );
/*
{
  craftDbg(4, "addCpType of CpBase");
  rc = cp->addToMapDirectly(label,obj);
  if(rc){return EXIT_FAILURE;}
  return EXIT_SUCCESS;
}
*/

// ===== POD ===== //
template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type> 
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              T * const i , 
              CpAbleProp cpAbleProp=CpAbleProp())
{
		craftDbg(4, "addCpType of POD");
    rc = cp->addToMap(label, std::make_shared<CpPOD<T>>(i, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== POD ARRAY ===== // 
//TODO: note: if an array is defined statically i.e. int a[5]; the array-argument in add function should be preceded with (int *) a
template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type>
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              T * const arrayPtr_, 
              size_t * const nRows_, 
              CpAbleProp cpAbleProp=CpAbleProp())
{
    craftDbg(4, "addCpType of POD-Array");
    rc = cp->addToMap(label, std::make_shared<CpArray<T>>(arrayPtr_, nRows_, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== POD MULTI-ARRAY ===== // 
//template <class T>
//int addCpType(Checkpoint * cp, std::string label, T** const arrayPtr_, const size_t nRows_, const size_t nCols_, const int toCpCol_){
//    rc = cp->addToMap(label, std::make_shared<CpMultiArray<T>>(arrayPtr_, nRows_, nCols_, toCpCol_, cp->getCpComm() ));
//    if(rc){return EXIT_FAILURE;}
//    return EXIT_SUCCESS;
//}

// ===== POD 2D MULTI-ARRAY ===== // 
template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type>
int addCpType(Checkpoint * cp, 
              std::string label, 
              T ** const arrayPtr_, 
              const size_t nDims_, 
              const size_t * dims_, 
              std::string toCpMask_="x,x", 
              int layout_=CRAFT_2D_ARRAY_LAYOUT_ROWMAJ,
              CpAbleProp cpAbleProp=CpAbleProp())
{
    size_t nRows = dims_[0], nCols = dims_[1];
    craftDbg(4, "nRows=%d\tnCols=%d", nRows, nCols); 
    rc = cp->addToMap(label, std::make_shared<CpMultiArray<T>>( arrayPtr_, nRows, nCols, toCpMask_, layout_, cp->getCpComm(), cpAbleProp ));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== STD::VECTOR ===== //
template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type> 
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::vector<T> * const i , 
              CpAbleProp cpAbleProp=CpAbleProp())
{
		craftDbg(4, "addCpType of std::vector");
    rc = cp->addToMap(label, std::make_shared<CpVecPOD<T>>(i, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== STD::VECTOR<STD::VECTOR> ===== //
template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type> 
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::vector<std::vector<T>> * const i , 
              CpAbleProp cpAbleProp=CpAbleProp())
{
		craftDbg(4, "addCpType of std::vector<std::vector<T>>");
    rc = cp->addToMap(label, std::make_shared<CpVecPOD2D<T>>(i, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== STD::COMPLEX<POD>, float, double, long double ===== //
//template <class T, class=typename std::enable_if< std::is_arithmetic<T>::value >::type> 
template <class T> 
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::complex<T> * const i , 
              CpAbleProp cpAbleProp=CpAbleProp())
{
		craftDbg(4, "addCpType of std::complex");
    rc = cp->addToMap(label, std::make_shared<CpComplex<T>>(i, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== STD::COMPLEX<POD> 1D-ARRAY float, double, long double ===== //
template <class T> 
int addCpType(Checkpoint * const cp, 
              const std::string label, 
              std::complex<T> * const i , 
              size_t * const nElems_, 
              CpAbleProp cpAbleProp=CpAbleProp())
{
		craftDbg(4, "addCpType of std::complex 1D Array");
    rc = cp->addToMap(label, std::make_shared<CpComplexArray<T>>(i, nElems_, cp->getCpComm(), cpAbleProp));
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}

// ===== POD 3D MULTI-ARRAY ===== // 
/*
template <class T>
int addCpType(Checkpoint * cp, std::string label, T *** arrayPtr_, const size_t nDims_, const int * dims_, const int toCpCol_){
//    arrayPtr_ 
//    rc = cp->addToMap(label, std::make_shared<CpMultiArray<T>>(arrayPtr_, nDims_, dims_, toCpCol_, cp->getCpComm() ));
  T a = 1;
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
}
*/

#ifdef GHOST_CP
// ===== GHOST DENSE MATRIX ===== //
int addCpType(Checkpoint * cp, std::string label, 
              ghost_densemat * const GDM,
              CpAbleProp cpAbleProp=CpAbleProp()
             );

int addCpType(Checkpoint * cp, std::string label, 
              ghost_densemat ** const GDMArray, 
              const size_t nDenseMat_, 
              const int toCpDenseMat_=ALL,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

// ===== GHOST SPARSE MATRIX ===== // TODO: add this functionality if needed by users
//int Checkpoint::addCpType(Checkpoint * cp, std::string label, ghost_sparsemat * const GSM)
//{
//}

#endif

#ifdef PHIST_CP 
// ===== PHIST MVEC, SDVEC types ===== // 

enum PHIST_TYPE_IDS{
	PHIST_MVEC_S  = 0,
	PHIST_MVEC_C  = 1,
	PHIST_MVEC_D  = 2,
	PHIST_MVEC_Z  = 3,
	PHIST_SDMAT_S = 4,
	PHIST_SDMAT_C = 5,
	PHIST_SDMAT_D = 6,
	PHIST_SDMAT_Z = 7
};

int addCpType(Checkpoint * cp, 
              std::string label, 
              void * phistPtr, 
              const int PHIST_TYPE_ID,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

#endif


#ifdef MKL_CP
// ===== MKL_Complex8 ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex8 * const dataPtr,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

// ===== MKL_Complex16 ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex16 * const dataPtr,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

// ===== MKL_Complex8 * (Array) ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex8 * const dataPtr, 
              const size_t nElem,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

// ===== MKL_Complex16 * (Array) ===== // 
int addCpType(Checkpoint * cp, 
              std::string label, 
              MKL_Complex16 * const dataPtr, 
              const size_t nElem,
              CpAbleProp cpAbleProp=CpAbleProp() 
             );

#endif

// ===== MPI DERIVED DATA TYPES ===== //
int addCpType(Checkpoint * cp, 
              std::string label, 
              void * bufferPtr, 
              MPI_Datatype * mpiDataType, 
              int numElem=1);

#endif    // __ADDEDCPTYPES_HPP__

