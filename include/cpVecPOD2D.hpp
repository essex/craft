#ifndef __CPVECPOD2D_HPP__
#define __CPVECPOD2D_HPP__

#include "cpEnum.h"
#include "cpBase.hpp"

template <class T>
class CpVecPOD2D: public CpBase
{
public:
  CpVecPOD2D  ( std::vector<std::vector<T>> * const dataPtr_, 
                  const MPI_Comm cpMpiComm_, 
                  CpAbleProp cpAbleProp_=CpAbleProp() )
                  : dataPtr(dataPtr_), 
                    CpBase (cpAbleProp_)
  {
	  craftDbg(4, "CpVecPOD2D::CpVecPOD2D ");
    if( craftMakeAsyncCopy == true){
	    craftDbg(4, "CpVecPOD2D craftMakeAsyncCopy is true ");
	    asynData = new std::vector<std::vector<T>>[1];
      writePtr = asynData;
    }
    else{
      writePtr = dataPtr;
    }
	  cpMpiComm = cpMpiComm_;
     
//    cpHelperFuncs::calcSequentialOffset( &os, (sizeof (T) * dataPtr->size() * ((*dataPtr)[0].size()) ), cpMpiComm);   // NOTE: 2D vectors can not change their size/dimensions.
    // NOTE: here the assumption is that (every process) has vector of vectors of the same size, otherwise write the size of all vectors of vectors independently.
    int myrank=-1; 
    MPI_Comm_rank(cpMpiComm, &myrank);
    os =  (sizeof (T) * dataPtr->size() * ((*dataPtr)[0].size()) * myrank) ;
  }
	~CpVecPOD2D<T>(){}	
private:
	std::vector<std::vector<T>> * dataPtr;
	std::vector<std::vector<T>> * writePtr;
	std::vector<std::vector<T>> * asynData;
  MPI_Offset os;
	int write(const std::string filename);
	int read(const std::string filename);
  int update();
};

template <class T>
int CpVecPOD2D<T>::update(){
	craftDbg(4, "CpVecPOD2D::update()");
  *asynData = *dataPtr;         // TODO: correct it for 2D
  
	craftDbg(4, "CpVecPOD2D::update() complete");
  return EXIT_SUCCESS; 
}

template <class T>
int CpVecPOD2D<T>::write(const std::string filename){

  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_VECTOR_2D_POD <T> ( filename, writePtr, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_write_VECTOR_2D_POD <T> (filename, writePtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_VECTOR_2D_POD <T> ( filename, writePtr) );
  	}
    sync(); 
  }

//  for (i = dataPtr->begin(); i!= dataPtr->end(); ++i)
//  {
//    std::cout << "CpVecPOD2D::update: val is " << *i << std::endl;
//	  craftDbg(4, "CpVecPOD2D::update: val is %f");
//  }

  return EXIT_SUCCESS;
}

template <class T>
int CpVecPOD2D<T>::read(const std::string filename){
  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_VECTOR_2D_POD <T> ( filename, dataPtr, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_read_VECTOR_2D_POD <T> (filename, dataPtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_read_VECTOR_2D_POD <T> (filename, dataPtr) );
  	}
    sync(); 
  }
  return EXIT_SUCCESS;
}

#endif

