#ifndef __CPCOMPLEX_HPP__
#define __CPCOMPLEX_HPP__

#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "craftDebugFuncs.hpp"
#include "cpBase.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <complex>

template <class T>
class CpComplex: public CpBase
{
public:
	CpComplex( std::complex<T> * const dataPtr_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpComplex();

	
	
private:

	std::complex<T> * dataPtr;
	std::complex<T> * asynData;
	std::complex<T> * writePtr;                     // in case of Async Writes writePtr=asynData , for sync writes writePtr = dataPtr

  int read(const std::string filename);
	int write(const std::string filename);
	int update();

//	MPI_Offset os;
};

template <class T>
CpComplex<T>::CpComplex( std::complex<T> * const dataPtr_, 
                  const MPI_Comm cpMpiComm_, 
                  CpAbleProp cpAbleProp_)
                  : dataPtr(dataPtr_),
                    CpBase(cpAbleProp_)
{
	craftDbg(4, "CpComplex::CpComplex ");
  if( craftMakeAsyncCopy == true){
	  craftDbg(4, "CpComplex craftMakeAsyncCopy is true ");
	  asynData  = new std::complex<T>[1];
    writePtr = asynData;
  }
  else{
    writePtr = dataPtr;
  }
	cpMpiComm = cpMpiComm_;
//	std::cout << "ADDED POD val is: " << *dataPtr << "  " << *asynData << std::endl;
//  cpHelperFuncs::calcSequentialOffset( &os, sizeof(T), cpMpiComm);  // os is offset in case of MPI-IO.
}

template <class T>
CpComplex<T>::~CpComplex(){
  if( craftMakeAsyncCopy == true){
    delete[] asynData;
  }
	craftDbg(4, "CpComplex is destructed");
}

template <class T>
int CpComplex<T>::update()
{
	craftDbg(4, "CpComplex::update()");
  *asynData = *dataPtr;
	craftDbg(4, "CpComplex::update() complete");
  return EXIT_SUCCESS;
}

template <class T>
int CpComplex<T>::write(const std::string filename)				// filename is same if MPIIO is ON else filename is different
{
   
  int rc;
	craftDbg(4, "CpComplex::write()");

  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_COMPLEX ( filename, writePtr, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_write_COMPLEX (filename, writePtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_COMPLEX (filename, writePtr) );
  	}
    sync(); 
  }

  return EXIT_SUCCESS;
}


template <class T>
int CpComplex<T>::read(const std::string filename)
{
  int rc;
	craftDbg(4, "CpComplex::read()");
  
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_COMPLEX ( filename, dataPtr, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_read_COMPLEX (filename, dataPtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_read_COMPLEX (filename, dataPtr) );
  	}
    sync(); 
  }

  return EXIT_SUCCESS;
}


#endif
