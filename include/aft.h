#ifndef __AFT_H__
#define __AFT_H__

#include <unistd.h>
#include <string>
#include <vector>
#include <set>

#include "mpi.h"
#include "mpi-ext.h"
#include "failureInfo.h"

extern bool AFT_APPLICATION_STATUS;

int AFT_appNeedsRepair(MPI_Comm *comm, char ** argv);
void AFT_errhandlerRespawn(MPI_Comm* pcomm, int* errcode, ...);
//int MPIX_Comm_replace(MPI_Comm comm, MPI_Comm *newcomm, char** argv);   // NONSHRINKING recovery ( REUSE, NOREUSE node policy)

int AFT_createResourceList(MPI_Comm * const comm);
int AFT_removeMachineFiles(MPI_Comm * const comm);



bool AFT_setAftRecoveryRun(bool CRAFT_aftFailed);
bool AFT_isRecoveryRun();
int AFT_getNumFailedProc();
int AFT_getFailedProcList(int * failureProcList_);
int AFT_getOrigNumProc();
int AFT_getPastRank();


#endif

// TODO: kill_all_procs_on_failed_processhosts
// active_machine_list -> kill_all_procs_on_failed_processhost
//
