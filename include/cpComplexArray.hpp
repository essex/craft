#ifndef __CPCOMPLEXARRAY_HPP__
#define __CPCOMPLEXARRAY_HPP__

#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "craftDebugFuncs.hpp"
#include "cpBase.hpp"
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <complex>

template <class T>
class CpComplexArray: public CpBase
{
public:
	CpComplexArray( std::complex<T> * const dataPtr_, size_t * const nElems_, const MPI_Comm cpMpiComm_, CpAbleProp cpAbleProp_);
	~CpComplexArray();

	
	
private:

	std::complex<T> * dataPtr;
	std::complex<T> * asynData;
	std::complex<T> * writePtr;                     // in case of Async Writes writePtr=asynData , for sync writes writePtr = dataPtr
	size_t * nElems;
  size_t asyncNElems;
  size_t * writeNElems;

  int read(const std::string filename);
	int write(const std::string filename);
	int update();

	MPI_Offset os;
};

template <class T>
CpComplexArray<T>::CpComplexArray( std::complex<T> * const dataPtr_, 
                  size_t * const nElems_,
                  const MPI_Comm cpMpiComm_, 
                  CpAbleProp cpAbleProp_)
                  : dataPtr(dataPtr_),
                    nElems(nElems_),
                    CpBase(cpAbleProp_)
{
	craftDbg(4, "CpComplexArray::CpComplexArray ");
	cpMpiComm = cpMpiComm_;
  if( craftMakeAsyncCopy == true){
	  craftDbg(4, "CpComplexArray craftMakeAsyncCopy is true ");
    writePtr   = asynData; 
    asyncNElems = *nElems;
    writeNElems = &asyncNElems;
  }
  else{
    writePtr = dataPtr;
    writeNElems = nElems;
  }
//	std::cout << "ADDED POD val is: " << *dataPtr << "  " << *asynData << std::endl;
  craftDbg(4, "CpComplexArray is initialized");
}

template <class T>
CpComplexArray<T>::~CpComplexArray(){
	craftDbg(4, "CpComplexArray is destructed");
}

template <class T>
int CpComplexArray<T>::update()
{
	craftDbg(4, "CpComplexArray::update()");
  asyncNElems  =  *nElems;
	craftDbg(4, "CpComplexArray::update() asyncNElems = %d", asyncNElems);
	asynData  = new std::complex<T>[asyncNElems];
  CRAFT_CHECK_ERROR ( CRAFT_copyArray ( dataPtr, asynData, asyncNElems) ) ;
  writePtr   = asynData; 

//  for(size_t i = 0; i < nElems; ++i)
//  {
//    asynData[i] = dataPtr[i];
//  }
	craftDbg(4, "CpComplexArray::update() complete");
  return EXIT_SUCCESS;
}

template <class T>
int CpComplexArray<T>::write(const std::string filename)				// filename is same if MPIIO is ON else filename is different
{
	craftDbg(4, "CpComplexArray::write()");
  if( craftMakeAsyncCopy == true ){
    craftDbg(5, "CpArray::write: asyncNElems is %d", asyncNElems);
    craftDbg(5, "CpArray::write: *writeNElems is %d", *writeNElems);
  } 

  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    cpHelperFuncs::calcSequentialOffset( &os, (sizeof (std::complex<T>) * (*writeNElems)), cpMpiComm);
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_COMPLEX_1D ( filename, writePtr, *writeNElems, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_write_COMPLEX_1D (filename, writePtr, *writeNElems) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_COMPLEX_1D (filename, writePtr, *writeNElems) );
  	}
    sync(); 
  }

  if( craftMakeAsyncCopy == true ){
    delete[] asynData;
  }
  return EXIT_SUCCESS;
}


template <class T>
int CpComplexArray<T>::read(const std::string filename)
{
  int rc;
	craftDbg(4, "CpComplexArray::read() with nElems = %d ", *nElems);
  
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    cpHelperFuncs::calcSequentialOffset( &os, (sizeof (std::complex<T>) * (*nElems)), cpMpiComm);
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_COMPLEX_1D ( filename, dataPtr, *nElems, os, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_read_COMPLEX_1D (filename, dataPtr, *nElems) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_read_COMPLEX_1D (filename, dataPtr, *nElems) );
  	}
  }

  return EXIT_SUCCESS;
}


#endif
