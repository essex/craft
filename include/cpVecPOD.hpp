#ifndef __CPVECPOD_HPP__
#define __CPVECPOD_HPP__

#include "cpEnum.h"
#include "cpBase.hpp"

template <class T>
class CpVecPOD: public CpBase
{
public:
  CpVecPOD  ( std::vector<T> * const dataPtr_, 
                  const MPI_Comm cpMpiComm_, 
                  CpAbleProp cpAbleProp_=CpAbleProp() )
                  : dataPtr(dataPtr_), 
                    CpBase (cpAbleProp_)
  {
	  craftDbg(4, "CpVecPOD::CpVecPOD ");
    if( craftMakeAsyncCopy == true){
	    craftDbg(4, "CpVecPOD craftMakeAsyncCopy is true ");
	    asynData = new std::vector<T>[1];
      writePtr = asynData;
    }
    else{
      writePtr = dataPtr;
    }
	  cpMpiComm = cpMpiComm_;
  }
	~CpVecPOD<T>(){}	
private:
	std::vector<T> * dataPtr;
	std::vector<T> * writePtr;
	std::vector<T> * asynData;

	int write(const std::string filename);
	int read(const std::string filename);
  int update();
};

template <class T>
int CpVecPOD<T>::update(){
	craftDbg(4, "CpVecPOD::update()");
  *asynData = *dataPtr;         
	craftDbg(4, "CpVecPOD::update() complete");
  return EXIT_SUCCESS; 
}

template <class T>
int CpVecPOD<T>::write(const std::string filename){

  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_VECTOR_POD <T> ( filename, writePtr, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_write_VECTOR_POD <T> (filename, writePtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {
      craftErr("%: CpVecPOD does not support writes in ASCII format @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
      );
//      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_VECTOR_POD <T> ( filename, writePtr) );
  	}
    sync(); 
  }

//  for (i = dataPtr->begin(); i!= dataPtr->end(); ++i)
//  {
//    std::cout << "CpVecPOD::update: val is " << *i << std::endl;
//	  craftDbg(4, "CpVecPOD::update: val is %f");
//  }

  return EXIT_SUCCESS;
}

template <class T>
int CpVecPOD<T>::read(const std::string filename){
  int rc;
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
    CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_VECTOR_POD <T> ( filename, dataPtr, cpMpiComm ) );
  }
  else if ( cpAbleProp.getIOType() == SERIALIO )						 // either in case of SCR / SERIAL IO: individual PFS checkpoints
  {
    if(cpAbleProp.getFileFormat() == BIN)                                           	// write in Binary format.  default = BIN
    {
      CRAFT_CHECK_ERROR (CRAFT_SERIAL_BIN_File_read_VECTOR_POD <T> (filename, dataPtr) );
    }
    else if (cpAbleProp.getFileFormat() == ASCII)
    {     
      craftErr("%: CpVecPOD does not support writes in ASCII format @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
      );
//      CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_read_VECTOR_POD <T> (filename, dataPtr) );
  	}
    sync(); 
  }
  return EXIT_SUCCESS;
}

#endif

