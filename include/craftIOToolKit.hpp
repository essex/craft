#ifndef __CRAFTIOTOOLKIT_HPP__
#define __CRAFTIOTOOLKIT_HPP__

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdarg.h> 
#include <stdio.h>
#include <iomanip>
#include <complex.h>
#include <unistd.h>
#include <vector>
#include <memory>

#include <mpi.h>

#include "cpHelperFuncs.hpp"
#include "craftDebugFuncs.hpp"
#include "craftConf.h"
#include "dataType.h"
#include "cpEnum.h"

// ============================== 2D - MPI READ WRITE/READ CALLS ================================ //
// NOTE: lenghts of arrays must stay constant
template <class T>
int CRAFT_MPI_File_write_2D ( const std::string filename,
                              T ** const writePtr, 
                              const size_t loopMin, 
                              const size_t loopMax, 
                              const size_t numElems, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );
template <class T>
int CRAFT_MPI_File_write_2D ( const std::string filename, 
                              T ** const writePtr, 
                              const size_t outerLoopMin, 
                              const size_t outerLoopMax, 
                              const size_t innerLoopMin, 
                              const size_t innerLoopMax, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );
template <class T>
int CRAFT_MPI_File_read_2D (  const std::string filename, 
                              T ** const dataPtr, 
                              const size_t loopMin, 
                              const size_t loopMax, 
                              const size_t umElems, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );
template <class T>
int CRAFT_MPI_File_read_2D (  const std::string filename,
                              T ** const dataPtr, 
                              const size_t outerLoopMin, 
                              const size_t outerLoopMax, 
                              const size_t innerLoopMin, 
                              const size_t innerLoopMax, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );

// ============================== 2D - SERIAL BIN READ WRITE CALLS ================================ //

template <class T>
int CRAFT_SERIAL_BIN_File_write_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t loopMin, 
                                     const size_t loopMax, 
                                     const size_t numElems
                                   );

template <class T>
int CRAFT_SERIAL_BIN_File_write_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t outerLoopMin, 
                                     const size_t outerLoopMax, 
                                     const size_t innerLoopMin, 
                                     const size_t innerLoopMax
                                    );
                 
template <class T>
int CRAFT_SERIAL_BIN_File_read_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t loopMin, 
                                     const size_t loopMax, 
                                     const size_t numElems
                                  );

template <class T>
int CRAFT_SERIAL_BIN_File_read_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t outerLoopMin, 
                                     const size_t outerLoopMax, 
                                     const size_t innerLoopMin, 
                                     const size_t innerLoopMax
                                   );

// ============================== 2D - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_2D (  const std::string filename, 
                                        T ** const dataPtr, 
                                        const size_t outerLoopMin, 
                                        const size_t outerLoopMax, 
                                        const size_t innerLoopMin, 
                                        const size_t innerLoopMax
                                      );


template <class T>
int CRAFT_SERIAL_ASCII_File_read_2D (  const std::string filename, 
                                        T ** const dataPtr, 
                                        const size_t outerLoopMin, 
                                        const size_t outerLoopMax, 
                                        const size_t innerLoopMin, 
                                        const size_t innerLoopMax
                                    );


// ============================== 1D  MPI READ/WRITE CALLS ================================ //
// NOTE: lenghts of arrays must stay constant, (because of MPI/IO file offset)
template <class T>
int CRAFT_MPI_File_write_1D ( const std::string filename,
                              T * const writePtr, 
                              const size_t numElems, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );
template <class T>
int CRAFT_MPI_File_read_1D (  const std::string filename, 
                              T * const dataPtr, 
                              const size_t numElems, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );

// ============================== 1D - SERIAL BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_1D ( const std::string filename,
                                     T * const writePtr, 
                                     const size_t numElems
                                   );
template <class T>
int CRAFT_SERIAL_BIN_File_read_1D ( const std::string filename, 
                                    T * const dataPtr, 
                                    const size_t numElems
                                  );

// ============================== 1D - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_1D ( const std::string filename,
                                      T * const writePtr, 
                                      const size_t numElems
                                     );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_1D ( const std::string filename, 
                                      T * const dataPtr, 
                                      const size_t numElems
                                    );

// ============================== 1D-VECTOR-POD - MPI BIN READ WRITE CALLS ================================ //
// NOTE: lenghts of arrays can vary. 
template <class T>
int CRAFT_MPI_File_write_VECTOR_POD ( const std::string filename,
                                      std::vector<T> * const dataPtr,
                                      MPI_Comm cpMpiComm
                                    );

template <class T>
int CRAFT_MPI_File_read_VECTOR_POD ( const std::string filename,
                                     std::vector<T> * const dataPtr,
                                     MPI_Comm cpMpiComm
                                   );

// ============================== 1D-VECTOR-POD - SERIAL BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                             );
template <class T>
int CRAFT_SERIAL_BIN_File_read_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                            );

// ============================== 1D-VECTOR-POD - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                            );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                            );


// ============================== 2D-VECTOR-POD - MPI BIN READ WRITE CALLS ================================ //
// NOTE: lenghts of arrays must stay constant, (because of MPI/IO file offset)
template <class T>
int CRAFT_MPI_File_write_VECTOR_2D_POD ( const std::string filename,
                                      std::vector<std::vector<T>> * const dataPtr,
	                                    MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                    );

template <class T>
int CRAFT_MPI_File_read_VECTOR_2D_POD ( const std::string filename,
                                     std::vector<std::vector<T>> * const dataPtr,
	                                   MPI_Offset os,
                                     MPI_Comm cpMpiComm
                                   );

// ============================== 2D-VECTOR-POD - SERIAL BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                             );
template <class T>
int CRAFT_SERIAL_BIN_File_read_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            );

// ============================== 2D-VECTOR-POD - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            );


// ============================== POD -  MPI READ/WRITE CALLS ================================ //
template <class T>              // write only one element
int CRAFT_MPI_File_write_POD ( const std::string filename,
                              T * const writePtr, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                              );
template <class T>
int CRAFT_MPI_File_read_POD (  const std::string filename, 
                              T * const dataPtr, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            );

// ============================== POD -  SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_POD ( const std::string filename,
                                     T * const writePtr
                                    );
template <class T>
int CRAFT_SERIAL_BIN_File_read_POD ( const std::string filename, 
                                    T * const dataPtr
                                    );

// ============================== POD -  SERIAL ASCII READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_POD ( const std::string filename,
                                      T * const writePtr
                                      );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_POD  ( const std::string filename, 
                                      T * const dataPtr
                                      );

// ============================== STD::COMPLEX<T> POD-1D-ARRAY -  MPI READ/WRITE CALLS ================================ //
template <class T>              // write only one element
int CRAFT_MPI_File_write_COMPLEX_1D ( const std::string filename,
                                      std::complex<T> * const writePtr, 
                                      const size_t numElems, 
	                                    MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                     );
template <class T>
int CRAFT_MPI_File_read_COMPLEX_1D ( const std::string filename, 
                                      std::complex<T> * const dataPtr, 
                                      const size_t numElems, 
	                                    MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                    );

// ============================== STD::COMPLEX<T> POD - SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_COMPLEX_1D ( const std::string filename,
                                             std::complex<T> * const writePtr,
                                             const size_t numElems
                                            );
template <class T>
int CRAFT_SERIAL_BIN_File_read_COMPLEX_1D ( const std::string filename, 
                                            std::complex<T> * const dataPtr,
                                            const size_t numElems
                                    );


// ============================== STD::COMPLEX<T> POD -  SERIAL ASCII READ/WRITE CALLS ========================= //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_COMPLEX_1D ( const std::string filename,
                                               std::complex<T> * const writePtr,
                                               const size_t numElems
                                             );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_COMPLEX_1D  ( const std::string filename, 
                                               std::complex<T> * const dataPtr,
                                               const size_t numElems
                                              );

// ============================== STD::COMPLEX<T> POD -  MPI READ/WRITE CALLS ================================ //
template <class T>              // write only one element
int CRAFT_MPI_File_write_COMPLEX ( const std::string filename,
                                   std::complex<T> * const writePtr, 
                                   MPI_Comm cpMpiComm
                                  );
template <class T>
int CRAFT_MPI_File_read_COMPLEX ( const std::string filename, 
                                  std::complex<T> * const dataPtr, 
                                  MPI_Comm cpMpiComm
                                );

// ============================== STD::COMPLEX<T> POD - SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_COMPLEX ( const std::string filename,
                                              std::complex<T> * const writePtr
                                            );
template <class T>
int CRAFT_SERIAL_BIN_File_read_COMPLEX ( const std::string filename, 
                                    std::complex<T> * const dataPtr
                                    );


// ============================== STD::COMPLEX<T> POD -  SERIAL ASCII READ/WRITE CALLS ========================= //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_COMPLEX ( const std::string filename,
                                      std::complex<T> * const writePtr
                                      );
template <class T>
int CRAFT_SERIAL_ASCII_File_read_COMPLEX  ( const std::string filename, 
                                      std::complex<T> * const dataPtr
                                      );

// ============================== 2D - COPY CALLS ================================ //
template <class T>
int CRAFT_copy2DArray ( T ** const src, 
                        T ** const dst, 
                        const size_t outerLoopMin, 
                        const size_t outerLoopMax, 
                        const size_t innerLoopMin, 
                        const size_t innerLoopMax
                      );

template <class T>
int CRAFT_copy2DArrayTo1DArray( T ** const src, 
                                T * const dst, 
                                const size_t outerLoopMin, 
                                const size_t outerLoopMax, 
                                const size_t innerLoopMin, 
                                const size_t innerLoopMax
                              );

template <class T>
int CRAFT_copyArray ( T * const src, 
                      T * const dst, 
                      const size_t numElems
                    );


// ============================== 2D - MPI READ WRITE CALLS ================================ //
//  (FILE_HANDLER, ** POINTER, LOOP_MIN, LOOP_MAX, NUM_ELEMS, STATUS)
template <class T>
int CRAFT_MPI_File_write_2D ( const std::string filename,
                              T ** const writePtr, 
                              const size_t loopMin, 
                              const size_t loopMax, 
                              const size_t numElems, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{

	int rc; 
	MPI_File fh;
	MPI_Status status;
  char * fname = new char[1024];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    for(size_t i = loopMin; i< loopMax ; ++i){
      MPI_File_write( fh, &(writePtr[i][0]), numElems, getMpiDataType(T), &status); 
    }
		MPI_File_close(&fh);
    return EXIT_SUCCESS;
  }
  return EXIT_SUCCESS;
}

//  (FILE_HANDLER, ** POINTER, OUTER_LOOP_MIN, OUTER_LOOP_MAX, INNER_LOOP_MIN, INNER_LOOP_MAX, STATUS)
template <class T>
int CRAFT_MPI_File_write_2D ( const std::string filename, 
                              T ** const writePtr, 
                              const size_t outerLoopMin, 
                              const size_t outerLoopMax, 
                              const size_t innerLoopMin, 
                              const size_t innerLoopMax, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
	int rc; 
	MPI_File fh;
	MPI_Status status;
  char * fname = new char[1024];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
  	    craftDbg(5, "writePtr[i][j] %f:", writePtr[i][j]);
  			MPI_File_write( fh, &(writePtr[i][j]), 1, getMpiDataType(T), &status); 
      }
    }
		MPI_File_close(&fh);
    return EXIT_SUCCESS;
  }
  return EXIT_SUCCESS;
}

//(FILE_HANDLER, ** POINTER, LOOP_MIN, LOOP_MAX, NUM_ELEMS, STATUS)
template <class T>
int CRAFT_MPI_File_read_2D (  const std::string filename, 
                              T ** const dataPtr, 
                              const size_t loopMin, 
                              const size_t loopMax, 
                              const size_t numElems, 
	                            MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
	int rc; 
	MPI_File fh;
	MPI_Status status;
  char * fname = new char[1024];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    for(size_t i = loopMin; i< loopMax ; ++i){
      MPI_File_read( fh, &(dataPtr[i][0]), numElems, getMpiDataType(T), &status); 
    }
	  MPI_File_close(&fh);
    return EXIT_SUCCESS;
  }
  return EXIT_SUCCESS;
}

//(FILE_HANDLER, ** POINTER, OUTER_LOOP_MIN, OUTER_LOOP_MAX, INNER_LOOP_MIN, INNER_LOOP_MAX, STATUS)
template <class T>
int CRAFT_MPI_File_read_2D (  const std::string filename,
                              T ** const dataPtr, 
                              const size_t outerLoopMin, 
                              const size_t outerLoopMax, 
                              const size_t innerLoopMin, 
                              const size_t innerLoopMax, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
	int rc; 
	MPI_File fh;
	MPI_Status status;
  char * fname = new char[1024];
  sprintf(fname, "%s",  (filename).c_str());
	rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
  	    craftDbg(5, "writePtr[i][j] %f:", dataPtr[i][j]);
  			MPI_File_read( fh, &(dataPtr[i][j]), 1, getMpiDataType(T), &status); 
      }
    }
	  MPI_File_close(&fh);
    return EXIT_SUCCESS;
  }
  return EXIT_SUCCESS;
}


// ============================== 2D - SERIAL BIN READ WRITE CALLS ================================ //

template <class T>
int CRAFT_SERIAL_BIN_File_write_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t loopMin, 
                                     const size_t loopMax, 
                                     const size_t numElems
                                   )
{
  std::ofstream fstr;
  fstr.open ((filename).c_str(), std::ios::out | std::ios::binary);  
  if (fstr.is_open()){
    for(size_t i = loopMin; i < loopMax; ++i){
	    craftDbg(5, "dataPtr[i][0] %f:", dataPtr[i][0]);
      fstr.write( (char *)&dataPtr[i][0], sizeof (T) * numElems);
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_write_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t outerLoopMin, 
                                     const size_t outerLoopMax, 
                                     const size_t innerLoopMin, 
                                     const size_t innerLoopMax
                                    )
{
  std::ofstream fstr;
  fstr.open ((filename).c_str(), std::ios::out | std::ios::binary);  
  if (fstr.is_open()){
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
	      craftDbg(5, "dataPtr[i][0] %f:", dataPtr[i][0]);
        fstr.write( (char *)&dataPtr[i][j], sizeof (T) );
      }
    }
    fstr.close(); 
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
                  
template <class T>
int CRAFT_SERIAL_BIN_File_read_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t loopMin, 
                                     const size_t loopMax, 
                                     const size_t numElems
                                  )
{
  std::ifstream fstr;
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);  
  if (fstr.is_open()){
    for(size_t i = loopMin; i < loopMax; ++i){
      craftDbg(5, "dataPtr[i][0] %f:", dataPtr[i][0]);
      fstr.read( (char *)&dataPtr[i][0], sizeof (T) * numElems);
    } 
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_2D ( const std::string filename,
                                     T ** const dataPtr, 
                                     const size_t outerLoopMin, 
                                     const size_t outerLoopMax, 
                                     const size_t innerLoopMin, 
                                     const size_t innerLoopMax
                                   )
{
  std::ifstream fstr;
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);  
  if (fstr.is_open()){
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
        fstr.read( (char *)&dataPtr[i][j], sizeof (T) );
	      craftDbg(5, "dataPtr[i][j] %f:", dataPtr[i][j]);
      }
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

// ============================== 2D - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_2D (  const std::string filename, 
                                        T ** const dataPtr, 
                                        const size_t outerLoopMin, 
                                        const size_t outerLoopMax, 
                                        const size_t innerLoopMin, 
                                        const size_t innerLoopMax
                                      )
{
  std::ofstream fstr;
  fstr.open ((filename).c_str(), std::ios::out);  
  if (fstr.is_open()){
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
	      craftDbg(5, "writePtr[i][j] %f:", dataPtr[i][j]);
        fstr << std::setprecision(17) << dataPtr[i][j] << std::endl;  
      }
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	  return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_ASCII_File_read_2D (  const std::string filename, 
                                        T ** const dataPtr, 
                                        const size_t outerLoopMin, 
                                        const size_t outerLoopMax, 
                                        const size_t innerLoopMin, 
                                        const size_t innerLoopMax
                                    )
{
  std::ifstream fstr;
  std::string line;
  fstr.open ((filename).c_str(), std::ios::in);  
  if (fstr.is_open()){
    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
      for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
        getline (fstr, line);
        dataPtr[i][j] = cpHelperFuncs::stringToNumber<T>(line);
	      craftDbg(5, "read dataPtr[i][j] %f:", dataPtr[i][j]);
      }
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	  return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}




// ============================== 1D  MPI READ/WRITE CALLS ================================ //
//  (FILE_HANDLER, * POINTER, NUM_ELEMS )
template <class T>
int CRAFT_MPI_File_write_1D ( const std::string filename,
                              T * const writePtr, 
                              const size_t numElems, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
  int rc;
  craftDbg(5, "CRAFT_MPI_File_write_1D");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{

    MPI_File_seek( fh , os, MPI_SEEK_SET);
//    MPI_File_write( fh, &numElems, 1, MPI_INT, &status);               // NOTE: No need to write the numElems. Assumption is array size can not be changed.
    MPI_File_write( fh, writePtr, numElems, getMpiDataType(T), &status); 
    MPI_File_close(&fh);
  }
  delete[] fname; 
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_1D (  const std::string filename, 
                              T * const dataPtr, 
                              const size_t numElems, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
  int rc;
  craftDbg(5, "CRAFT_MPI_File_read_1D");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE; 
  }
  else{
   MPI_File_seek( fh , os, MPI_SEEK_SET);
//    MPI_File_read( fh, num1DElems, 1, MPI_INT, &status);                     
    MPI_File_read( fh, dataPtr, numElems, getMpiDataType(T), &status);        //XXX: should new num1DElems be same as old. In future, make it flexible.
    MPI_File_close(&fh);
  }
  delete[] fname;
  return EXIT_SUCCESS;
}


// ============================== 1D - SERIAL BIN READ WRITE CALLS ================================ //

template <class T>
int CRAFT_SERIAL_BIN_File_write_1D ( const std::string filename,
                                     T * const writePtr, 
                                     const size_t numElems
                                   )
{
  std::ofstream fstr;
	craftDbg(5, "CRAFT_SERIAL_BIN_File_write_1D");
  fstr.open ((filename).c_str(), std::ios::out | std::ios::binary);	
  if(fstr.is_open()){
    fstr.write( (char *)writePtr, sizeof (T) * numElems );
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_1D ( const std::string filename, 
                                    T * const dataPtr, 
                                    const size_t numElems
                                  )
{
  std::ifstream fstr;
  craftDbg(5, "CRAFT_SERIAL_BIN_File_read_1D ");
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
  if(fstr.is_open()){
    fstr.read( (char*)dataPtr , sizeof(T) * numElems);	
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

// ============================== 1D - SERIAL ASCII READ WRITE CALLS ================================ //

template <class T>
int CRAFT_SERIAL_ASCII_File_write_1D ( const std::string filename,
                                      T * const writePtr, 
                                      const size_t numElems
                                     )
{

  std::ofstream fstr;
	craftDbg(5, "CRAFT_SERIAL_ASCII_File_write_1D");
  fstr.open ((filename).c_str(), std::ios::out );	
  if(fstr.is_open()){
    for(size_t i = 0; i < numElems; ++i){
      fstr << std::setprecision(17) << writePtr[i] << std::endl;  
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_ASCII_File_read_1D ( const std::string filename, 
                                      T * const dataPtr, 
                                      const size_t numElems
                                    )
{
  std::ifstream fstr;
  craftDbg(5, "CRAFT_SERIAL_ASCII_File_read_1D ");
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
  if(fstr.is_open()){
    std::string line;
    for(size_t i = 0; i < numElems; ++i)
    {
      getline (fstr, line);
      dataPtr[i] = cpHelperFuncs::stringToNumber<T>(line);
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

// ============================== 1D-VECTOR-POD - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                             )
{

  std::ofstream fstr;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ((filename).c_str(), std::ios::out );	

  static typename	std::vector<T>::iterator begin;
  static typename	std::vector<T>::iterator end;
  begin = dataPtr->begin();
  end   = dataPtr->end();
 
  if(fstr.is_open()){
    typename std::vector<T>::iterator i;
    fstr << dataPtr->size() << std::endl;
    std::setprecision(17);

    for (const auto &e : (*dataPtr)) { fstr << e << "\n"; }

    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}

template <class T>
int CRAFT_SERIAL_ASCII_File_read_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                            )
{
  // In serial-ASCII file format, all elements of a file can be read into this vector without bothering the lenght of the vector. 
  std::ifstream fstr;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	

  if(fstr.is_open()){
    std::string line;
    getline (fstr, line);     // first line determine the size of the vector
    size_t numVecElems = cpHelperFuncs::stringToNumber<T>(line);
    dataPtr->resize(numVecElems);                                     // In case the checkpointed vector is shorter or longer than the myCP.add(vector)
    craftDbg (5, "%s: Number of elements in the vector : %d", __PRETTY_FUNCTION__, numVecElems);  
    size_t i=0;
    while ( fstr.peek() != EOF )
    {
      getline (fstr, line);
      (*dataPtr)[i++] = cpHelperFuncs::stringToNumber<T>(line); 
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
}

// ============================== 1D-VECTOR-POD - SERIAL BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                             )
{
  std::ofstream fstr;
	craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ( (filename).c_str(), std::ios::out | std::ios::binary );	

  static typename	std::vector<T>::iterator begin;
  static typename	std::vector<T>::iterator end;
  begin = dataPtr->begin();
  end   = dataPtr->end();
 
  if(fstr.is_open()){
    typename std::vector<T>::iterator i;
    size_t numVecElems = dataPtr->size();
    fstr.write( (char *)&numVecElems, sizeof (size_t) );

//    MPI_File_write( fh, &(*i) , numVecElems , getMpiDataType(T), &status); 
    i = begin;
    fstr.write( (char *)&(*i), sizeof (T)*numVecElems  );
/*
    for(i = begin; i != end; ++i){
      fstr.write( (char *)&(*i), sizeof (T) );
    }
*/
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_VECTOR_POD ( const std::string filename,
                                              std::vector<T> * const dataPtr
                                            )
{
  std::ifstream fstr;
  craftDbg (5, "%s", __PRETTY_FUNCTION__);
  fstr.open ( (filename).c_str(), std::ios::in | std::ios::binary );

  if ( fstr.is_open() ){
    std::string line;
    size_t numVecElems; 
    fstr.read ( (char*) &numVecElems, sizeof (size_t) );                // first line determines the size of the vector.
    dataPtr->resize(numVecElems);                                     // In case the checkpointed vector is shorter or longer than the myCP.add(vector)
    craftDbg (5, "%s: Number of elements in the vector : %d", __PRETTY_FUNCTION__, numVecElems);  

    static typename	std::vector<T>::iterator begin;
    typename std::vector<T>::iterator i;
    begin = dataPtr->begin();
    i = begin;
    fstr.read( (char *)&(*i), sizeof (T)*numVecElems );
/*
    while ( fstr.peek() != EOF )
    {
      T readTemp;
      fstr.read( (char *)&readTemp, sizeof (T) );
//      dataPtr->push_back(readTemp);
      (*dataPtr)[i++] = readTemp; 
    }
*/
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}

// ============================== 1D-VECTOR-POD - MPI BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_MPI_File_write_VECTOR_POD ( const std::string filename,
                                      std::vector<T> * const dataPtr,
                                      MPI_Comm cpMpiComm
                                    )
{
  int rc;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  MPI_File fh;
  MPI_Status status;
  static typename	std::vector<T>::iterator begin;
  static typename	std::vector<T>::iterator end;
  begin = dataPtr->begin();
  end   = dataPtr->end();

  char * fname = new char[512];
  sprintf(fname, "%s",  (filename).c_str());

  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    size_t numVecElems = dataPtr->size();
	  MPI_Offset osPOD;
    int myrank; 
    MPI_Comm_rank(cpMpiComm, &myrank);
    osPOD = sizeof(size_t)*myrank; 

    CRAFT_MPI_File_write_POD ( filename + ".offset", &numVecElems, osPOD, cpMpiComm );   // The number of elements are stored each time in this file. The number of elements can change in a vector, this is why a separate file is required so that offset in the main file can be calculated easily while reading.

	  MPI_Offset os;
    cpHelperFuncs::calcSequentialOffset ( &os, ( (sizeof (T) * numVecElems) ), cpMpiComm );
    MPI_File_seek( fh , os, MPI_SEEK_SET);

    typename std::vector<T>::iterator i;
    i = begin;
    MPI_File_write( fh, &(*i) , numVecElems , getMpiDataType(T), &status); 
//
//    for(i = begin; i != end; ++i){
//      T writeBuf = *i;
//      MPI_File_write( fh, &writeBuf , 1 , getMpiDataType(T), &status); 
//      MPI_File_write( fh, &(*i) , 1 , getMpiDataType(T), &status); 
//    }
    MPI_File_close(&fh);
  }
  sync();
  delete[] fname; 
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_VECTOR_POD ( const std::string filename,
                                     std::vector<T> * const dataPtr,
                                     MPI_Comm cpMpiComm
                                   )
{
  int rc;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  MPI_File fh;
  MPI_Status status;

  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());

  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    size_t numVecElems;
    MPI_Offset osPOD;
    int myrank; 
    MPI_Comm_rank(cpMpiComm, &myrank);
    osPOD = sizeof(size_t)*myrank; 

    CRAFT_MPI_File_read_POD ( filename + ".offset", &numVecElems, osPOD, cpMpiComm );    // reads number of elems in vectors of each process. 
    dataPtr->resize(numVecElems);                       // In case the checkpointed vector is shorter or longer than the myCP.add(vector)
	  MPI_Offset os;
    cpHelperFuncs::calcSequentialOffset ( &os, (sizeof (T) * numVecElems ), cpMpiComm );
    MPI_File_seek( fh , os, MPI_SEEK_SET);

    static typename	std::vector<T>::iterator begin;
    typename std::vector<T>::iterator i;
    begin = dataPtr->begin();
    i = begin;
    MPI_File_read( fh, &(*i) , numVecElems , getMpiDataType(T), &status); 
/*
    for(int i = 0 ; i < numVecElems ; ++i){
      T readTemp;
      MPI_File_read( fh, &readTemp , 1 , getMpiDataType(T), &status); 
      (*dataPtr)[i] = readTemp; 
    }
*/
    MPI_File_close(&fh);
  }
  sync();
  delete[] fname; 
  return EXIT_SUCCESS;
}

//// ============================== 2D-VECTOR-POD - MPI BIN READ WRITE CALLS ================================ //
// NOTE: 2D vector size must remain same during writing & reading. 

template <class T>
int CRAFT_MPI_File_write_VECTOR_2D_POD ( const std::string filename,
                                      std::vector<std::vector<T>> * const dataPtr,
                                      MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                    ){
  int rc;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  MPI_File fh;
  MPI_Status status;

  char * fname = new char[512];
  sprintf(fname, "%s",  (filename).c_str());

  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{

    size_t * num2DVecElems = new size_t[2]; 
    num2DVecElems[0] = dataPtr->size();
    num2DVecElems[1] = (*dataPtr)[0].size();        // NOTE: here the assumption is that all vector of vectors have the same size, otherwise write the size of all vectors of vectors independently.
    craftDbg(5, "%s , num2DVecElems[0]: %d, num2DVecElems[1]: %d", __PRETTY_FUNCTION__, num2DVecElems[0], num2DVecElems[1]);
//    CRAFT_MPI_File_write_1D ( filename + ".offset", num2DVecElems, 2, cpMpiComm );     // The number of elements are stored each time in this file. The number of elements can change in a vector, this is why a separate file is required so that offset in the main file can be calculated easily while reading.

//	  MPI_Offset os;
//    cpHelperFuncs::calcSequentialOffset ( &os, ( sizeof (T) * num2DVecElems[0]*num2DVecElems[1]), cpMpiComm ); 
    MPI_File_seek( fh , os, MPI_SEEK_SET);

//    int rank; MPI_Comm_rank (MPI_COMM_WORLD, &rank);
//    os = rank*(sizeof (T) * numElems);;
//    typename std::vector<T>::iterator i;
//    craftDbg(5, "%s , myoffset: %d ", __PRETTY_FUNCTION__, os);

    static typename std::vector< std::vector<T> >::iterator row;
    static typename std::vector<T>::iterator col;

//    row = dataPtr->begin();
//    col = row->begin();
//    MPI_File_write( fh, &(*col) , num2DVecElems[0]*num2DVecElems[1] , getMpiDataType(T), &status);

//    MPI_File_write( fh, &num2DVecElems[0] , 1 , MPI_INT, &status);
//    MPI_File_write( fh, &num2DVecElems[1] , 1 , MPI_INT, &status);
    for (row = dataPtr->begin(); row != dataPtr->end(); row++) {
      col = row->begin(); 
      MPI_File_write( fh, &(*col) , num2DVecElems[1] , getMpiDataType(T), &status);
      col++;
    }

/*
    for (row = dataPtr->begin(); row != dataPtr->end(); row++) {
      for (col = row->begin(); col != row->end(); col++) {
        MPI_File_write( fh, &(*col) , 1 , getMpiDataType(T), &status); 
      }
    }
*/
    MPI_File_close(&fh);
    delete[] num2DVecElems;
  }
  sync();
  delete[] fname; 
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_VECTOR_2D_POD ( const std::string filename,
                                     std::vector<std::vector<T>> * const dataPtr,
                                     MPI_Offset os,
                                     MPI_Comm cpMpiComm
                                   ){

  int rc;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  MPI_File fh;
  MPI_Status status;

  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());

  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    size_t * num2DVecElems = new size_t[2]; 
    num2DVecElems[0] = dataPtr->size();
    num2DVecElems[1] = (*dataPtr)[0].size();        // NOTE: here the assumption is that all vector of vectors have the same size, otherwise write the size of all vectors of vectors independently.
 
//    CRAFT_MPI_File_read_1D ( filename + ".offset", num2DVecElems, 2, cpMpiComm );    // reads number of elems in vectors of each process. 
//    craftDbg(5, "%s , num2DVecElems[0]: %d, num2DVecElems[1]: %d", __PRETTY_FUNCTION__, num2DVecElems[0], num2DVecElems[1]);
//	  MPI_Offset os;
//    cpHelperFuncs::calcSequentialOffset ( &os, (sizeof (T) * num2DVecElems[0]*num2DVecElems[1] ), cpMpiComm );
//
    MPI_File_seek( fh , os, MPI_SEEK_SET);

    static typename std::vector< std::vector<T> >::iterator row;
    static typename std::vector<T>::iterator col;
    for (row = dataPtr->begin(); row != dataPtr->end(); row++) {
      col = row->begin(); 
      MPI_File_read( fh, &(*col) , num2DVecElems[1] , getMpiDataType(T), &status);
      col++;
    } 

/*
 *
    for(int i = 0 ; i < num2DVecElems[0] ; ++i){        // Using this method, the length of vectors could be changed as well inbeeween checkpoints.
      for(int j = 0 ; j < num2DVecElems[1] ; ++j){
        T readTemp;
        MPI_File_read( fh, &readTemp , 1 , getMpiDataType(T), &status); 
        (*dataPtr)[i][j] = readTemp;
      }
    }
*/

    MPI_File_close(&fh);
  }
  sync();
  delete[] fname; 
  return EXIT_SUCCESS;

}

// ============================== 2D-VECTOR-POD - SERIAL BIN READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                             )
{
  std::ofstream fstr;
	craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ( (filename).c_str(), std::ios::out | std::ios::binary );	

  if(fstr.is_open()){
    size_t * num2DVecElems = new size_t[2]; 
    num2DVecElems[0] = dataPtr->size();
    num2DVecElems[1] = (*dataPtr)[0].size();        // NOTE: here the assumption is that all vector of vectors have the same size, otherwise write the size of all vectors of vectors independently.
    fstr.write( (char *)&num2DVecElems[0], sizeof (size_t) );
    fstr.write( (char *)&num2DVecElems[1], sizeof (size_t) );

    static typename std::vector< std::vector<T> >::iterator row;
    static typename std::vector<T>::iterator col;

    for (row = dataPtr->begin(); row != dataPtr->end(); row++) {
      for (col = row->begin(); col != row->end(); col++) {
        fstr.write( (char *)&(*col), sizeof (T) );
      }
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            ){
  std::ifstream fstr;
  craftDbg (5, "%s", __PRETTY_FUNCTION__);
  fstr.open ( (filename).c_str(), std::ios::in | std::ios::binary );

  if ( fstr.is_open() ){
    size_t * num2DVecElems = new size_t[2]; 
    num2DVecElems[0]=0;
    num2DVecElems[1]=0;
    fstr.read ( (char*) &num2DVecElems[0], sizeof (size_t) );
    fstr.read ( (char*) &num2DVecElems[1], sizeof (size_t) );
    craftDbg (5, "%s , num2DVecElems[0]: %d, num2DVecElems[1]: %d", __PRETTY_FUNCTION__, num2DVecElems[0], num2DVecElems[1]);
    for(size_t i = 0 ; i < num2DVecElems[0] ; ++i){
      for(size_t j = 0 ; j < num2DVecElems[1] ; ++j){
        T readTemp;
        fstr.read( (char *)&readTemp, sizeof (T) );
  //      dataPtr->push_back(readTemp);
        (*dataPtr)[i][j] = readTemp; 
      }
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}
// ============================== 2D-VECTOR-POD - SERIAL ASCII READ WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            ){

  std::ofstream fstr;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ((filename).c_str(), std::ios::out );	

  if(fstr.is_open()){

    size_t * num2DVecElems = new size_t[2]; 
    num2DVecElems[0] = dataPtr->size();
    num2DVecElems[1] = (*dataPtr)[0].size();        // NOTE: here the assumption is that all vector of vectors have the same size, otherwise write the size of all vectors of vectors independently.
    fstr.write( (char *)&num2DVecElems[0], sizeof (size_t) );
    fstr.write( (char *)&num2DVecElems[1], sizeof (size_t) );
    fstr <<  num2DVecElems[0] << std::endl;
    fstr <<  num2DVecElems[1] << std::endl;

    std::setprecision(17);

    static typename std::vector< std::vector<T> >::iterator row;
    static typename std::vector<T>::iterator col;

    for (row = dataPtr->begin(); row != dataPtr->end(); row++) {
      for (col = row->begin(); col != row->end(); col++) {
        fstr <<  *col << std::endl;
//        fstr.write( (char *)&(*col), sizeof (T) );
      }
    }


    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
}
template <class T>
int CRAFT_SERIAL_ASCII_File_read_VECTOR_2D_POD ( const std::string filename,
                                              std::vector<std::vector<T>> * const dataPtr
                                            ){
/*
  std::ifstream fstr;
  craftDbg(5, "%s", __PRETTY_FUNCTION__);
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	

  if(fstr.is_open()){
    std::string line;
    getline (fstr, line);     // first line determine the size of the vector
    size_t numVecElems = cpHelperFuncs::stringToNumber<T>(line);
    dataPtr->resize(numVecElems);                                     // In case the checkpointed vector is shorter or longer than the myCP.add(vector)
    craftDbg (5, "%s: Number of elements in the vector : %d", __PRETTY_FUNCTION__, numVecElems);  
    int i=0;
    while ( fstr.peek() != EOF )
    {
      getline (fstr, line);
      (*dataPtr)[i++] = cpHelperFuncs::stringToNumber<T>(line); 
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
*/
  return EXIT_FAILURE;
}
// ============================== POD -  MPI READ/WRITE CALLS ================================ //

template <class T>              // write only one element
int CRAFT_MPI_File_write_POD ( const std::string filename,
                              T * const writePtr, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                             )
{
  int rc;
  craftDbg(5, "CRAFT_MPI_File_write_POD");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
//    MPI_Offset os;
//    cpHelperFuncs::calcSequentialOffset ( &os, sizeof(T) , cpMpiComm );
//    int myrank; 
//    MPI_Comm_rank(cpMpiComm, &myrank);
//    os = sizeof(T)*myrank; 
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    MPI_File_write( fh, writePtr, 1 , getMpiDataType(T), &status); 
    MPI_File_close(&fh);
  }
  delete[] fname; 
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_POD (  const std::string filename, 
                              T * const dataPtr, 
                              MPI_Offset os,
                              MPI_Comm cpMpiComm
                            )
{
  int rc;
  craftDbg(5, "CRAFT_MPI_File_read_POD");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE; 
  }
  else{
//    MPI_Offset os;
//    cpHelperFuncs::calcSequentialOffset ( &os, sizeof(T) , cpMpiComm );
//    int myrank; 
//    MPI_Comm_rank(cpMpiComm, &myrank);
//    os = sizeof(T)*myrank; 

    MPI_File_seek( fh , os, MPI_SEEK_SET);
    MPI_File_read( fh, dataPtr, 1, getMpiDataType(T), &status); 
    MPI_File_close(&fh);
  }
  delete[] fname;
  return EXIT_SUCCESS;
}

// ============================== POD -  SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_POD ( const std::string filename,
                                     T * const writePtr
                                   )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_1D (filename, writePtr, 1) );
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_POD ( const std::string filename, 
                                    T * const dataPtr
                                  )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_1D ( filename, dataPtr, 1 ) );
  return EXIT_SUCCESS;
}

// ============================== POD -  SERIAL ASCII READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_POD ( const std::string filename,
                                      T * const writePtr
                                     )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_1D (filename, writePtr, 1) );
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_ASCII_File_read_POD ( const std::string filename, 
                                      T * const dataPtr
                                    )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_1D ( filename, dataPtr, 1 ) );
  return EXIT_SUCCESS;
}


// ============================== STD::COMPLEX<T> POD-1D-ARRAY -  MPI READ/WRITE CALLS ================================ //
template <class T>              // write only one element
int CRAFT_MPI_File_write_COMPLEX_1D ( const std::string filename,
                                      std::complex<T> * const writePtr, 
                                      const size_t numElems, 
	                                    MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                    )
{

  int rc;
  craftDbg(5, "CRAFT_MPI_File_write_COMPLEX_1D");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[256];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_CREATE | MPI_MODE_WRONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek  ( fh, os, MPI_SEEK_SET);
    MPI_File_write( fh, writePtr, numElems, getMpiDataType(std::complex<T>), &status); 
    MPI_File_close(&fh);
  }
  delete[] fname; 

  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_COMPLEX_1D ( const std::string filename, 
                                      std::complex<T> * const dataPtr, 
                                      const size_t numElems, 
	                                    MPI_Offset os,
                                      MPI_Comm cpMpiComm
                                    )
{

  int rc;
  craftDbg(5, "CRAFT_MPI_File_read_COMPLEX_1D");
  MPI_File fh;
  MPI_Status status;
  char * fname = new char[1024];
  sprintf(fname, "%s",  (filename).c_str());
  rc = MPI_File_open( cpMpiComm, fname, MPI_MODE_RDONLY, MPI_INFO_NULL, &fh);
  if(rc) { 
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  else{
    MPI_File_seek( fh , os, MPI_SEEK_SET);
    MPI_File_read( fh, dataPtr, numElems, getMpiDataType(std::complex<T>), &status); 
    MPI_File_close(&fh);
  }
  delete[] fname; 

  return EXIT_SUCCESS;
}

// ============================== STD::COMPLEX<T> POD-1D-ARRAY - SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_COMPLEX_1D ( const std::string filename,
                                             std::complex<T> * const writePtr,
                                             const size_t numElems
                                            )
{

  std::ofstream fstr;
	craftDbg(5, "CRAFT_SERIAL_BIN_File_write_COMPLEX_1D");
  fstr.open ((filename).c_str(), std::ios::out | std::ios::binary);	
  if(fstr.is_open()){
    T rl, im;
    for(size_t i = 0; i < numElems; ++i){
      rl = writePtr[i].real();
      im = writePtr[i].imag();
      fstr.write( (char *)&rl, sizeof (T) );
      fstr.write( (char *)&im, sizeof (T) );
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_COMPLEX_1D ( const std::string filename, 
                                            std::complex<T> * const dataPtr,
                                            const size_t numElems
                                          )
{

  std::ifstream fstr;
  craftDbg(5, "CRAFT_SERIAL_BIN_File_read_COMPLEX_1D ");
  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
  if(fstr.is_open()){
    T rl, im;
    for(size_t i = 0; i < numElems; ++i){
      fstr.read( (char*)&rl , sizeof(T) );	
      fstr.read( (char*)&im , sizeof(T) );	
      dataPtr[i].real(rl);
      dataPtr[i].imag(im);
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

// ============================== STD::COMPLEX<T> POD-1D-ARRAY -  SERIAL ASCII READ/WRITE CALLS ========================= //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_COMPLEX_1D ( const std::string filename,
                                               std::complex<T> * const writePtr,
                                               const size_t numElems
                                             )
{
  std::ofstream fstr;
	craftDbg(5, "CRAFT_SERIAL_ASCII_File_write_COMPLEX_1D");
  fstr.open ((filename).c_str(), std::ios::out );	
  if(fstr.is_open()){
    for(size_t i = 0; i < numElems; ++i){
      fstr << std::setprecision(17) << writePtr[i].real() << std::endl;  
      fstr << std::setprecision(17) << writePtr[i].imag() << std::endl;  
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else{
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
template <class T>
int CRAFT_SERIAL_ASCII_File_read_COMPLEX_1D  ( const std::string filename, 
                                               std::complex<T> * const dataPtr,
                                               const size_t numElems
                                              )
{
  std::ifstream fstr;
  craftDbg(5, "CRAFT_SERIAL_ASCII_File_read_COMPLEX_1D ");
  fstr.open ((filename).c_str(), std::ios::in );	
  if(fstr.is_open()){
    std::string line;
    for(size_t i = 0; i < numElems; ++i)
    {
      getline (fstr, line);
      dataPtr[i].real( cpHelperFuncs::stringToNumber<T>(line) );
      getline (fstr, line);
      dataPtr[i].imag( cpHelperFuncs::stringToNumber<T>(line) );
    }
    fstr.close();
    return EXIT_SUCCESS;
  }
  else
  {
    craftErr("%s: unable to open file @ %s:%d",
              __PRETTY_FUNCTION__, __FILE__, __LINE__
            );
	 return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}


// ============================== STD::COMPLEX<T> POD -  MPI READ/WRITE CALLS ================================ //
template <class T>              // write only one element
int CRAFT_MPI_File_write_COMPLEX ( const std::string filename,
                                       std::complex<T> * const writePtr, 
                                       MPI_Comm cpMpiComm
                                     )
{
	MPI_Offset os;
  cpHelperFuncs::calcSequentialOffset ( &os, sizeof(std::complex<T>), cpMpiComm ); 
  CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_COMPLEX_1D (filename, writePtr, 1, os, cpMpiComm) );
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_MPI_File_read_COMPLEX ( const std::string filename, 
                                      std::complex<T> * const dataPtr, 
                                      MPI_Comm cpMpiComm
                                    )
{
	MPI_Offset os;
  cpHelperFuncs::calcSequentialOffset ( &os, sizeof(std::complex<T>), cpMpiComm ); 
  CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_COMPLEX_1D (filename, dataPtr, 1, os, cpMpiComm) );

  return EXIT_SUCCESS;
}

// ============================== STD::COMPLEX<T> POD - SERIAL BIN READ/WRITE CALLS ================================ //
template <class T>
int CRAFT_SERIAL_BIN_File_write_COMPLEX ( const std::string filename,
                                              std::complex<T> * const writePtr
                                            )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_COMPLEX_1D (filename, writePtr, 1) );
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_BIN_File_read_COMPLEX ( const std::string filename, 
                                    std::complex<T> * const dataPtr
                                    )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_COMPLEX_1D (filename, dataPtr, 1) );

  return EXIT_SUCCESS;
}


// ============================== STD::COMPLEX<T> POD -  SERIAL ASCII READ/WRITE CALLS ========================= //
template <class T>
int CRAFT_SERIAL_ASCII_File_write_COMPLEX ( const std::string filename,
                                      std::complex<T> * const writePtr
                                      )
{
  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_COMPLEX_1D (filename, writePtr, 1) );
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_SERIAL_ASCII_File_read_COMPLEX  ( const std::string filename, 
                                      std::complex<T> * const dataPtr
                                      )
{

  CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_COMPLEX_1D (filename, dataPtr, 1) );
  return EXIT_SUCCESS;
}


// ============================== 2D - COPY CALLS ================================ //
template <class T>
int CRAFT_copy2DArray ( T ** const src, 
                        T ** const dst, 
                        const size_t outerLoopMin, 
                        const size_t outerLoopMax, 
                        const size_t innerLoopMin, 
                        const size_t innerLoopMax
                      )
{

    for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
  		for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
  			dst[i][j] = src[i][j];
  		}
  	}
  return EXIT_SUCCESS;
}

template <class T>
int CRAFT_copy2DArrayTo1DArray( T ** const src, 
                                T * const dst, 
                                const size_t outerLoopMin, 
                                const size_t outerLoopMax, 
                                const size_t innerLoopMin, 
                                const size_t innerLoopMax
                              )
{

	craftDbg(5, "CRAFT_copy2DArrayTo1DArray K should be = %d", ( outerLoopMax - outerLoopMin ) * 
                                                              ( innerLoopMax - innerLoopMin ) );
  int k=0;
  for(size_t i = outerLoopMin; i < outerLoopMax; ++i){
    for(size_t j = innerLoopMin; j < innerLoopMax; ++j){
      dst[k++] = src[i][j]; 
    }
  }
	craftDbg(5, "CRAFT_copy2DArrayTo1DArray K after copy is = %d", k ); 

/*
  if(layout = CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)
  {
    for(size_t i = 0; i < nRows; ++i){
  		for(size_t j = 0; j < nCols; ++j){
  			dst[i][j] = src[i][j];
  		}
  	}
  }
  if(layout = CRAFT_2D_ARRAY_LAYOUT_COLMAJ)
  {
    for(size_t i = 0; i < nCols; ++i){
  		for(size_t j = 0; j < nRows; ++j){
  			dst[i][j] = src[i][j];
  		}
  	}
  }
*/
  return EXIT_SUCCESS;
}

// ============================== 1D - COPY CALL================================ //
template <class T>
int CRAFT_copyArray ( T * const src, 
                      T * const dst, 
                      const size_t numElems
                    )
{
  for(size_t i= 0; i< numElems; ++i){
    dst[i] = src[i];
  }
  return EXIT_SUCCESS;
}

#endif  // __CRAFTIOTOOLKIT_HPP__
