#ifndef __CHECKPOINT_HPP__
#define __CHECKPOINT_HPP__

#include <map>
#include <future>
#include <vector>
#include <mpi.h>

#include "craftConf.h"

class CpBase;
#include "craftDebugFuncs.hpp"

#define SSTR( x ) static_cast< std::ostringstream & >( \
								        ( std::ostringstream() << std::dec << x ) ).str()


class Checkpoint
{
private:
	MPI_Comm cpMpiCommInternal;
  MPI_Comm asynComm;            // NOTE: This comm is only valid in case of async. Cp writes, and gets initialized in asynPFSwrite function.
	std::string cpPath;
	std::string cpBasePath;	
	std::string cpPathVersion;
	std::string cpName;
	std::string cpVersionPrefix;
	bool cpUseSCR;
	bool cpCommitted;
	size_t cpVersion; 	
  bool restartStatus;

  Checkpoint * parentCp=NULL;
  typedef std::vector<Checkpoint*> checkpointVec_t;
  checkpointVec_t children;
  typedef std::map<const std::string, std::shared_ptr<CpBase> > cpMap_t;
  cpMap_t checkpointablesMap;
  std::shared_future<int> fut;
  bool ret;
  
  double cpWriteTime=0.0;
  double cpUpdateTime=0.0;

// variables for a signal based Cp 
  bool takeCp = false;
  int unSkewIter = -1;

// ************ 
//  WRITE CALLS
// ************ 
	int writeCpMetaData(const std::string filename);
	int PFSwrite();
	int SCRwrite();
  int syncPFSwrite();
  int asynPFSwrite();
  int doAsynPFSwrite(MPI_Comm * comm_);

// ************ 
//  READ CALLS
// ************ 
	int readCpMetaData();
//  int SCRreadCpMetaData();
	int PFSread (const std::string toReadCpable);
	int SCRread (const std::string toReadCpable);
  int getCpVersion(); 

// ************************* 
//  NESTED CHECKPOINTS CALLS
// ************************* 
  int invalidateNestedCp();
  int invalidate();
  int addChild(Checkpoint * child);

// ************* 
//  HELPER CALLS
// *************
  std::string generateFullFileName( const std::string cpPath, const std::string filename);
	int deleteBackupCp(MPI_Comm comm_);
  std::string getCpName();
  
public:
	Checkpoint(const std::string name_, const MPI_Comm cpMpiCommExternal);
	~Checkpoint();  

// **************************** 
//  READ, WRITE, UPDATE RELATED
// **************************** 
//  int read ();
  int read ( std::string toReadCpable="" );
 	int write();
	int update();
  int updateAndWrite(const int iter=1, const int cpfreq=1);
  int updateAndWriteAtSignal(const int iter);
  bool needRestart();
  int restartIfNeeded(int * iter=NULL);             // iter's value will be increased by 1.
	int commit();
  int wait();
  
  int nestedCp(Checkpoint * parentCp_);
	void disableSCR();
  MPI_Comm getCpComm();

// **************************
//  ADD to MAP INTERFACE CALL
// **************************

  int addToMap(std::string label, std::shared_ptr<CpBase> const objToAdd);
  int addToMapDirectly(std::string label, std::shared_ptr<CpBase> const objToAdd); 
  template <typename ...Params>
  int add(Params&&... params);
/*
  {
    int rc;
    rc = addCpType(this, std::forward<Params>(params)...);
    if(rc){return EXIT_FAILURE;}
    return EXIT_SUCCESS;
  }
*/
  int rm(const std::string toRmKey)
  {
    checkpointablesMap.erase(toRmKey);
		craftDbg(0, "The following checkpointable is removed: %s", toRmKey.c_str());
    return EXIT_SUCCESS;
  }
  
};

#include "addedCpTypes.hpp"

template <typename ...Params>
int Checkpoint::add(Params&&... params)
{
  if( !craftEnabled ){
    craftDbg(3, "Checkpoint::add(): CRAFT_ENABLE: %d", craftEnabled);
    return EXIT_SUCCESS;
  }
  if (cpCommitted){
			craftDbg(0, "This checkpoint is already committed. No data can be added to checkpoint after commit() call of a checkpoint");
    return EXIT_FAILURE;
  }

  int rc;
  craftDbg(3, "Checkpoint::add(): forwarding params");
  rc = addCpType(this, std::forward<Params>(params)...);
  if(rc){return EXIT_FAILURE;}
  return EXIT_SUCCESS;
}

// implementation of addToMap() for anything that is CpBase,
// anything else will give an error message

int CRAFT_DeleteCheckpointData(const std::string toRm);

#endif // __CHECKPOINT_HPP__
