#ifndef __CPBASE_HPP__
#define __CPBASE_HPP__

#include <string>
#include <mpi.h>
#include <unistd.h>

#include "cpAbleProperties.hpp"

class CpBase
{
protected: 

public:

//  std::shared_ptr<CpAbleProp> cpAbleProp;
//	CpBase(std::shared_ptr<CpAbleProp> cpAbleProp_ = std::make_shared<CpAbleProp>())
//    :cpAbleProp(cpAbleProp_){
//    craftDbg(4, "CpBase is constructed");
//  }

  CpAbleProp cpAbleProp;
	CpBase(CpAbleProp cpAbleProp_ = CpAbleProp() )
    :cpAbleProp(cpAbleProp_){
    craftDbg(4, "CpBase is constructed");
  }
 
  virtual ~CpBase(){ craftDbg(4, "CpBase is destructed");}

	MPI_Comm cpMpiComm;



  // ===== VIRUAL FUNCTIONS ===== //
	virtual int read(const std::string filename) = 0;
  virtual int write(const std::string filename) = 0;
  virtual int update() = 0;
  
};


#endif
