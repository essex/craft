#ifndef __CPABLEPROPERTIES_HPP__
#define __CPABLEPROPERTIES_HPP__

#include <string>
#include <sstream>
#include <algorithm>

#include "craftDebugFuncs.hpp"
#include "cpEnum.h"

class CpAbleProp;
static int parseCpBaseOptions(CpAbleProp * cpaProp, const std::string opts_);
static int matchOptions(CpAbleProp * cpaProp, const std::string toMatch);

// ===== this class carryies each checkpointables properties ===== //
class CpAbleProp
{
private:
  int IOType;                                        // SERIAL OR PARALLEL (SERIALIO, MPIIO)
  int fileFormat;                                             // fileFormat = ASCII or BIN 
public:
  CpAbleProp(std::string prop_):IOType(SERIALIO),fileFormat(BIN){
    if (prop_!=""){
      parseCpBaseOptions(this, prop_);
    }
  }
  CpAbleProp():IOType(SERIALIO),fileFormat(BIN){
    craftDbg(5, "default properties of CpAbleProp are set");
  }
  ~CpAbleProp()
  {
  }

   int getIOType()
  { 
    return IOType; 
  } 
  int setIOType(int toSetType)
  { 
    IOType = toSetType; 
    return EXIT_SUCCESS;
  } 
  int getFileFormat()
  {
    return fileFormat; 
  }
  int setFileFormat(int toSetFileFormat)
  {
    fileFormat = toSetFileFormat;
    return EXIT_SUCCESS;
  }
}; 

static int parseCpBaseOptions(CpAbleProp * cpaProp, const std::string opts_){
  craftDbg(6, "Parsing the Options string %s", opts_.c_str());
  std::istringstream ss(opts_);
  std::string token;
  while(std::getline(ss, token, ',')) 
  {
    token.erase(std::remove_if(                                  // REMOVE SPACES IN STRING
                              token.begin(), 
                              token.end(), 
                              [](unsigned char x){return std::isspace(x);}
                              ), 
                token.end()
                ); 
    matchOptions(cpaProp, token);    
  }
  return EXIT_SUCCESS;
}

static int matchOptions(CpAbleProp * cpaProp, const std::string toMatch){
  if(toMatch == "MPIIO"){
    craftDbg ( 6, "The IOType is MPIIO" );
    cpaProp->setIOType(MPIIO);
    return EXIT_SUCCESS;
  }
  else if(toMatch == "SERIALIO"){
    craftDbg ( 6, "The IOType is SERIALIO" );
    cpaProp->setIOType(SERIALIO);
    return EXIT_SUCCESS;
  }
  else if(toMatch == "ASCII"){
    craftDbg ( 6, "The file-format is ASCII " );
    cpaProp->setFileFormat(ASCII);
    return EXIT_SUCCESS;
  }
  else if(toMatch == "BIN"){
    craftDbg ( 6, "The file-format is BIN" );
    cpaProp->setFileFormat(BIN);
    return EXIT_SUCCESS;
  }
  else {
    craftDbg ( 4, "This is not a valid checkpointable option: %s", toMatch.c_str() );
  }
  return EXIT_SUCCESS;
}


#endif

