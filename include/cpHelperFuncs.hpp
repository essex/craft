#ifndef __CPHELPERFUNCS_HPP__
#define __CPHELPERFUNCS_HPP__

#include <string>
#include <sstream>
#include <stdarg.h> 
#include <stdio.h>
#include <iomanip>

#include <mpi.h>

#include "craftConf.h"

namespace cpHelperFuncs{

template <typename T>
std::string numberToString ( T number )
{
	std::ostringstream ss;
	ss << number;
	return ss.str();
}
template <typename T>
T stringToNumber ( const std::string& text )
{
  std::stringstream ss(text);
  T result;
  return ss >> result ? result : 0;
}

int mpiCommWorldRank();
int mpiCommWorldNumProcs();

void getEnvValTemp(int *varVal);
//void getEnvStr(char * varVal, const char * str);
//void checkPathName(std::string* const s);
void checkDirectoryName(std::string* const s);
std::string exec(const char* cmd);
int calcSequentialOffset( MPI_Offset * offset, size_t numBytes, MPI_Comm comm);
int getUnskewIter(MPI_Comm comm_, int n);
int mkCpDir(std::string path, MPI_Comm comm);

}



#endif

