#ifndef __CP2DARRAY_HPP__
#define __CP2DARRAY_HPP__

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <assert.h>

#include "cpHelperFuncs.hpp"
#include "craftIOToolKit.hpp"
#include "cpEnum.h"
#include "cpBase.hpp"


// ===== POD 2D MULTI-ARRAY ===== // 
template <class T>
class CpMultiArray: public CpBase
{
public:
	CpMultiArray( T ** const dataPtr_, 
                const size_t nRows_, 
                const size_t nCols_, 
                std::string toCpMask_, 
                int mulArrLayout_,
                const MPI_Comm cpMpiComm_, 
                CpAbleProp cpAbleProp_ );

	~CpMultiArray();

	int read(const std::string filename);
	int write(const std::string filename);
  int update();
	void print();
  int parseToCpRowCol(std::string opts_);
private:
  int mulArrLayout;

	T ** dataPtr;
	T ** asynData;
	T * asynData1D;
	T ** writePtr;
  std::string toCpMask;
  int toCp;
	size_t nRows;
	size_t nCols;
	int toCpCol;
	int toCpRow;

	void copyMultiArray(T ** const src, T ** const dst, const size_t nRows, const size_t nCols);
  int calcWhatToCp();

	int readParallel(const std::string filename);
	int readSerial(const std::string filename);
  int readSerialASCII(const std::string filename);
  int readSerialBIN(const std::string filename);

	int writeParallel(const std::string filename);
	int writeSerial(const std::string filename);
  int writeSerialASCII(const std::string filename);
  int writeSerialBIN(const std::string filename);
  
  MPI_Offset os;
};

template <class T>
int CpMultiArray<T>::parseToCpRowCol(std::string opts_)
{
  craftDbg(4, "Parsing the Options string: %s", opts_.c_str());
  std::istringstream ss(opts_);
  std::string token;
  for(int i=0; i<2 ; ++i)
  {
    std::getline(ss, token, ',');
    token.erase(std::remove_if(                                  // REMOVE SPACES IN STRING
                              token.begin(), 
                              token.end(), 
                              [](unsigned char x){return std::isspace(x);}
                              ), 
                token.end()
                ); 
    if(token == "x"){ token = cpHelperFuncs::numberToString(ALL);  }
    if(token == "CYCLIC"){ token = cpHelperFuncs::numberToString(CYCLIC); }
    if(i==0){ 
      toCpRow = cpHelperFuncs::stringToNumber<int>(token); 
//      if (toCpRow == CYCLIC) { cyclicRowCP = true; }
    }
    if(i==1){ 
      toCpCol = cpHelperFuncs::stringToNumber<int>(token); 
//      if (toCpCol == CYCLIC) { cyclicColCP = true; }
    }
  }
  return EXIT_SUCCESS;
}

template<class T>
int CpMultiArray<T>::calcWhatToCp(){
  if (toCpRow == ALL && toCpCol == ALL)
  { 
    toCp = CRAFT_2D_ARRAY_CP_ALL; 
    return toCp;
  }
  else if (toCpRow >=0 && toCpCol == ALL)         // [----]
  { 
    assert(toCpRow < nRows);
    toCp = CRAFT_2D_ARRAY_CP_ONE_ROW;
    return toCp;
  }
  else if (toCpRow == CYCLIC && toCpCol == ALL)   // [----]
  { 
    toCp = CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW;
    toCpRow = 0;                                  // CYCLIC CP ROW Counter starts from 0
    return toCp;
  }
  else if (toCpRow == ALL && toCpCol >= 0)        // [ | ]
  { 
    assert(toCpCol < nCols);
    toCp = CRAFT_2D_ARRAY_CP_ONE_COL;
    return toCp;
  }
  else if (toCpCol == CYCLIC && toCpRow == ALL)   // [ | ]
  { 
    toCp = CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL;
    toCpCol = 0;                                  // CYCLIC CP COL Counter starts from 0
    return toCp;
  }

  else 
  {
    craftErr("CpMultiArray::calcWhatToCp unable to determine the required structure to checkpoint. @ %s:%d",
              __FILE__, __LINE__
            );
  }
  return 0;
}

// ===== POD MULTI-ARRAY function implementations ===== // 
template <class T>
CpMultiArray<T>::CpMultiArray( T ** const dataPtr_, 
                const size_t nRows_, 
                const size_t nCols_, 
                std::string toCpMask_, 
                int mulArrLayout_,
                const MPI_Comm cpMpiComm_, 
                CpAbleProp cpAbleProp_ )
                : 
                  dataPtr(dataPtr_),
                  nRows(nRows_),
                  nCols(nCols_),
                  toCpMask(toCpMask_),
                  mulArrLayout(mulArrLayout_),
                  CpBase(cpAbleProp_)
{

	craftDbg(0, "Use multi-arrays with caution, this is not fully fool proof yet."); // TODO: the MULTI-ARRAY function need to be tested again. 
  cpMpiComm = cpMpiComm_;
  parseToCpRowCol(toCpMask);
  calcWhatToCp();             // Calculates if ALL, ONE ROW, or ONE COL is to be checkpointed.
  craftDbg(4, "toCpRow: %d \t toCpCol: %d ", toCpRow, toCpCol);
  craftDbg(4, "CRAFT_2D_ARRAY_LAYOUT: %d ", mulArrLayout);
  
// allocate on the needed amount of memory, as indicated by toCpRow, toCpCol

   if( craftMakeAsyncCopy == true){
     if (toCp == CRAFT_2D_ARRAY_CP_ALL)     // All 2D array to checkpoint
     {
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
         asynData = new T*[nRows];
         for(size_t i = 0; i < nRows; ++i){
           asynData[i] = new T[nCols];
	       }
         craftDbg(4, "CRAFT_2D_ARRAY: asynData is allocated ROWMAJ");
        }
        else if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
         asynData = new T*[nCols];
         for(size_t i = 0; i < nCols; ++i){
           asynData[i] = new T[nRows];
	       }
         craftDbg(4, "CRAFT_2D_ARRAY: asynData is allocated COLMAJ");
        }
        writePtr = asynData; 
     }
     else if(toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW ) // one Full Row to checkpoint
     {
       asynData1D = new T[nCols];
       craftDbg(4, "CRAFT_2D_ARRAY: asynData1D is allocated for ONE ROW");
       /*
       asynData = new T*[1];
       for(size_t i = 0; i < 1; ++i){
           asynData[i] = new T[nCols];
	     }
        */
//       writePtr = asynData;
     }
     else if(toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL ) // one Full Column to checkpoint
     {
       asynData1D = new T[nRows];
       craftDbg(4, "CRAFT_2D_ARRAY: asynData1D is allocated for ONE COL");
      /*
       asynData = new T*[nRows];
       for(size_t i = 0; i < nRows; ++i){
         asynData[i] = new T[1];
	     }
      */
//       writePtr = asynData;
     }
   }
   else{                                      // SYNC WRITE FROM DATA
     craftDbg (4, "CpMultiArray::CpMultiArray: Sync write is selected. data will be directly written from the originat data"); 
     writePtr = dataPtr; 
   }
   

  if(cpAbleProp.getIOType() == MPIIO)                  // Parallel PFS IO, calc offset in case of non-uniform arrays of processes.
  {
  	if(toCp == CRAFT_2D_ARRAY_CP_ALL){
      cpHelperFuncs::calcSequentialOffset( &os, (sizeof (T) * nRows * nCols), cpMpiComm);
    }
  	else if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
      cpHelperFuncs::calcSequentialOffset( &os, (sizeof (T) * nCols), cpMpiComm);
    }
    else if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
      cpHelperFuncs::calcSequentialOffset( &os, (sizeof (T) * nRows), cpMpiComm);
    }
  }

}

template <class T>
CpMultiArray<T>::~CpMultiArray()
{
  if( craftMakeAsyncCopy == true){
  if(toCp == CRAFT_2D_ARRAY_CP_ALL){
    for(size_t i = 0; i < nRows; ++i){
      delete[] asynData[i];
	  }
    delete[] asynData;
  }
 	else if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
  }
  else if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
  }
    
 }
	craftDbg(4, "CpMultiArray is deconstructed");

}

template <class T>
int CpMultiArray<T>::update()
{
  // =============== UPDATE ALL 2D ARRAY =============== // 
  if(toCp == CRAFT_2D_ARRAY_CP_ALL){
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: ROWMAJ CP_ALL copy start ");
	    CRAFT_copy2DArray(dataPtr, asynData, 0, nRows, 0, nCols );
	    craftDbg(4, "CpMultiArray::update: ROWMAJ CP_ALL copy done ");
    }
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: COLMAJ CP_ALL copy start");
	    CRAFT_copy2DArray(dataPtr, asynData, 0, nCols, 0, nRows );
	    craftDbg(4, "CpMultiArray::update: COLMAJ CP_ALL copy done ");
    }
  }
  // =============== UPDATE ONE ROW =============== // 
  else if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: ROWMAJ ONE_ROW copy start ");
	    CRAFT_copy2DArrayTo1DArray(dataPtr, asynData1D, toCpRow, toCpRow+1, 0, nCols );
	    craftDbg(4, "CpMultiArray::update: ROWMAJ ONE_ROW copy done ");
    }
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: COLMAJ ONE_ROW copy start ");
	    CRAFT_copy2DArrayTo1DArray(dataPtr, asynData1D, 0, nCols, toCpRow, toCpRow+1 );
	    craftDbg(4, "CpMultiArray::update: COLMAJ ONE_ROW copy done ");
    }
  }
  // =============== UPDATE ONE COL =============== // 
  else if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: ROWMAJ ONE_COL copy start");
	    CRAFT_copy2DArrayTo1DArray(dataPtr, asynData1D, 0, nRows, toCpCol, toCpCol+1 );
	    craftDbg(4, "CpMultiArray::update: ROWMAJ ONE_COL copy done ");
    }
    if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
    {
	    craftDbg(4, "CpMultiArray::update: COLMAJ ONE_COL copy start ");
	    CRAFT_copy2DArrayTo1DArray(dataPtr, asynData1D, toCpCol, toCpCol+1, 0, nRows );
	    craftDbg(4, "CpMultiArray::update: COLMAJ ONE_COL copy done ");
    }
  }
	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::write(const std::string filename)
{
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
	  CRAFT_CHECK_ERROR( writeParallel(filename) );
  }
  else
  {
	  CRAFT_CHECK_ERROR( writeSerial(filename) );
  }
	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::writeParallel(const std::string filename)      // Parallel write with BIN format.
{
	craftDbg(4, "CpMultiArray<T>::writeParallel");

      // =============== WRITE ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
        craftDbg(4, "CpMultiArray::writeParallel(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeParallel(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR( CRAFT_MPI_File_write_2D (filename, writePtr, 0, nRows, nCols, os, cpMpiComm) );
          return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeParallel(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_2D (filename, writePtr, 0, nCols, nRows, os, cpMpiComm) );
	        return EXIT_SUCCESS;
        }
      }

      // =============== WRITE ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
        craftDbg(4, "CpMultiArray::writeParallel(): toCp = CRAFT_2D_ARRAY_CP_ONE_ROW or ONE_CYCLIC_ROW");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_1D (filename, asynData1D, nCols, os, cpMpiComm ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeParallel(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_2D (filename, writePtr, toCpRow, toCpRow+1 , nCols, os, cpMpiComm) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeParallel(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_2D (filename, writePtr, 0, nCols, toCpRow, toCpRow+1 , os, cpMpiComm) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
			  // ===== write the metadata file for cycliccpcounter ===== // 
	        craftDbg(4, "writing cyclic toCpRow count: %d", toCpRow);
			    std::string filenameMD;
          int myrank=-1; 
          MPI_Comm_rank(cpMpiComm, &myrank);
			    filenameMD = filename + ".metadata" + cpHelperFuncs::numberToString(myrank);
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpRow) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpRow);
			    ++toCpRow;
			    if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }

      // =============== WRITE ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
	      craftDbg(4, "CpMultiArray::writeParallel(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_1D (filename, asynData1D, nRows, os, cpMpiComm ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeParallel(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_2D (filename, writePtr, 0, nRows, toCpCol, toCpCol+1 , os, cpMpiComm) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeParallel(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_MPI_File_write_2D (filename, writePtr, toCpCol, toCpCol+1, nRows, os, cpMpiComm) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
  			  // ===== write the metadata file for cycliccpcounter ===== // 
  	      craftDbg(4, "writing cyclic toCpCol count: %d", toCpCol);
  			  std::string filenameMD;
          int myrank=-1; 
          MPI_Comm_rank(cpMpiComm, &myrank);
    	    filenameMD = filename + ".metadata" + cpHelperFuncs::numberToString(myrank);
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpCol) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpCol);
  			  ++toCpCol;
  			  if( toCpCol == nCols ){
  				  toCpCol = 0;	
  			  }
        }
	      return EXIT_SUCCESS;
    }
	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::writeSerial(const std::string filename){

  craftDbg(4, "CpMultiArray::writeSerial():");
  if (cpAbleProp.getFileFormat() == ASCII)
  {
    craftDbg(4, "CpMultiArray::writeSerial(): write ASCII format");
    CRAFT_CHECK_ERROR ( writeSerialASCII(filename) );
  }
  if (cpAbleProp.getFileFormat() == BIN)
  {
    craftDbg(4, "CpMultiArray::writeSerial(): write BIN format");
    CRAFT_CHECK_ERROR ( writeSerialBIN(filename) );
  }

	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::writeSerialASCII(const std::string filename){

	  craftDbg(4, "CpMultiArray::writeSerialASCII()");
    
      // =============== WRITE ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
	      craftDbg(4, "CpMultiArray::writeSerialASCII(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, 0, nRows, 0, nCols ) );
	        return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, 0, nCols, 0, nRows ) );
	        return EXIT_SUCCESS;
        }
      }

      // =============== WRITE ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
	      craftDbg(4, "CpMultiArray::writeSerialASCII(): toCp = CRAFT_2D_ARRAY_CP_ONE_ROW");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_1D (filename, asynData1D, nCols ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if ( mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ )  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, toCpRow, toCpRow+1, 0, nCols ) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, 0, nCols, toCpRow, toCpRow+1) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
			    // ===== write the metadata file for cycliccpcounter ===== // 
			    std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpRow) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpRow);
			    ++toCpRow;
			    if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }
  
      // =============== WRITE ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
	      craftDbg(4, "CpMultiArray::writeSerialASCII(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_1D (filename, asynData1D, nRows ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, 0, nRows, toCpCol, toCpCol+1) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialASCII(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_write_2D (filename, writePtr, toCpCol, toCpCol+1, 0, nRows) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
			    // ===== write the metadata file for cycliccpcounter ===== // 
			    std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpCol) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpCol);
			    ++toCpCol;
			    if( toCpCol == nCols ){
				    toCpCol = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }
    
  return 0;
}

template <class T>
int CpMultiArray<T>::writeSerialBIN(const std::string filename){
	  craftDbg(4, "CpMultiArray::writeSerialBIN()");
      // =============== WRITE ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
	      craftDbg(4, "CpMultiArray::writeSerialBIN(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, 0, nRows, nCols) );
	        return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, 0, nCols, nRows) );
	        return EXIT_SUCCESS;
        }
      }

      // =============== WRITE ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
	      craftDbg(4, "CpMultiArray::writeSerialBIN(): toCp = CRAFT_2D_ARRAY_CP_ONE_ROW or ONE_CYCLIC_ROW");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_1D (filename, asynData1D, nCols ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, toCpRow, toCpRow+1, nCols) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, 0, nCols, toCpRow, toCpRow+1) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
			    // ===== write the metadata file for cycliccpcounter ===== // 
			    std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpRow) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpRow);
			    ++toCpRow;
			    if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }
      // =============== WRITE ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
	      craftDbg(4, "CpMultiArray::writeSerialBIN(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");
        if ( craftMakeAsyncCopy == true )
        {
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_1D (filename, asynData1D, nRows ) );
        }
        else if ( craftMakeAsyncCopy == false )
        {
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = ROWMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, 0, nRows, toCpCol, toCpCol+1) );
          }
          if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
          {
  	        craftDbg(4, "CpMultiArray::writeSerialBIN(): layout = COLMAJ");
            CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_write_2D (filename, writePtr, toCpCol, toCpCol+1, nRows) );
          }
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
  			  // ===== write the metadata file for cycliccpcounter ===== // 
    	    std::string filenameMD;
  		    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR (CRAFT_SERIAL_ASCII_File_write_POD (filenameMD, &toCpCol) );
//          CRAFT_writeASCIICounterToFile(filenameMD, toCpCol);
  		    ++toCpCol;
  		    if( toCpCol == nCols ){
  			    toCpCol = 0;	
  		    }
        }
	      return EXIT_SUCCESS;
    }
  return EXIT_SUCCESS;
}



template <class T>
int CpMultiArray<T>::read(const std::string filename)
{
  if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
  {
	  CRAFT_CHECK_ERROR ( readParallel(filename) );
  }
  else
  {
	  CRAFT_CHECK_ERROR ( readSerial(filename) );
  }
	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::readParallel(const std::string filename)
{
	craftDbg(4, "CpMultiArray<T>::readParallel\n");

      // =============== READ ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
        craftDbg(4, "CpMultiArray::readParallel(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, 0, nRows, nCols, os, cpMpiComm ) );
	        return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, 0, nCols, nRows, os, cpMpiComm ) );
	        return EXIT_SUCCESS;
        }
      }
      // =============== READ ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
        craftDbg(4, "CpMultiArray::readParallel(): toCp = CRAFT_2D_ARRAY_CP_ONE_ROW or ONE_CYCLIC_ROW");
        // =============== CHECK FOR CYCLIC ROW NUMBER =============== // 
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
          std::string filenameMD;
          int myrank=-1; 
          MPI_Comm_rank(cpMpiComm, &myrank);
  	      filenameMD = filename + ".metadata" + cpHelperFuncs::numberToString(myrank);
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpRow) );
//          toCpRow = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpRow= %d", toCpRow);
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, toCpRow, toCpRow+1, nCols, os, cpMpiComm ) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, 0, nCols, toCpRow, toCpRow+1, os, cpMpiComm ) );
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
          ++toCpRow;
          if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }

      // =============== READ ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
	      craftDbg(4, "CpMultiArray::readParallel(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");

        // =============== CHECK FOR CYCLIC COL NUMBER =============== // 
        if (toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL)
        {
          std::string filenameMD;
          int myrank=-1; 
          MPI_Comm_rank(cpMpiComm, &myrank);
  	      filenameMD = filename + ".metadata" + cpHelperFuncs::numberToString(myrank);
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpCol) );
//          toCpCol = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpCol = %d", toCpCol);
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, 0, nRows, toCpCol, toCpCol+1, os, cpMpiComm ) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readParallel(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_MPI_File_read_2D ( filename, dataPtr, toCpCol, toCpCol+1, nRows, os, cpMpiComm ) );
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
          ++toCpCol;
			    if( toCpCol == nCols ){
				    toCpCol = 0;	
			    }
        }
	      return EXIT_SUCCESS;
      }
	return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::readSerial(const std::string filename){
  craftDbg(4, "CpMultiArray::readSerial()");
  if (cpAbleProp.getFileFormat() == ASCII)
  {
    craftDbg(4, "CpMultiArray::readSerial(): read ASCII format");
    CRAFT_CHECK_ERROR ( readSerialASCII(filename) );
  }
  if (cpAbleProp.getFileFormat() == BIN)
  {
    craftDbg(4, "CpMultiArray::readSerial(): read BIN format");
    CRAFT_CHECK_ERROR ( readSerialBIN(filename) );
  }
	return EXIT_SUCCESS;
}
template <class T>
int CpMultiArray<T>::readSerialASCII(const std::string filename){

	  craftDbg(4, "CpMultiArray::readSerialASCII()");

      // =============== READ ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
	      craftDbg(4, "CpMultiArray::readSerialASCII(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D(filename, dataPtr, 0, nRows, 0, nCols) );
          return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D(filename, dataPtr, 0, nCols, 0, nRows) );
          return EXIT_SUCCESS;
        }
      }

      // =============== READ ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
        // =============== CHECK FOR CYCLIC ROW NUMBER =============== // 
        if (toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW)
        {
          std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpRow) );
//          toCpRow = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpRow= %d", toCpRow);
        }

	      craftDbg(4, "CpMultiArray::readSerialASCII(): toCp = CRAFT_2D_ARRAY_CP_ONE_ROW");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D(filename, dataPtr, toCpRow, toCpRow+1, 0, nCols) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D (filename, dataPtr, 0, nCols, toCpRow, toCpRow+1 ) );
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
          ++toCpRow;
          if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
        return EXIT_SUCCESS;
      }
  
      // =============== READ ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
        // =============== CHECK FOR CYCLIC COL NUMBER =============== // 
        if (toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL)
        {
          std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpCol) );
//          toCpCol = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpCol = %d", toCpCol);
        }

	      craftDbg(4, "CpMultiArray::readSerialASCII(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D (filename, dataPtr, 0, nRows, toCpCol, toCpCol+1 ) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialASCII(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_2D (filename, dataPtr, toCpCol, toCpCol+1, 0, nRows ) );
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
          ++toCpCol;
  			  if( toCpCol == nCols ){
  				  toCpCol = 0;	
  			  }
        }
        return EXIT_SUCCESS;
      }
      return EXIT_SUCCESS;
}

template <class T>
int CpMultiArray<T>::readSerialBIN(const std::string filename){
	  craftDbg(4, "CpMultiArray::readSerialBIN()");
    
         // =============== READ ALL 2D ARRAY =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ALL){
	      craftDbg(4, "CpMultiArray::readSerialBIN(): toCp = ALL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, 0, nRows, nCols) );
          return EXIT_SUCCESS;
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, 0, nCols, nRows) );
          return EXIT_SUCCESS;
        }
      }

      // =============== READ ONE ROW =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_ROW || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW){
        // =============== CHECK FOR CYCLIC ROW NUMBER =============== // 
        if (toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW)
        {
          std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpRow) );
//          toCpRow = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpRow= %d", toCpRow);
        }

        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, toCpRow, toCpRow+1, nCols) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, 0, nCols, toCpRow, toCpRow+1) );
        }       
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_ROW )
        {
          ++toCpRow;
          if( toCpRow == nRows ){
				    toCpRow = 0;	
			    }
        }
      }
  
      // =============== READ ONE COL =============== // 
      if (toCp == CRAFT_2D_ARRAY_CP_ONE_COL || toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL){
        // =============== CHECK FOR CYCLIC COL NUMBER =============== // 
        if (toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL)
        {
          std::string filenameMD;
			    filenameMD = filename + ".metadata";
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_ASCII_File_read_POD(filenameMD, &toCpCol) );
//          toCpCol = CRAFT_readASCIICounterFromFile<int>(filenameMD);
			    craftDbg(4, "Restarting with toCpCol = %d", toCpCol);
        }

	      craftDbg(4, "CpMultiArray::readSerialBIN(): toCp = CRAFT_2D_ARRAY_CP_ONE_COL");
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_ROWMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = ROWMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, 0, nRows, toCpCol, toCpCol+1) );
        }
        if(mulArrLayout == CRAFT_2D_ARRAY_LAYOUT_COLMAJ)  
        {
	        craftDbg(4, "CpMultiArray::readSerialBIN(): layout = COLMAJ");
          CRAFT_CHECK_ERROR ( CRAFT_SERIAL_BIN_File_read_2D (filename, dataPtr, toCpCol, toCpCol+1, nRows) );
        }
        if ( toCp == CRAFT_2D_ARRAY_CP_ONE_CYCLIC_COL )
        {
          ++toCpCol;
  			  if( toCpCol == nCols ){
  				  toCpCol = 0;	
  			  }
        }
        return EXIT_SUCCESS;
      }
  return EXIT_SUCCESS;
}


template <class T>
void CpMultiArray<T>::print(){
	std::cout << "dataPtr:" << std::endl;
	for(size_t i = 0; i < this->nRows; ++i){
		for(size_t j = 0; j < this->nCols; ++j){
			std::cout << dataPtr[i][j] << "\t";
		}
		std::cout << std::endl;
	}
//	std::cout << "asynData:" << std::endl;
//	for(size_t i = 0; i < this->nRows; ++i){
//		for(size_t j = 0; j < this->nCols; ++j){
//			std::cout << asynData[j][i] << "\t";
//		}
//		std::cout << std::endl;
//	}
	return;
}

#endif
