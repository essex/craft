#ifndef __CRAFTDEBUGFUNCS_HPP__
#define __CRAFTDEBUGFUNCS_HPP__

#include <string>
#include <mpi.h>

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define CRAFT_CHECK_ERROR(val) if(val==EXIT_FAILURE){ \
    craftErr("The function %s exits with EXIT_FAILURE status @ %s:%d", \
            __FUNCTION__, __FILE__, __LINE__ \
            ); \
    return EXIT_FAILURE; \
  } else {}


void craftErr(const char *fmt, ...);
void craftDbg(int level, const char *fmt, ...);
void craftDbgfname(int level, const char* funcName, const char *fmt, ...);
void craftWarning(int level, const char *fmt, ...);
void craftAbort(const char *fmt, ...);
void craftTime(const std::string str);
void craftTime(const std::string str, const MPI_Comm * const comm);
//int craftLog(MPI_Comm const * comm, const char *fmt, ...);
void CRAFT_getWalltime(double* wcTime);

#endif // __CRAFTDEBUGFUNCS_HPP__
