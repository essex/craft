#ifndef __CRAFTCONF_H__
#define __CRAFTCONF_H__

#include <string>
// 1 = enable , 0 = disable
// 1 = true   , 0 = false


#ifndef CRAFT_ENABLE
#define CRAFT_ENABLE (true)
#endif

#ifndef CRAFT_DEBUG
#define CRAFT_DEBUG (false)
#endif

#ifndef CRAFT_DEBUG_LOG
#define CRAFT_DEBUG_LOG (true)
#endif


#ifndef CRAFT_DEBUG_PROC
#define CRAFT_DEBUG_PROC (0)
#endif

#ifndef CRAFT_READ_CP_ON_RESTART 
#define CRAFT_READ_CP_ON_RESTART (true)
#endif

#ifndef CRAFT_CP_PATH
#define CRAFT_CP_PATH "./"
#endif

#ifndef CRAFT_USE_SCR
#ifdef SCR         // if SCR is define, then by default its status is ON (1)
#define CRAFT_USE_SCR (true)
#else
#define CRAFT_USE_SCR (false)
#endif
#endif

#ifndef CRAFT_COMM_RECOVERY_POLICY 
#define CRAFT_COMM_RECOVERY_POLICY "NON-SHRINKING"
#endif

#ifndef CRAFT_COMM_SPAWN_POLICY
#define CRAFT_COMM_SPAWN_POLICY   "NO-REUSE"
#endif

#ifndef CRAFT_TIMING
#define CRAFT_TIMING (true)
#endif

#ifndef CRAFT_WRITE_ASYNC
#define CRAFT_WRITE_ASYNC (false)
#endif

#ifndef CRAFT_WRITE_ASYNC_ZERO_COPY
#define CRAFT_WRITE_ASYNC_ZERO_COPY (false)
#endif

#ifndef CRAFT_COMM_RECOVERY_CP_READ_POLICY
#define CRAFT_COMM_RECOVERY_CP_READ_POLICY "NEW_RANK"       // in case of SHRINKING recovery, the default val should be OLD_RANK
#endif


void CRAFT_getEnvParam();

extern int craftDebug;
extern int craftDebugLog;
extern int craftDebugProc;
extern int craftEnabled;
extern int craftReadCpOnRestart;
extern std::string craftCpPath;
extern int craftUseSCR;
extern std::string craftCommRecoveryPolicy;
extern std::string craftCommSpawnPolicy;
extern std::string nodeFile;
extern int craftTiming;
extern std::string craftScrCpName;

extern int craftWriteAsync;
extern int craftWriteAsyncZeroCopy;
extern int craftMakeAsyncCopy;     // NOTE: This parameter gets its value depending on craftWriteAsync & craftWriteAsyncZeroCopy and is the only parameter used mainly inside CRAFT.
extern int craftNumBufferCp;

extern char * craftLogFile;

extern bool craftInvalidateNestedCp;
extern bool craftSignalForCp;

extern std::string craftCommRecoveryCpReadPolicy;

#endif
