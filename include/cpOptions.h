#ifndef __CPOPTIONS_H__
#define __CPOPTIONS_H__

#include "cpHelperFuncs.hpp"
#include "craftDebugFuncs.hpp"
#include <iostream>
using namespace cpHelperFuncs;


#include <string.h>
using namespace std;

template <class T>
int CRAFT_getEnvVar(std::string strToMatch, T * val)
{
  int myrank;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  char* envIn;
  envIn = getenv (strToMatch.c_str());
  if (envIn!=NULL){
    T readVar;
    std::string envInStr;
    envInStr = envIn; 
    readVar = stringToNumber<int>(envInStr);
    *val = readVar;
    if(myrank==0) 
    {
      std::cout << strToMatch << "= " << *val << std::endl;
    }
    return EXIT_SUCCESS;
  }
  else {
  	craftWarning(0, "%s value has not been set", strToMatch.c_str() );
    return EXIT_FAILURE;
  }
}

namespace cpOpt {
class CpOptions{
private:
  int cpFreq;
  int nIter;

public:
  CpOptions();
  ~CpOptions();

  void setCpFreq ( const int cpFreq_);
  int getCpFreq();
  void setnIter(const int nIter_);
  int getnIter();
  
};
}

#endif
