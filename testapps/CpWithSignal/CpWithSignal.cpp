// ==================================================================================== 
//  This code tests the CRAFTs functionality of checkpointing using external signals
//  It resolve the possible skew between iterations of different processes before taking checkpoint.
//
// ====================================================================================
//  Intel MPI: export I_MPI_JOB_SIGNAL_PROPAGATION=1; mpirun -np 3 ./CpWithSignalAndSkew.bin -niter 29
//  Open MPI: mpirun -mca orte_forward_job_control 1 -np 3 ./CpWithSignalAndSkew.bin -niter 29
// ====================================================================================


#include <craft.h>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;
using namespace cpOpt;

int read_params(int argc, char* argv[] , CpOptions * myCpOpt){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
	}
  delete[] tmp;
}

int createSkew(int sleepTime)
{
  sleep(sleepTime); 
  return 0;
}



int main(int argc, char* argv[])
{
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  printf("thread support level %d\n", provided);

  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt); 


  int n = 5;
  int myint = 0;
  double mydouble = 0.0123;
  int iteration = 1; 

{
  Checkpoint myCP( "minimalCP", FT_Comm);

  CRAFT_CHECK_ERROR(  myCP.add("myint", &myint) );
  CRAFT_CHECK_ERROR(  myCP.add("mydouble", &mydouble) );
  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration) );

  CRAFT_CHECK_ERROR(  myCP.commit() ); 
   
  double t1=0.0, t2=0.0; 

  myCP.restartIfNeeded(&iteration);

  CRAFT_getWalltime(&t1);

  for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    myint++; mydouble++;

    { printf("%d:=== iter: %d , myint: %d \n", myrank, iteration, myint );}

    if( iteration == 3){ createSkew(myrank); }
    myCP.updateAndWriteAtSignal( iteration );
    usleep(1000000);

  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);

  CRAFT_CHECK_ERROR(  myCP.wait() );
  CRAFT_CHECK_ERROR(  myCP.read() );
  if(myrank==0) { printf("Last checkpoint vals: \n %d:=== iter: %d , myint: %d, mydouble: %f \n", myrank, iteration, myint, mydouble);}
  
}

#ifdef AFT
    AFT_END()
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}


