// USAGE
// mpirun_rrze -np <1> ./multilevelCP.bin
//

#include <cstring>
#include <unistd.h>

#ifdef AFT
	#include <aft.h>
	#include <aft_macros.h>
#endif

#include "multilevelCP.h"
#include <checkpoint.hpp>

int myrank;

template < typename T>
void printArray( T * ptr, const int nRows){
	for(size_t i = 0; i < nRows; ++i){
		if(myrank==0) std::cout << ptr[i] << std::endl;
	}
	return;
}


int read_params(int argc, char* argv[] , CpOptions * myCpOpt){
/*
	char * tmp = new char[256];
	std::string cpPathTemp ;
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
	}
*/
  return 0;
}

int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
  int numprocs;
//	===== AFT BEGIN =====
  MPI_Comm FT_Comm;
	MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
#ifdef AFT
   	AFT_BEGIN(FT_Comm, &myrank, argv);	
#endif 

//	CpOptions * myCpOpt = new CpOptions[1];
	MPI_Comm_rank(FT_Comm, &myrank);
	MPI_Comm_size(FT_Comm, &numprocs);
//  read_params(argc, argv, myCpOpt); 

	int n = 2;
	double a 	= 0.;
	double * b 	= new double[n];
	for(int i = 0; i < n; ++i){
			b[i] = 0.0;
	}
	
	int nL1Iter = 5,  L1Iter=0, cpL1Freq=1;
	int nL2Iter = 50, L2Iter=0, cpL2Freq=10;
	
	Checkpoint  cpL1("cpL1", FT_Comm);
	Checkpoint  cpL2("cpL2", FT_Comm);
  cpL2.disableSCR();
  cpL1.disableSCR();
	cpL1.add("a", &a);
	cpL1.add("L1Iter", &L1Iter);
	cpL1.commit(); 
	cpL2.add("b", b);
	cpL2.add("L2Iter", &L2Iter);
	cpL2.commit(); 
/* instead of: 
	if( cpL1.needRestart() == true) {
	  printf("RESTART L1------> failed == true \n");
    if(cpL1.read()==EXIT_SUCCESS){
		  L1Iter++;
		  if(myrank==0) std::cout << "L1Iter =" << L1Iter << std::endl;
    }
	}
I want to write:
*/
  cpL1.restartIfNeeded(&L2Iter); // with an optional int* argument

  printf("%d: before for loop\n", myrank );
  for(; L1Iter < nL1Iter; L1Iter++)
  {
			std::cout << "L1Iter:" << L1Iter << std::endl;
			L2Iter = 0;
/*
			if( cpL2.needRestart() == true )
      {
				printf("RESTART L2------> failed == true \n");
				if(cpL2.read()==EXIT_SUCCESS){
				  L2Iter++;
				  printf("L2Iter = %d \n", L2Iter);
        }
			}
*/
                cpL2.restartIfNeeded(&nL2Iter);
  		for(; L2Iter < nL2Iter ; L2Iter++)
  		{
				for(size_t i = 0; i < n ; ++i){
					b[i] += 0.1;
				}
				usleep(200000);
        MPI_Barrier(FT_Comm);
        printf("=== L2Iter: %d, \tb[0]: %f \n", L2Iter, b[0] );
				if( (L2Iter % cpL2Freq) == 0){
          printf("Checkpointing L2...\n");
					cpL2.update();
					cpL2.write();
				}
			}
			a = b[0];
			usleep(200000);
      printf("============ L1Iter: %d, \ta: %f \n", L1Iter, a);
/* instead of:
			if(L1Iter % cpL1Freq == 0){
          printf("Checkpointing L1...\n");
					cpL1.update();
					cpL1.write();
			}
*/
                cpL1.updateAndWrite(nL1Iter,cpL1Freq);// (with default parameters i=1,freq=1)
	}
  if(myrank==0) std::cout <<("1before AFT_END\n");
#ifdef AFT
	AFT_END();
#endif

  if(myrank==0) std::cout <<"before MPI_Finalize\n";
	MPI_Finalize();
	return 0;
}


