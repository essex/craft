// USAGE
// WARNING: NESTED_LEVEL checkpointing feature is in TESTING phase.
// mpirun -np <1> ./nestedCP_2subCPs.bin
//

#include <cstring>
#include <unistd.h>

#ifdef AFT
	#include <aft.h>
	#include <aft_macros.h>
#endif

#include <checkpoint.hpp>

int myrank;

template < typename T>
void printArray( T * ptr, const int nRows){
	for(size_t i = 0; i < nRows; ++i){
		if(myrank==0) std::cout << ptr[i] << std::endl;
	}
	return;
}




int main(int argc, char* argv[])
{
	MPI_Init(&argc, &argv);
  int numprocs;
//	===== AFT BEGIN =====
  MPI_Comm FT_Comm;
	MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
#ifdef AFT
   	AFT_BEGIN(FT_Comm, &myrank, argv);	
#endif 

	MPI_Comm_rank(FT_Comm, &myrank);
	MPI_Comm_size(FT_Comm, &numprocs);

	int n = 2;
	double a 	= 0.;
	double b1,b2 	= 0.0;
	float c 	= 0.0;
	
	int nL1Iter = 3,  L1Iter=1, cpL1Freq=1;
	int nL21Iter = 30, L21Iter=1, cpL21Freq=10;
	int nL22Iter = 30, L22Iter=1, cpL22Freq=10;
	
	Checkpoint  cpL1("cpL1", FT_Comm);
	Checkpoint  cpL21("cpL21", FT_Comm);
	Checkpoint  cpL22("cpL22", FT_Comm);
  cpL21.nestedCp(&cpL1);
  cpL22.nestedCp(&cpL1);
  // ============= define CP level 1 ============== // 
	cpL1.add("a", &a);
	cpL1.add("b2", &b2);                                         // as b2 retains its values in each level 1 iteration, its added in level 1 CP.
	cpL1.add("L1Iter", &L1Iter);
	cpL1.commit(); 

  // ============= define CP level 2.1 ============== // 
	cpL21.add("b1", &b1);
	cpL21.add("L21Iter", &L21Iter);
	cpL21.commit(); 

  // ============= define CP level 2.2 ============== // 
	cpL22.add("b2", &b2);
	cpL22.add("L22Iter", &L22Iter);
	cpL22.commit(); 



  cpL1.restartIfNeeded(&L1Iter);
  // ============= level 1 ============== // 
  for(; L1Iter <= nL1Iter; L1Iter++)
  {
      if(myrank==0) printf("F1: ======================== L1Iter: %d, \ta: %f \n", L1Iter, a);
      L21Iter=1;
      b1 = 0.0;
      cpL21.restartIfNeeded(&L21Iter);
      // ============= level 2.1 ============== // 
  		for(; L21Iter <= nL21Iter ; L21Iter++)
  		{
        b1 += 0.01;
				usleep(100000);
        MPI_Barrier(FT_Comm);
        if(myrank==0) printf("=== L21Iter: %d, \tb1: %f \n", L21Iter, b1 );
        cpL21.updateAndWrite(L21Iter, cpL21Freq); 
			}
     
      L22Iter=1;  
      cpL22.restartIfNeeded(&L22Iter);
      // ============= level 2.2 ============== // 
   		for(; L22Iter <= nL22Iter ; L22Iter++)
  		{
        b2 += 0.01;                                         // NOTE: value of b2 is NOT reset in each L2 iteration. previous calculated values carry on.
				usleep(100000);
        MPI_Barrier(FT_Comm);
        if(myrank==0) printf("=== L22Iter: %d, \tb2: %f \n", L22Iter, b2 );
        cpL22.updateAndWrite(L22Iter, cpL22Freq); 
			}     


			a += 100*b1+b2;
      if(myrank==0) printf("F2: ======================== L1Iter: %d, \ta: %f \n", L1Iter, a);
      cpL1.updateAndWrite(L1Iter, cpL1Freq); 

			usleep(200000);
      MPI_Barrier(FT_Comm);
	}
  if(myrank==0) std::cout <<("1before AFT_END\n");
#ifdef AFT
	AFT_END();
#endif

  if(myrank==0) std::cout <<"before MPI_Finalize\n";
	MPI_Finalize();
	return 0;
}


