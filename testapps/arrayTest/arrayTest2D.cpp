// ==================================================================================== 
//  This code tests the CRAFT POD and POD-array checkpoint functions. 
//  The program make regular checkpoints.
//  At the end of the program run, the POD and POD-array values are 
//  reset and checkpoints are read.
// ====================================================================================
//  mpirun -np 2 ./arrayTest.bin -niter 29 -cpfreq 10
// ====================================================================================

#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>

#include <craft.h>
#include <mpi.h>

using namespace cpOpt;

template < typename T>
void printArray( const std::string s, const T * const ptr, const int nRows, const MPI_Comm comm ){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nRows; ++i){
		    std::cout << ptr[i] << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}

template < typename T>
void printDoubleArrayCMM(const std::string s, T ** const ptr, const int nRows, const int nCols, const MPI_Comm comm){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nCols; ++i){
		    for(size_t j = 0; j < nRows; ++j){
			    std::cout << ptr[i][j] << "\t";
		    }
		    std::cout << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}



template < typename T>
void printDoubleArrayRMM(const std::string s, T ** const ptr, const int nRows, const int nCols, const MPI_Comm comm){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nRows; ++i){
		    for(size_t j = 0; j < nCols; ++j){
			    std::cout << ptr[i][j] << "\t";
		    }
		    std::cout << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}


int read_params(int argc, char* argv[] , CpOptions * myCpOpt){
	char * tmp = new char[256];
	std::string cpPathTemp ;
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
	}
}

template <class T>
int initRMM(T ** rmm, int nRows, int nCols, T val){
	for(int i=0; i<nRows; ++i){
		for(int j=0; j<nCols; ++j){
			rmm[i][j] = val; 
		}
	}
  return 0;
}

template <class T>
int initRMM(T ** rmm, int nRows, int nCols){
	for(int i=0; i<nRows; ++i){
		for(int j=0; j<nCols; ++j){
			rmm[i][j] = i+(0.1*j) ; 
		}
	}
  return 0;
}


template <class T>
int initCMM(T ** cmm, int nRows, int nCols, T val){
	
  for(int i=0; i<nCols; ++i){
		for(int j=0; j<nRows; ++j){
			cmm[i][j] = val;
		}
	}
  return 0;
}

template <class T>
int initCMM(T ** cmm, int nRows, int nCols){
	
  for(int i=0; i<nCols; ++i){
		for(int j=0; j<nRows; ++j){
			cmm[i][j] = j+(0.1*i) ;
		}
	}
  return 0;
}

int testRowMajorMatrixCheckpointing  ( const int nRows, 
                                       const int nCols,
                                       CpOptions * const myCpOpt,
                                       MPI_Comm * FT_Comm
                                     )
{
	int iteration = 0, myrank =-1;
	MPI_Comm_rank(*FT_Comm, &myrank);
  int nDims=2;
  int * dims = new int[nDims];
  dims[0] = nRows;
  dims[1] = nCols;


  float ** rmm;
  rmm = new float*[nRows];
  for(int i=0; i<nRows; ++i){
		rmm[i] = new float[nCols];	
	} 
  initRMM(rmm, nRows, nCols );

  CpAbleProp myPropSB("SERIALIO, BIN"); 
  CpAbleProp myPropSA("SERIALIO, ASCII"); 
  CpAbleProp myPropMB("MPIIO, BIN"); 
  
	printDoubleArrayRMM("Array RMM", rmm, nRows, nCols, *FT_Comm);

  Checkpoint  myCP("arrayTestRMM", *FT_Comm);
  myCP.add("rmm", rmm, nDims, dims, " 1, x", CRAFT_2D_ARRAY_LAYOUT_ROWMAJ, myPropMB );
	myCP.add("iteration", &iteration, myPropMB);
	myCP.commit(); 


  CRAFT_CHECK_ERROR( myCP.restartIfNeeded(&iteration));


	for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    for(int i=0; i<nRows; ++i){
  		for(int j=0; j<nCols; ++j){
  			rmm[i][j] += 0.00001 ; 
  		}
  	}   
		printf("%d: === iter: %d\t rmm[0][0]: %f\n", myrank, iteration, rmm[0][0]);
    myCP.updateAndWrite(iteration , myCpOpt->getCpFreq() );
		MPI_Barrier(*FT_Comm);
  }

  initRMM(rmm, nRows, nCols, (float)-77.7 );
	printDoubleArrayRMM("after reinit ", rmm, nRows, nCols, *FT_Comm);
  myCP.read(); 
	printDoubleArrayRMM("after read ", rmm, nRows, nCols, *FT_Comm);
  
  return 0;
}

int testColMajorMatrixCheckpointing  ( const int nRows, 
                                       const int nCols,
                                       CpOptions * const myCpOpt,
                                       MPI_Comm * FT_Comm
                                     )
{
	int iteration = 0, myrank =-1;
	MPI_Comm_rank(*FT_Comm, &myrank);
  int nDims=2;
  int * dims = new int[nDims];
  dims[0] = nRows;
  dims[1] = nCols;

  float ** cmm;
  cmm = new float*[nCols];
  for (int i=0; i< nCols; ++i){
    cmm[i] = new float [nRows];
  }
  initCMM(cmm, nRows, nCols );
	printDoubleArrayCMM("Array CMM", cmm, nRows, nCols, *FT_Comm);

  CpAbleProp myPropSB("SERIALIO, BIN"); 
  CpAbleProp myPropSA("SERIALIO, ASCII"); 
  CpAbleProp myPropMB("MPIIO, BIN"); 

  Checkpoint  myCP("arrayTestCMM", *FT_Comm);
  myCP.add("cmm", cmm, nDims, dims, "CYCLIC, x", CRAFT_2D_ARRAY_LAYOUT_COLMAJ, myPropMB ) ;
//  myCP.add("cmm", cmm, nDims, dims, "x, 2", CRAFT_2D_ARRAY_LAYOUT_COLMAJ, myPropMB ) ;
	myCP.add("iteration", &iteration, myPropMB);
	myCP.commit(); 

  CRAFT_CHECK_ERROR( myCP.restartIfNeeded(&iteration));
  
	for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    for(int i=0; i<nCols; ++i){
  		for(int j=0; j<nRows; ++j){
  			cmm[i][j] += 0.00001 ; 
  		}
  	}   
		printf("%d: === iter: %d\t cmm[0][0]: %f\n", myrank, iteration, cmm[0][0]);
    myCP.updateAndWrite(iteration , myCpOpt->getCpFreq() );
		MPI_Barrier(*FT_Comm);
  }

  initCMM(cmm, nRows, nCols, (float)-77.7 );
	printDoubleArrayCMM("after reinit ", cmm, nRows, nCols, *FT_Comm);
  myCP.read(); 
	printDoubleArrayCMM("after read ", cmm, nRows, nCols, *FT_Comm);
  
  return 0;
}


int main(int argc, char* argv[])
{
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
{
  printf("thread support level %d\n", provided);
  int myrank, numprocs;
//	===== AFT BEGIN =====
  MPI_Comm FT_Comm;
	MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
#ifdef AFT
   	AFT_BEGIN(FT_Comm, &myrank, argv);	
#endif 

	MPI_Comm_rank(FT_Comm, &myrank);
	MPI_Comm_size(FT_Comm, &numprocs);
	printf("%d/%d\n", myrank, numprocs);

	CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt); 
  
  double mydouble = 0.01234;
	int nRows=3+myrank, nCols=5;


  testRowMajorMatrixCheckpointing  ( nRows, nCols, myCpOpt, &FT_Comm );
//  testColMajorMatrixCheckpointing  ( nRows, nCols, myCpOpt, &FT_Comm );

#ifdef AFT
	AFT_END();
#endif
}
	MPI_Finalize();
	return 0;
}

