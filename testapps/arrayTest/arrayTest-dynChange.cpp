// ==================================================================================== 
//  This code tests the CRAFT POD-array checkpoint functions, in which size and pointer
//  of the array can change during the program run.
// ====================================================================================
// 
//  export CRAFT_cpFreq=5
//  export CRAFT_nIter=20
//  export CRAFT_DEBUG=2
//  mpirun -np 2 ./arrayTest-dynChange.bin
// ====================================================================================

#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <mpi.h>

#include <craft.h>

using namespace cpOpt;

template < typename T>
void printArray( const std::string s, const T * const ptr, const int nElem, const MPI_Comm comm){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nElem; ++i){
		    std::cout << ptr[i] << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}


int main(int argc, char* argv[])
{
	int iteration = 1;
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  printf("thread support level %d\n", provided);
  int myrank, numprocs;
// ===== AFT BEGIN =====
  MPI_Comm FT_Comm;
	MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
  MPI_Comm_rank(FT_Comm, &myrank);
	MPI_Comm_size(FT_Comm, &numprocs);
	printf("%d/%d\n", myrank, numprocs);
  int myCSize;
  MPI_Type_size( MPI_CXX_FLOAT_COMPLEX, &myCSize );
{
#ifdef AFT
   	AFT_BEGIN(FT_Comm, &myrank, argv);	
#endif 

  int cpFreq , nIter;
  CRAFT_getEnvVar("CRAFT_cpFreq", &cpFreq);
  CRAFT_getEnvVar("CRAFT_nIter", &nIter);


  double mydouble = 0.01234;
  int nElem = 20;
  size_t nElemCP = 0;
  
	printf("sizeof(float)=%d\n", sizeof(float));

	double * d 	= new double[nElem];
	for(int i = 0; i < nElem; ++i){
	  d[i] = ((double)myrank) + double(i) + 0.55;
	}
//	printArray("Array d", d, nElem, FT_Comm);

	Checkpoint  myCP("arrayTest", FT_Comm);
  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, ASCII"); 
	myCP.add("iteration", &iteration, mypropSA);
	myCP.add("d", d, &nElemCP, mypropMB );  
	myCP.add("nElemCP", &nElemCP, mypropSA );  
	myCP.commit(); 
   
  if(myCP.needRestart())  
  { 
	 if(myrank==0) printf("=========== RESTART PHASE 1 =========== \n" );
    myCP.read("nElemCP");
  }
  // Do something based on nElemCP 
	printf("nElemCP = %d\n", nElemCP);

  if(myCP.needRestart()) 
  { 
	  if(myrank==0) printf("=========== RESTART PHASE 2 =========== \n" );
    myCP.read();    // read all checkpoint.
    iteration++; 
  }

	if(myrank==0) printf("iteration= %d\n", iteration);

	for(; iteration <= nIter ; iteration++)
  {
		for(size_t i = 0; i <= nElemCP ; ++i){
			d[i] += double(i); 
		}
    nElemCP++;
    usleep(100000);
		printf("%d: === iter: %d\t d[0]: %f\n", myrank, iteration, d[0]);

    if (iteration % cpFreq == 0 )
    {
      myCP.updateAndWrite();
    }
		MPI_Barrier(FT_Comm);
  }

  delete[] d;

#ifdef AFT
	AFT_END();
#endif
  MPI_Barrier(MPI_COMM_WORLD);
}

	MPI_Finalize();
	return 0;
}

