// ==================================================================================== 
//  This code tests the CRAFT POD and POD-array checkpoint functions. 
//  The program make regular checkpoints.
//  At the end of the program run, the POD and POD-array values are 
//  reset and checkpoints are read.
// ====================================================================================
// export CRAFT_nIter=50
// export CRAFT_cpFreq=10
//  mpirun -np 2 ./arrayTest.bin 
// ====================================================================================

#include <vector>
#include <string>
#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include <malloc.h>
#include <mpi.h>

#include <craft.h>

using namespace cpOpt;

template < typename T>
void printArray( const std::string s, const T * const ptr, const int nRows, const MPI_Comm comm){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nRows; ++i){
		    std::cout << ptr[i] << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}

int main(int argc, char* argv[])
{
	int iteration = 0;
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
{
  printf("thread support level %d\n", provided);
  int myrank, numprocs;
// ===== AFT BEGIN =====
  MPI_Comm FT_Comm;
	MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
#ifdef AFT
   	AFT_BEGIN(FT_Comm, &myrank, argv);	
#endif 

	MPI_Comm_rank(FT_Comm, &myrank);
	MPI_Comm_size(FT_Comm, &numprocs);
	printf("%d/%d\n", myrank, numprocs);

  double mydouble = 0.01234;
	size_t nRows=3+myrank;
	printf("sizeof(float)=%d\n", sizeof(float));

  size_t dCharSize = nRows*sizeof(double);
	double * d 	= new double[nRows];
	for(int i = 0; i < nRows; ++i){
	  d[i] = ((double)myrank) + 0.55;
	}

  int cpFreq , nIter;
  CRAFT_getEnvVar("CRAFT_cpFreq", &cpFreq);
  CRAFT_getEnvVar("CRAFT_nIter", &nIter);

	printArray("Array d", d, nRows, FT_Comm);
  size_t nCharArray = 20;
  char * myCharArray = new char[nCharArray];
  
	Checkpoint  myCP("arrayTest", FT_Comm);
  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, ASCII"); 
	myCP.add("iteration", &iteration, mypropMB);
	myCP.add("d", d, &nRows, mypropSB );  
//	myCP.add("d", (char*)d, &dCharSize,  mypropSB);  
//	myCP.add("a", myCharArray , &nCharArray);  
	myCP.commit(); 

  myCP.restartIfNeeded(&iteration);
  usleep(100000);
  MPI_Barrier(FT_Comm);

	for(; iteration <= nIter; iteration++)
  {
		for(size_t i = 0; i < nRows ; ++i){
			d[i] += 1.0;
		}
		printf("%d: === iter: %d\t d[0]: %f\n", myrank, iteration, d[0]);
    myCP.updateAndWrite(iteration, cpFreq);
//    myCP.wait();
    usleep(100000);
		MPI_Barrier(FT_Comm);
  }


//	printArray("Array d", d, nRows, FT_Comm);
	for(int i = 0; i < nRows; ++i){
	  d[i] = ((double)1);
	}

	printArray("After reinit Array d", d, nRows, FT_Comm);
//	printDoubleArray("After reinit Array AA", rmm , nRows, nCols, FT_Comm);

  CRAFT_CHECK_ERROR( myCP.wait()) ;
  CRAFT_CHECK_ERROR( myCP.read()) ;
	printArray("After read Array d", d, nRows, FT_Comm);
		printf("%d: === iter: %d\t d[0]: %f\n", myrank, iteration, d[0]);
//	printDoubleArray("After read Array AA", rmm , nRows, nCols, FT_Comm);

  delete[] d;

#ifdef AFT
	AFT_END();
#endif
}
	MPI_Finalize();
	return 0;
}

