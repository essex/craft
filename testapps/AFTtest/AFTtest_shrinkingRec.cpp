// ==================================================================================== 
//  This code tests AFT feature of CRAFT. 
//  The program make regular checkpoints.
//  At a predetermined iteration, one of the processes (rank=1) dies.
//  The AFT(ULFM) feature rebuilds the broken communicator.
// ====================================================================================
//   mpirun -am ft-enable-mpi -np 4 ./AFTtest_shrinkingRec.bin -cpfreq 10 -niter 19 -failIter 15 -failProcNum 2 
// ====================================================================================


#include <aft.h>
#include <craft.h>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;
using namespace cpOpt;

template < typename T>
void printArray( const std::string s, const T * const ptr, const int nRows, const MPI_Comm comm){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
	    for(size_t i = 0; i < nRows; ++i){
		    std::cout << ptr[i] << std::endl;
	    }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}

bool getFirstRunStatusFromMaster(int firstRun , MPI_Comm FT_Comm){ // it is necessary to get the first-run status from the zero process, because for the recovery process, the first run will be the restarted run for the zero process.
  int myrank = -1;
  MPI_Comm_rank(FT_Comm, &myrank);
  if(myrank==0){
    MPI_Send(&firstRun, 1, MPI_INT, 1, 0, FT_Comm);
  } 
  if(myrank==1){
    MPI_Status status;
    MPI_Recv(&firstRun, 1, MPI_INT, 0, 0, FT_Comm, &status); 
  }
  return firstRun;
}
int read_params(int argc, char* argv[] , CpOptions * myCpOpt, int * failIter, int * failProcNum){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter: " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq: " << myCpOpt->getCpFreq() << std::endl;
		}
    if ((!strcmp(argv[i], "-failIter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*failIter = ( atoi(tmp) );
			if(myrank==0) std::cout << "failIter: " << *failIter << std::endl;
		}
    if ((!strcmp(argv[i], "-failProcNum"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*failProcNum = ( atoi(tmp) );
			if(myrank==0) std::cout << "failProcNum: " << *failProcNum << std::endl;
		}
	}
  delete[] tmp;
  return 0;
}

int main(int argc, char* argv[])
{
  int provided, failIter=-99, failProcNum;
  int firstRun=true;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  printf("thread support level %d\n", provided);

  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

	MPI_Comm_size(FT_Comm, &numprocs);

  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt, &failIter, &failProcNum); 


  int n = 5;
  int myint = myrank;
  size_t nElem = 4;
	double * d 	= new double[nElem];
	for(int i = 0; i < nElem; ++i){
	  d[i] = ((double)myrank) + 0.55;
	}
  int iteration = 1; 

{
  Checkpoint myCP( "AFTtest-shrinking-CP", FT_Comm);

  CRAFT_CHECK_ERROR(  myCP.add("myint", &myint) );
  CRAFT_CHECK_ERROR(  myCP.add("d", d, &nElem) );
  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration) );
  CRAFT_CHECK_ERROR(  myCP.commit() ); 
   
  double t1=0.0, t2=0.0; 

  int pastRank = myrank;
  #ifdef AFT
    if ( AFT_isRecoveryRun() == true ){
      int numFailed = AFT_getNumFailedProc (); 
      int * failureProcList = new int[numFailed];
      AFT_getFailedProcList(failureProcList);
      pastRank = AFT_getPastRank(); 
      printf("XX AFT_RECOVERY_RUN is TRUE, numFailed=%d, newRank= %d, PastRank= %d, OrigNumProc=%d \n", numFailed, myrank, pastRank, AFT_getOrigNumProc() );     // NOTE: this solution will not work in case of non-shrinking recovery.
      
          for (int i=0; i<numFailed; ++i)
          {
            printf("%d: failed Proc = %d \n", myrank, failureProcList[i]);
          }
    }
  #endif 

  myCP.restartIfNeeded(&iteration);       // in case of shrinking recovery, could there be a special case?

  CRAFT_getWalltime(&t1);

  for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    myint++; 

	  for(int i = 0; i < nElem; ++i){
      d[i] += 1.0;
    }
    {   
      if ( myrank == 0 ) printf ( "======\n" ); 
	    MPI_Comm_rank(FT_Comm, &myrank);
	    MPI_Comm_size(FT_Comm, &numprocs);

      for(int p=0; p<numprocs ; ++p){
        if(myrank == p){
          printf("%d: === iter: %d , myint: %d, d: %f \n", myrank, iteration, myint, d[0]);
//		      printf("%d: === iter: %d\t myint: %d , d: %f, AFT_RECOVERY_RUN: %d\n", myrank, iteration, myint, d[0], AFT_RECOVERY_RUN);
        }
        usleep(100000);
        MPI_Barrier(FT_Comm);
      } 
    }

    myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());
    usleep(300000);
    
    /*
    if(iteration == failIter){
      bool doFail= getFirstRunStatusFromMaster(firstRun, FT_Comm);
      if ( (myrank == failProcNum && doFail == true) || (myrank == 1 && doFail == true))
      {
        printf("doFail status = %d\n", doFail);
        exit(-1);
      }
      firstRun = false;
    }
    */
    MPI_Barrier(FT_Comm);
  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);

}

#ifdef AFT
    AFT_END()
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}


