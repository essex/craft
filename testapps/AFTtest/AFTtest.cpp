// ==================================================================================== 
//  This code tests AFT feature of CRAFT. 
//  The program make regular checkpoints.
//  At a predetermined iteration, one of the processes (rank=1) dies.
//  The AFT(ULFM) feature rebuilds the broken communicator.
// ====================================================================================
//  mpirun -am ft-enable-mpi -np 2 ./AFTtest.bin -niter 29 -cpfreq 10 -failIter 15
// ====================================================================================


#include <craft.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <mpi.h>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;
using namespace cpOpt;

bool getFirstRunStatusFromMaster(int firstRun , MPI_Comm FT_Comm){ // it is necessary to get the first-run status from the zero process, because for the recovery process, the first run will be the restarted run for the zero process.
  int myrank = -1;
  MPI_Comm_rank(FT_Comm, &myrank);
  if(myrank==0){
    MPI_Send(&firstRun, 1, MPI_INT, 1, 0, FT_Comm);
  } 
  if(myrank==1){
    MPI_Status status;
    MPI_Recv(&firstRun, 1, MPI_INT, 0, 0, FT_Comm, &status); 
  }
  return firstRun;
}

int read_params(int argc, char* argv[] , CpOptions * myCpOpt, int * failIter){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter: " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq: " << myCpOpt->getCpFreq() << std::endl;
		}
    if ((!strcmp(argv[i], "-failIter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*failIter = ( atoi(tmp) );
			if(myrank==0) std::cout << "failIter: " << *failIter << std::endl;
		}
	}
  delete[] tmp;
  return 0;
}

int main(int argc, char* argv[])
{
  int provided, failIter=-99;
  int firstRun=true;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  printf("thread support level %d\n", provided);

  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt, &failIter); 


  int n = 5;
  int myint = 0;
  double mydouble = 0.0123;
  int iteration = 1; 

{
  Checkpoint myCP( "AFTtest-CP", FT_Comm);

  CRAFT_CHECK_ERROR(  myCP.add("myint", &myint) );
  CRAFT_CHECK_ERROR(  myCP.add("mydouble", &mydouble) );
  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration) );

  CRAFT_CHECK_ERROR(  myCP.commit() ); 
   
  double t1=0.0, t2=0.0; 

  myCP.restartIfNeeded(&iteration);

  CRAFT_getWalltime(&t1);

  for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    myint++; mydouble++;
    { printf("%d:=== iter: %d , myint: %d, mydouble: %f \n", myrank, iteration, myint, mydouble);}

    myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());
    usleep(300000);
    if(iteration == failIter){
      bool doFail= getFirstRunStatusFromMaster(firstRun, FT_Comm);
      printf("doFail status = %d\n", doFail);
//      if( doFail==true){
//        printf("process # 1 will now exit\n");
//        char * cmd=new char[256];
//        sprintf(cmd, "$CRAFT/tools/killproc.sh n 2 AFT");
//        if (myrank == 0) {
//          system(cmd); 
//        }
//      }
      firstRun = false;
    }
    MPI_Barrier(FT_Comm);
  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);

  CRAFT_CHECK_ERROR(  myCP.wait() );
  CRAFT_CHECK_ERROR(  myCP.read() );
  if(myrank==0) { printf("Last checkpoint vals: \n %d:=== iter: %d , myint: %d, mydouble: %f \n", myrank, iteration, myint, mydouble);}
  
}

#ifdef AFT
    AFT_END()
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}


