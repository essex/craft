// ==================================================================================== 
//  This code tests the CRAFT checkpointing function for std::vector. 
//  The program make regular checkpoints.
// ====================================================================================
//  mpirun -np 2 ./vector2DTest.bin -niter 29 -cpfreq 10 -nrows 100 -ncols 100
// ====================================================================================


#include "vectorTest.h"
#include <craft.h>
#include <vector>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;
using namespace cpOpt;

template <class T>
void print2DVector (std::vector<std::vector<T>> * vec, const char * fmt, ...)
{
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  char buff[256];
   va_list argp;
   va_start(argp, fmt);
  vsprintf(buff, fmt, argp);
  va_end(argp);
    int * num2DVecElems = new int[2]; 
    num2DVecElems[0] = vec->size();
    num2DVecElems[1] = (*vec)[0].size();

  for(int i=0; i<numprocs; ++i){
    MPI_Barrier(MPI_COMM_WORLD);
    if (myrank==i)
    {
      std::cout << buff << std::endl;
      for(auto veci : *vec)
      {
        for(auto vecj : veci)
          std::cout<<vecj<<" ";
        std::cout << std::endl;
      }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    usleep(200000);
  }
}

template <class T>
void mod2DVector (std::vector<std::vector<T>> * vec, const T val)
{

    static typename std::vector< std::vector<T> >::iterator row;
    static typename std::vector<T>::iterator col;

    for (row = vec->begin(); row != vec->end(); row++) {
      for (col = row->begin(); col != row->end(); col++) {
          *col += val;
      }
    }
}

int read_params(int argc, char* argv[] , CpOptions * myCpOpt, int * nRows, int * nCols){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
    if ((!strcmp(argv[i], "-nrows"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*nRows= atoi(tmp) ;
			if(myrank==0) std::cout << "Array nRows is " << *nRows<< std::endl;
		}
    if ((!strcmp(argv[i], "-ncols"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*nCols = atoi(tmp) ;
			if(myrank==0) std::cout << "Array nCols is " << *nCols << std::endl;
		}
	}
  delete[] tmp;
}

int main(int argc, char* argv[])
{
  int provided;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  printf("thread support level %d\n", provided);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 
  int nRows=1, nCols=1;
  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt, &nRows, &nCols); 


  int iteration = 0; 

  std::vector <std::vector<double> > vec2D(nRows, std::vector<double>(nCols, 1.1 *(myrank+1)));
 
//  print2DVector(&vec2D, "Vec2D ===== rank: %d, iter: %d =====", myrank, iteration);

  {

  Checkpoint myCP( "vector2D", FT_Comm);

  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, BIN"); 

//  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration) );
  CRAFT_CHECK_ERROR(  myCP.add("vec2D", &vec2D, mypropMB) );

  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  double t1=0.0, t2=0.0; 

//  CRAFT_CHECK_ERROR( myCP.restartIfNeeded(&iteration));
  
  MPI_Barrier(MPI_COMM_WORLD);
  CRAFT_getWalltime(&t1);


  myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());


  MPI_Barrier(MPI_COMM_WORLD);
  CRAFT_getWalltime(&t2);

  double dataSizeTotalMB = double(nRows*nCols)*8.0*(double)numprocs/1000000.0;
  double time = t2-t1;
  double BW = dataSizeTotalMB/(time);         // NOTE: only for 1 iteration.
  
  if(myrank==0) printf("DatasizeX : %f, Time = %f. BW =  %f \n", dataSizeTotalMB, time, BW);


/*
  for(; iteration <= myCpOpt->getnIter() ; iteration++) 
  {
    usleep(300000);
    print2DVector(&vec2D, "Vec DBL ===== rank: %d, iter: %d =====", myrank, iteration);
    mod2DVector(&vec2D, 0.1*(double)(myrank+1));

    myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());
    MPI_Barrier(FT_Comm);
  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);

  CRAFT_CHECK_ERROR(  myCP.wait() );
  CRAFT_CHECK_ERROR(  myCP.read() );
 */ 

  }

#ifdef AFT
    AFT_END()
#endif
    MPI_Finalize();

    return EXIT_SUCCESS;
}


