// ==================================================================================== 
//  This code tests the CRAFT checkpointing function for std::vector. 
//  The program make regular checkpoints.
// ====================================================================================
//  mpirun -np 2 ./vectorTest.bin -niter 29 -cpfreq 10 -nelems 100
// ====================================================================================


#include "vectorTest.h"
#include <craft.h>
#include <vector>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;
using namespace cpOpt;

template <class T>
void printVector (const std::vector<T> * const vec, const char * fmt, ...)
{
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  char buff[256];
   va_list argp;
   va_start(argp, fmt);
  vsprintf(buff, fmt, argp);
  va_end(argp);

  for(int i=0; i<numprocs; ++i){
    MPI_Barrier(MPI_COMM_WORLD);
    if (myrank==i)
    {
      std::cout << buff << std::endl;
      for( T i:(*vec) )
      {
        std::cout << i << " ";
      }
      std::cout << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
//    usleep(50000);
  }
}



template <class T>
void mod1DVector (std::vector<T> * vec, const T val)
{
    static typename std::vector<T>::iterator row;
    for (row = vec->begin(); row != vec->end(); row++) {
          *row+= val;
    }
}



int read_params(int argc, char* argv[] , CpOptions * myCpOpt, int *nElem){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
		if ((!strcmp(argv[i], "-nelems"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*nElem = atoi(tmp) ;
			if(myrank==0) std::cout << "Array nElem is " << *nElem << std::endl;
		}
	}
  delete[] tmp;
}

int main(int argc, char* argv[])
{
  int provided;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  printf("thread support level %d\n", provided);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 
  int nElem;
  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt, &nElem); 

  int iteration = 0; 

  std::vector<double> VD (nElem, 1.5);
  std::vector<int> VI (nElem, 1);  

  double * d 	= new double[nElem];
	for(int i = 0; i < nElem; ++i){
	  d[i] = ((double)myrank) + 0.55;
	}
  typename	std::vector<int>::iterator iIt;
  typename	std::vector<double>::iterator dIt;

{

  Checkpoint myCP( "vector1D", FT_Comm);

  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, BIN"); 

  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration) );
  CRAFT_CHECK_ERROR(  myCP.add("VD", &VD, mypropMB) );
  size_t nElem_ST = nElem;
//  CRAFT_CHECK_ERROR(  myCP.add("d", d, &nElem_ST, mypropMB) );
//  CRAFT_CHECK_ERROR(  myCP.add("VI", &VI, mypropMB) );

  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  double t1=0.0, t2=0.0; 

  
  MPI_Barrier(MPI_COMM_WORLD);
  CRAFT_getWalltime(&t1);


  myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());


  MPI_Barrier(MPI_COMM_WORLD);
  CRAFT_getWalltime(&t2);

  double dataSizeTotalMB = (double(nElem)*8.0*(double)numprocs)/1000000.0;
  double time = t2-t1;
  double BW = dataSizeTotalMB/(time);         // NOTE: only for 1 iteration.
  
  if(myrank==0) printf("Datasize : %f, Time = %f. BW =  %f \n", dataSizeTotalMB, time, BW);

/*
  iIt = VI.begin();
  dIt = VD.begin();
  CRAFT_CHECK_ERROR( myCP.restartIfNeeded(&iteration));
  for(; iteration <= myCpOpt->getnIter() ; iteration++, iIt++, dIt++)
  {
    usleep(300000);
//    VD.push_back( double(iteration) + (.001*(myrank+1)) );        // Changing the size of the vector each iteration.
//    VI.push_back(iteration);
//    printVector(&VI, "Vec INT ===== rank: %d, iter: %d =====", myrank, iteration);
//
    mod1DVector(&VD, 0.1*(double)(myrank+1));
    printVector(&VD, "Vec DBL ===== rank: %d, iter: %d =====", myrank, iteration);

    myCP.updateAndWrite( iteration, myCpOpt->getCpFreq());
    MPI_Barrier(FT_Comm);
  }
 
  CRAFT_getWalltime(&t2);
  
  if(myrank==0) printf("Total time is = %f. \n", t2 -t1);
*/

//  CRAFT_CHECK_ERROR(  myCP.wait() );
//  CRAFT_CHECK_ERROR(  myCP.read() );
  
}

#ifdef AFT
    AFT_END()
#endif
    MPI_Finalize();

    return EXIT_SUCCESS;
}


