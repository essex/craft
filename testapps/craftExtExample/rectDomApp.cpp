#include "rectDomApp.h"
#include <rectDomain.h>
#include <craft.h>

int read_params(int argc, char* argv[] , int * npToSpawn, std::string * spawnHost){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-npToSpawn"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			*npToSpawn = atoi(tmp);
			if(myrank==0) std::cout << "npToSpawn: " << *npToSpawn << std::endl;
		}
		if ((!strcmp(argv[i], "-spawnHost"))) {
			sprintf(tmp, "%s" ,argv[++i]);
      *spawnHost = tmp;
			if(myrank==0) std::cout << "spawnHost: " << *spawnHost << std::endl;
		}
	}
}




int main(int argc, char * argv[])
{
	MPI_Init(&argc, &argv);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);  
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);  
  Checkpoint myCP("rectDom1", MPI_COMM_WORLD);
  int iter = 1, cp_freq = 10; 
   
 
  if(myrank==0) printf("myrank:%d, numprocs %d\n", myrank, numprocs);
  int npToSpawn = 0;
  std::string spawnHost;
//  read_params(argc, argv, &npToSpawn, &spawnHost);

  rectDomain myrec(3, 4);
  
  myCP.add("iter", &iter);  
  myCP.add("myrect", &myrec);
  myCP.commit();
  for( ; iter <= 30; ++iter){
    printf("iter %d\n", iter);
    usleep(500000);
    if(iter % cp_freq ==0 )
    {
      myCP.update();
      myCP.write();
    }
  }
 
//  spawnMerge( origComm, &bigComm, argv, npToSpawn, spawnHost); 

  if(myrank==0) printf("==== End ===\n");
  MPI_Finalize();
  return 0;
}

