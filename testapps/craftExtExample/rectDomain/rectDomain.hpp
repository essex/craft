#ifndef __RECTDOMAIN_H__
#define __RECTDOMAIN_H__

#include<stdio.h>
#include<iostream>
#include<mpi.h>
#include<string>
#include<cstring>
#include<unistd.h>


class rectDomain 
{
public:
  rectDomain( const int length, const int width);
  rectDomain( const rectDomain &obj );
	~rectDomain();
private:
	int length; 
	int width;
  double * val; 
};

