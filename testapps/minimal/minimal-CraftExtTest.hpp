#ifndef __MINIMAL_CRAFTEXTTEST_HPP__
#define __MINIMAL_CRAFTEXTTEST_HPP__
#include <craft.h>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>


// ====================== CRAFT EXTENSION METHOD 2 ===================== //
class DataType1: public CpBase            // The case where original class could be changed.
{
public:
    DataType1(int a=200){ a_ = a;
      craftDbg(4, "DataType1::DataType1()");
    }
    ~DataType1(){
      craftDbg(4, "DataType1::~DataType1()");
    }
    int getVal(){ return a_; }
    int setVal(int val)
    { 
      a_ = val; 
      return 0; 
    }

private:
  int a_;
  int asynData;
  int update(){
    craftDbg(5, "DataType1::update()");
    asynData = a_;
    return EXIT_SUCCESS;
  }
	int write(const std::string filename){
    craftDbg(5, "DataType1::write() I write at %s", (filename).c_str());
	  std::ofstream fstr;
      
	  fstr.open ((filename).c_str(), std::ofstream::binary );	
	  if(fstr.is_open()){
		  fstr.write( (char *)&asynData, sizeof (a_) );
		  fstr.close();
      sync();
	  }
	  else{
	    int myrank;
	    MPI_Comm_rank(cpMpiComm, &myrank);
		  std::cerr << myrank << "Can't open file -- " << filename << std::endl;			
		  return EXIT_FAILURE;
	  }
    return EXIT_SUCCESS;
  }
	int read(const std::string filename){
    craftDbg(5, "DataType1::read() I read from %s", (filename).c_str());
    std::ifstream fstr;
	  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
	  if(fstr.is_open()){
		  fstr.read( (char *) (&a_) , sizeof(a_));	
		  fstr.close();
	  }
	  else{
		  std::cerr << "Can't open file " << filename << std::endl;			
		  return EXIT_FAILURE;
	  }
      return EXIT_SUCCESS;
    } 
};

// ====================== CRAFT EXTENSION METHOD 3 ===================== //
class DataType2                        // The case where original class could NOT be changed.
{
public:
  DataType2(int a=300){ a_=a;
    craftDbg(4, "DataType2::DataType2()");
  } 

  ~DataType2(){
    craftDbg(4, "DataType2::~DataType2");
  }
  int getVal(){ return a_; }
  int setVal(int val)
  { 
    a_ = val; 
    return 0; 
  }
private:
  int a_;
};

class cpDataType2: public CpBase{
public:
  cpDataType2( DataType2 * const dataPtr_, const MPI_Comm cpMpiComm_){
    craftDbg(4, "cpDataType2::cpDataType2()");
    dataPtr = dataPtr_;
    asynData = new DataType2(dataPtr->getVal());
  }
  ~cpDataType2(){
    craftDbg(4, "cpDataType2::~cpDataType2()");
    delete asynData;
  }

private:

  DataType2 * dataPtr;
  DataType2 * asynData;
  int update(){
    craftDbg(5, "cpDataType2::update");
    asynData->setVal(dataPtr->getVal());
    return EXIT_SUCCESS;
  }
	int write(const std::string filename){
    craftDbg(5, "cpDataType2::write() I write at %s", (filename).c_str());
	  std::ofstream fstr;
      
	  fstr.open ((filename).c_str(), std::ofstream::binary );	
	  if(fstr.is_open()){
      int temp = asynData->getVal();
		  fstr.write( (char *)&temp, sizeof (temp) );
		  fstr.close();
      sync();
	  }
	  else{
	    int myrank;
	    MPI_Comm_rank(cpMpiComm, &myrank);
		  std::cerr << myrank << "Can't open file -- " << filename << std::endl;			
		  return EXIT_FAILURE;
	  }

    return EXIT_SUCCESS;
  }
	int read(const std::string filename){
    craftDbg(5, "cpDataType2::read I read from %s", (filename).c_str());
    std::ifstream fstr;
	  fstr.open ((filename).c_str(), std::ios::in | std::ios::binary);	
    int temp;
	  if(fstr.is_open()){
		  fstr.read( (char *) &temp, sizeof(temp));	
      dataPtr->setVal(temp); 
		  fstr.close();
	  }
	  else{
		  std::cerr << "Can't open file " << filename << std::endl;			
		  return EXIT_FAILURE;
	  }
      return EXIT_SUCCESS;
   } 
};

#endif
