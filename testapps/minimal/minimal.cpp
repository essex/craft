// ==================================================================================== 
//  This code tests the CRAFT POD and POD-array checkpoint functions. 
//  The program make regular checkpoints.
//  At the end of the program run, the POD and POD-array values are 
//  reset and checkpoints are read.
// ====================================================================================
//  mpirun -np 2 ./minimal.bin -niter 29 -cpfreq 10
// ====================================================================================


#include "minimal.h"
#include <craft.h>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;

int main(int argc, char* argv[])
{
  int provided;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  printf("thread support level %d\n", provided);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  int cpFreq =0, nIter=10;
  CRAFT_getEnvVar("CRAFT_cpFreq", &cpFreq);
  CRAFT_getEnvVar("CRAFT_nIter", &nIter);

  int n = 5;
  int myint = 0;
  double mydouble = 0.0123;
  int iteration = 1; 
  
{
  printf("%d: after repair flag -100\n", myrank);

  Checkpoint myCP( "minimalCP", FT_Comm);
  MPI_Barrier(FT_Comm);
  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, BIN"); 
  CRAFT_CHECK_ERROR(  myCP.add("myint", &myint) );
  CRAFT_CHECK_ERROR(  myCP.add("mydouble", &mydouble, mypropMB) );
  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration, mypropMB) );

  printf("%d: after repair flag 00\n", myrank);
  
  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  printf("after repair flag 01\n");

  double t1=0.0, t2=0.0; 
  usleep(1000000);
  CRAFT_CHECK_ERROR( myCP.restartIfNeeded(&iteration));

  CRAFT_getWalltime(&t1);

  for(; iteration <= nIter ; iteration++)
  {
    myint++; mydouble++;
    { printf("%d:=== iter: %d , myint: %d, mydouble: %f \n", myrank, iteration, myint, mydouble);}

    myCP.updateAndWrite( iteration, cpFreq);
    usleep(700000);

    MPI_Barrier(FT_Comm);
  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);

  if(myrank==0) { printf("Last checkpoint vals: \n %d:=== iter: %d , myint: %d, mydouble: %f \n", myrank, iteration, myint, mydouble);}
  
}

#ifdef AFT
    AFT_END()
#endif
    MPI_Finalize();

    return EXIT_SUCCESS;
}


