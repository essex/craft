// ============== 
// USAGE Example:
// ============== 
// ./minimal-CraftExtTest.bin -cpfreq 10 -niter 40
//
// for parallel fault tolerant with dynamic process recovery(AFT) run:
// mpirun -am ft-enable-mpi -np 4 -npernode 2 ./minimal-CraftExtTest.bin -niter 50 -cpfreq 10
//

#include "minimal-CraftExtTest.hpp"

using namespace std;
using namespace cpOpt;

int read_params(int argc, char* argv[] , CpOptions * myCpOpt){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
			if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
		}
	}
  delete[] tmp;
}

int main(int argc, char* argv[])
{

  int provided=-1;
  MPI_Init(&argc, &argv);
//  MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);
  printf("=== 1 === prodvided thread: %d \n", provided);

  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt); 

  int n = 5;
  int myint = 0;
  double mydouble = 0.0123;
  double * myarray   = new double[n];
  for(int i=0; i<n;++i){
    myarray[i]=0.1;
  }
  int iteration = 1; 
  
{
  std::shared_ptr<DataType1> myDT1(new DataType1(2000));    // extension method 1
  DataType2 myDT2(3000);
  int rc;
  Checkpoint myCP( "minimalCP", FT_Comm);
  CRAFT_CHECK_ERROR(myCP.add("myint", &myint));
  CRAFT_CHECK_ERROR(myCP.add("iteration", &iteration));
  CRAFT_CHECK_ERROR(myCP.add("myDT1", myDT1));
  CRAFT_CHECK_ERROR(myCP.add("myDT2", std::make_shared<cpDataType2>(&myDT2, FT_Comm) ));     // extension method 2
//std::shared_ptr<cpDataType2> cpMyDT2(new cpDataType2(&myDT2, FT_Comm));   // alternate extension method 2
//  CRAFT_CHECK_ERROR(myCP.add("myDT2", cpMyDT2));
  CRAFT_CHECK_ERROR(myCP.commit()); 
  
  
  double t1=0.0, t2=0.0; 
  double updatet1=0.0, updatet2=0.0; 
  double writet1=0.0, writet2=0.0; 
  double itert1=0.0, itert2=0.0; 

  if( myCP.needRestart() == true) 
  {
    if(myrank==0) printf("%d:RESTART ------>  true \n", myrank);
    CRAFT_CHECK_ERROR(myCP.read());
    iteration++;
    if(myrank==0) printf("%d:iteration = %d \n", myrank, iteration);
  }


  CRAFT_getWalltime(&t1);

  for(; iteration <= myCpOpt->getnIter() ; iteration++)
  {
    CRAFT_getWalltime(&itert1);
    myint++;
    myDT1->setVal( myDT1->getVal() + 1 );
    myDT2.setVal ( myDT2.getVal() + 1 );
    for(size_t j = 0; j < 4 ; ++j){
      for(size_t i = 0; i < n ; ++i){
          myarray[i] += 1.0;
      }
    }

    usleep(200000);
    if(iteration % myCpOpt->getCpFreq() == 0){
      if(myrank==0){ printf("Checkpointing...\n");}
        CRAFT_getWalltime(&updatet1);
        CRAFT_CHECK_ERROR(myCP.update());
        CRAFT_getWalltime(&updatet2);
        if(myrank==0) printf("update time is : %f\n", updatet2 - updatet1);
        MPI_Barrier(FT_Comm);
//        usleep(1);
        CRAFT_getWalltime(&writet1);
        CRAFT_CHECK_ERROR(myCP.write());
        CRAFT_getWalltime(&writet2);
        if(myrank==0) printf("write time is : %f\n", writet2 - writet1);
        MPI_Barrier(FT_Comm);
    }

    MPI_Barrier(FT_Comm);
    CRAFT_getWalltime(&itert2);
    if(myrank==0) { printf("%d:=== iter: %d , myint: %d \t, myDT1.val: %d, myDT2.val: %d, iter time: %f \n", myrank, iteration, myint-1, myDT1->getVal(), myDT2.getVal(), itert2 -itert1);}
  }
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);
  
}
#ifdef AFT
    AFT_END()
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}




