// ==================================================================================== 
//  This code tests the CRAFT POD and POD-array checkpoint functions. 
//  The program make regular checkpoints.
//  At the end of the program run, the POD and POD-array values are 
//  reset and checkpoints are read.
// ====================================================================================
//  export CRAFT_cpFreq=10
//  export CRAFT_nIter=39
//  mpirun -np 2 ./minimal-complex.bin
// ====================================================================================


#include <craft.h>
#include <cstring>
#include <unistd.h>
#include <mpi.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

using namespace std;

template < typename T>
void printArray( const std::string s, const std::complex<T> * const ptr, const size_t nElem, const MPI_Comm comm=MPI_COMM_WORLD){
  MPI_Barrier(comm);
  int myrank, numprocs; 
	MPI_Comm_rank(comm, &myrank);
	MPI_Comm_size(comm, &numprocs);
  for(int p=0; p<numprocs ; ++p){
    if(myrank == p){
      std::cout << s << " at process "<< p << std::endl; 
      for(size_t i = 0; i < nElem; ++i){
        cout << "elem: " << i << " = (" << ptr[i].real() << ", " << ptr[i].imag() << ")" << std::endl;  
      }
    }
    MPI_Barrier(comm);
    usleep(100000);
  }
	return;
}

template < typename T>
void initArray( std::complex<T> * const ptr, const size_t nElem, const MPI_Comm comm=MPI_COMM_WORLD){
  for ( size_t i=0;i<nElem ; ++i)
  {
    ptr[i] = {i*0.01+0.01, i*0.001+0.001};
  }
  return;
}

int main(int argc, char* argv[])
{
  int provided;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);

  printf("thread support level %d\n", provided);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
// ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  int cpFreq , nIter;
  CRAFT_getEnvVar("CRAFT_cpFreq", &cpFreq);
  CRAFT_getEnvVar("CRAFT_nIter", &nIter);

  int n = 5;
  int iteration = 0; 
  size_t nElemCP = 0;
  size_t nElem = 30; 
  std::complex<float> * cArray = new std::complex<float>[nElem];  

  initArray(cArray, nElem); 
  printArray("cArray", cArray, nElem); 

  std::complex<double> c1 (0.0, 0.00); 
  if(myrank==0)
  {
    cout << "real " << c1.real() << endl;
    cout << "imag " << c1.imag() << endl;
  } 
{
  Checkpoint myCP( "minimal-complex-CP", FT_Comm);
  CpAbleProp mypropSB("SERIALIO, BIN"); 
  CpAbleProp mypropSA("SERIALIO, ASCII"); 
  CpAbleProp mypropMB("MPIIO, BIN"); 

  CRAFT_CHECK_ERROR(  myCP.add("iteration", &iteration, mypropSA) );
//  CRAFT_CHECK_ERROR(  myCP.add("c1", &c1, mypropMB) );
  CRAFT_CHECK_ERROR(  myCP.add("cArray", cArray, &nElemCP, mypropMB) );     // nElemCP could change
  CRAFT_CHECK_ERROR(  myCP.add("nElemCP", &nElemCP, mypropSA) );
  
  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  double t1=0.0, t2=0.0; 

  if(myCP.needRestart())  
  { 
	 if(myrank==0) printf("=========== RESTART PHASE 1 =========== \n" );
    myCP.read("nElemCP");
  }
  if(myCP.needRestart())  
  { 
	 if(myrank==0) printf("=========== RESTART PHASE 2 =========== \n" );
    myCP.read();
    iteration++; 
    nElemCP++;
  }

  CRAFT_getWalltime(&t1);

  for(; iteration < nIter ; iteration++, nElemCP++)
  {
    if(myrank==0) printf("%d/%d ===========\n", iteration, nIter);

/*    {
    c1.real ( c1.real() + 0.1);
    c1.imag ( c1.imag() + 0.01);
    if(myrank==0)
    {
      cout << "real" << c1.real() << std::endl;  
      cout << "imag" << c1.imag() << std::endl;  
    }
    }
*/
    //  ========= MODIFY COMPLEX ARRAY ELEMS ==========
    for(size_t i = 0; i < nElemCP; ++i) 
    {
      cArray[i].real (cArray[i].real()+0.01);
      cArray[i].imag (cArray[i].imag()+0.001);
    }
    if(myrank==0)
    {
      cout << "elem 0:" << " = (" << cArray[0].real() << ", " << cArray[0].imag() << ")" << std::endl;  
    }
    myCP.updateAndWrite(iteration, cpFreq);
    usleep(100000);
    MPI_Barrier(FT_Comm);
  }
  
  CRAFT_getWalltime(&t2);
  if(myrank==0) printf("Total time is : %f\n", t2 - t1);
    MPI_Barrier(FT_Comm);
    printArray("cArray", cArray, nElem); 
  
}

#ifdef AFT
    AFT_END()
#endif
    MPI_Finalize();

    return EXIT_SUCCESS;
}


