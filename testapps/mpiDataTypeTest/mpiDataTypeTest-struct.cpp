/// ==================================================================================== 
//  This code tests the CRAFT checkpoints in the case, when MPI_Type_create_struct 
//  is used to define the data-type. The data-type defined has a static allocation 
//  of memory.
// ====================================================================================
// mpirun -np 4 ./mpiDataTypeTest-struct.bin
//
//
#include <mpi.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <future>
#include <omp.h>

#include <craft.h>
using namespace std;

const int nB = 4;
const int nC = 5;

struct Partstruct;
int initPartstruct(Partstruct * particles, int numParticles, int val=-1);
int printParticle(const std::string s, Partstruct * particle, MPI_Comm comm);
int createMpiPartDataType(Partstruct * particles, MPI_Datatype * mpiParticleDataType, MPI_Comm comm);

struct Partstruct
{
    char a;
    double b[nB];
    int c[nC];
};

int main(int argc, char* argv[])
{
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  printf("thread support level %d\n", provided);
{
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  Checkpoint myCP( "cpMpiDataTypeTest-struct", MPI_COMM_WORLD);

  // ===== define array of particles ===== // 
  int numParticles = 10; 
  Partstruct * particles = new Partstruct[numParticles];
  initPartstruct(particles, numParticles, 99);
  printParticle("== INITIAL PATICLE VALES ==", &(particles[0]), MPI_COMM_WORLD);

  // ===== define derived MPI datatypes ===== // 
  MPI_Datatype mpiParticleDataType;
  createMpiPartDataType(particles, &mpiParticleDataType, MPI_COMM_WORLD);
  
  // ===== adding particles to checkpoint ===== // 
  CRAFT_CHECK_ERROR(  myCP.add("partdatatype-struct", particles, &mpiParticleDataType, numParticles) ) ;
  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  // ===== write CP ===== // 
  CRAFT_CHECK_ERROR(myCP.update());
  CRAFT_CHECK_ERROR(myCP.write());
  CRAFT_CHECK_ERROR(myCP.wait());
  MPI_Barrier(MPI_COMM_WORLD);

  // ===== re-init ===== // 
  initPartstruct(particles, numParticles, 0);
  printParticle("== AFTER INIT AGAIN ==", &(particles[1]), MPI_COMM_WORLD);

  // ===== read CP ===== // 
  CRAFT_CHECK_ERROR(myCP.read());
  printParticle("== AFTER READ AGAIN ==", &(particles[1]), MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);
}
  MPI_Finalize();
  return EXIT_SUCCESS;
}


int initPartstruct(Partstruct * particles, int numParticles, int val){
  for(int i =0; i < numParticles; ++i)
  {
    particles[i].a = 'a';
    for(int j=0; j<nB; ++j){
      if(val==-1){       // default init
        particles[i].b[j] = (double) j;
      }
      else{
        particles[i].b[j] = (double) val;
      }
    }
    for(int j=0; j<nC; ++j){
      if(val==-1){       // default init
        particles[i].c[j] = j;
      }
      else{
        particles[i].c[j] = val;
      }
    }
  }
  return 0;
}

int printParticle(const std::string s, Partstruct * particle, MPI_Comm comm){
  MPI_Barrier(comm);
  craftDbg(1, "%s", s.c_str());
  int myrank, numprocs;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &numprocs);
  for(int i=0; i<numprocs; ++i)
  {
    if(myrank == i){
      printf ("printing particle values\n");
      printf ("a: %c", particle->a);
      printf ("\n");
      for(int j=0; j<nB; ++j){
        printf("b[]: %f\t", particle->b[j]);
      }
      printf ("\n");
      for(int j=0; j<nC; ++j){
        printf("c[]: %d\t", particle->c[j]);
      }
      printf ("\n");
    }
    MPI_Barrier(comm);
    usleep(10000);
  } 
  return 0;
}

int createMpiPartDataType(Partstruct * particle, MPI_Datatype * mpiParticleDataType, MPI_Comm comm)
{
  int myrank, numprocs, ret;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &numprocs);

  MPI_Datatype type[3] = { MPI_CHAR, MPI_DOUBLE, MPI_INT };
  int blocklen[3] = { 1, nB, nC};
  
  MPI_Aint elemAddress[4];
  MPI_Address(particle, &elemAddress[0]);
  MPI_Address(&(particle->a), &elemAddress[1]);
  MPI_Address(particle->b, &elemAddress[2]);
  MPI_Address(particle->c, &elemAddress[3]);
  
  MPI_Aint disp[3];
  disp[0] = elemAddress[1] - elemAddress[0];
  disp[1] = elemAddress[2] - elemAddress[0];
  disp[2] = elemAddress[3] - elemAddress[0];

  ret = MPI_Type_create_struct(3, blocklen, disp, type, mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_create_struct is not successful\n");

  ret = MPI_Type_commit(mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_commit is not successful\n");
  
  int mpiDTSize;
  MPI_Type_size(*mpiParticleDataType, &mpiDTSize);
  craftDbg(1,"createMpiPartDataType: mpiDTSize is %d", mpiDTSize);
 
  return EXIT_SUCCESS;
}
/*
int createMpiPartDataType(Partstruct * particles, MPI_Datatype * mpiParticleDataType)
{
  int i, j, myrank;
  MPI_Status status;
  MPI_Datatype type[3] = { MPI_CHAR, MPI_DOUBLE, MPI_INT };
  int blocklen[3] = { 1, nB, nC};
  MPI_Aint disp[3];
  craftDbg(1,"address is %d", &particles[0]);
 
  disp[0] = (MPI_Aint)&particles[0].a - (MPI_Aint)&particles[0];
  disp[1] = (MPI_Aint)&particles[0].b - (MPI_Aint)&particles[0];
  disp[2] = (MPI_Aint)&particles[0].c - (MPI_Aint)&particles[0];

  craftDbg(1,"disp[0] is %d", disp[0]);
  craftDbg(1,"disp[1] is %d", disp[1]);
  craftDbg(1,"disp[2] is %d", disp[2]);

  int ret; 
  ret = MPI_Type_create_struct(3, blocklen, disp, type, mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_create_struct is not successful\n");

  ret = MPI_Type_commit(mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_commit is not successful\n");
  
  int mpiDTSize;
  MPI_Type_size(*mpiParticleDataType, &mpiDTSize);
  craftDbg(1,"mpiDTSize is %d", mpiDTSize);
 
  return EXIT_SUCCESS;
}
*/
