///// ==================================================================================== 
//  This code tests the CRAFT checkpoints in the case, when MPI_Type_vector
//  is used to define the data-type. The data-type defined has a dynamic allocation 
//  of memory.
// ====================================================================================
// mpirun -np 4 ./mpiDataTypeTest-dyn-vector.bin
//
//
#include <mpi.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <future>
#include <omp.h>

#include <craft.h>

#include "mpiDataTypeTest_helpers.hpp"

using namespace std;
using namespace cpOpt;

int read_params(int argc, char* argv[] , CpOptions * myCpOpt);

int main(int argc, char* argv[])
{

  int provided; 
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);         // ASYNC write requires MPI_THREAD_MULTIPLE support
  printf("thread support level %d\n", provided);
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);


  int numGhostElems=2;
  int X_DIM_BASE=5; 
  int Y_DIM_BASE=5;
  int xDim=(X_DIM_BASE+myrank),yDim=Y_DIM_BASE+myrank;
  int * grid = new int [xDim*yDim];
  initGrid(grid, xDim , yDim, (myrank+1));
  printGridAllProc("After Init", grid, xDim , yDim, MPI_COMM_WORLD);
 
  Checkpoint myCP( "cpMpiDataTypeTest-vector", MPI_COMM_WORLD);
  MPI_Datatype boundaryEWType, boundaryNSType;
  defineDerivedDataType( yDim, numGhostElems, xDim, MPI_INT, &boundaryEWType);
  defineDerivedDataType( numGhostElems, xDim, xDim, MPI_INT, &boundaryNSType);
  
  // ===== adding boundary elements to checkpoints ===== // 
  CRAFT_CHECK_ERROR(  myCP.add("WestBoundary",  grid                            , &boundaryEWType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("EastBoundary",  grid+(xDim-numGhostElems)       , &boundaryEWType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("NorthBoundary", grid                            , &boundaryNSType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("SouthBoundary", grid+((yDim-numGhostElems)*xDim), &boundaryNSType) ); 
  CRAFT_CHECK_ERROR(  myCP.commit() ); 


  CRAFT_CHECK_ERROR(myCP.update());
  CRAFT_CHECK_ERROR(myCP.write());
  CRAFT_CHECK_ERROR(myCP.wait());

  initGrid(grid, xDim , yDim, 0);
  
  printGridAllProc("After Init again ", grid, xDim , yDim, MPI_COMM_WORLD);
  CRAFT_CHECK_ERROR(myCP.read());
  MPI_Barrier(MPI_COMM_WORLD);
  craftDbg(4,"============== AFTER READ ============== "); 

  printGridAllProc("After Read", grid, xDim , yDim, MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return EXIT_SUCCESS;
}

