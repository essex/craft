//// ==================================================================================== 
//  This code tests the CRAFT checkpoints in the case, when MPI_Type_vector
//  is used to define the data-type. The data-type defined has a static allocation 
//  of memory.
// ====================================================================================
// mpirun -np 2 ./mpiDataTypeTest-vector.bin
//
#include <mpi.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <future>
#include <omp.h>

#include <craft.h>

#include "mpiDataTypeTest_helpers.hpp"

using namespace std;
using namespace cpOpt;

int communicateGridGhost(int * grid, const int xDim, const int yDim, const int numGhostElems, const MPI_Datatype boundaryEWType, const MPI_Datatype boundaryNSType);

int main(int argc, char* argv[])
{

  int provided, myrank, numprocs;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);         // ASYNC write requires MPI_THREAD_MULTIPLE support
  printf("thread support level %d\n", provided);
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  if ( numprocs != 2 ) {printf("This program requires to be run with only 2 processes\n"); return 0;}

  int xDim=10,yDim=10, numGhostElems=2;
  int * grid = new int [xDim*yDim];
  if(myrank ==0){ 
    initGrid(grid, xDim , yDim);
  }
  if(myrank ==1){ 
    initGrid(grid, xDim , yDim, 0);
  }

  Checkpoint myCP( "cpMpiDataTypeTest-vector", MPI_COMM_WORLD);

  // ===== define derived MPI datatypes ===== // 
  MPI_Datatype boundaryEWType, boundaryNSType;
  defineDerivedDataType( yDim, numGhostElems, xDim, MPI_INT, &boundaryEWType);
  defineDerivedDataType( numGhostElems, xDim, xDim, MPI_INT, &boundaryNSType);

  // ===== communicate boundary elements of 0 to 1 process ===== // 
  printGridAllProc("Before Communication ", grid, xDim , yDim, MPI_COMM_WORLD);
  communicateGridGhost(grid, xDim, yDim, numGhostElems, boundaryEWType, boundaryNSType); 
  printGridAllProc("After Communication ", grid, xDim , yDim, MPI_COMM_WORLD);
  
  // ===== adding boundary elements to checkpoints ===== // 
  CRAFT_CHECK_ERROR(  myCP.add("WestBoundary",  grid                  , &boundaryEWType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("EastBoundary",  grid+(xDim-numGhostElems)        , &boundaryEWType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("NorthBoundary", grid                  , &boundaryNSType) ); 
  CRAFT_CHECK_ERROR(  myCP.add("SouthBoundary", grid+((yDim-numGhostElems)*xDim), &boundaryNSType) ); 
  CRAFT_CHECK_ERROR(  myCP.commit() ); 

  // ===== Write checkpoint ===== // 
  CRAFT_CHECK_ERROR(myCP.update());
  CRAFT_CHECK_ERROR(myCP.write());
  CRAFT_CHECK_ERROR(myCP.wait());

  // ===== re-inint grid from base ===== // 
  initGrid(grid, xDim , yDim, 0);
  printGridAllProc("After Init again ", grid, xDim , yDim, MPI_COMM_WORLD);
  
  // ===== read and print grid ===== // 
  CRAFT_CHECK_ERROR(myCP.read());
  printGridAllProc("After Read", grid, xDim , yDim, MPI_COMM_WORLD);


  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  return EXIT_SUCCESS;
}


int communicateGridGhost(int * grid, const int xDim, const int yDim, const int numGhostElems, const MPI_Datatype boundaryEWType, const MPI_Datatype boundaryNSType){
  int numprocs, myrank; 
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  MPI_Status status;
  if (myrank == 0)
  {
    MPI_Send(grid, 1, boundaryEWType, 1, 123, MPI_COMM_WORLD);                              // WEST Boundary
    MPI_Send(grid+(xDim-numGhostElems), 1, boundaryEWType, 1, 124, MPI_COMM_WORLD);                    // EAST Boundary
    MPI_Send(grid, 1, boundaryNSType, 1, 125, MPI_COMM_WORLD);                              // NORTH Boundary
    MPI_Send(grid+((yDim-numGhostElems)*xDim), 1, boundaryNSType, 1, 126, MPI_COMM_WORLD);            // SOUTH Boundary
  }

  if (myrank == 1)
  {
    MPI_Recv(grid, 1, boundaryEWType, 0, 123, MPI_COMM_WORLD, &status);                     // WEST Boundary
    MPI_Recv(grid+(xDim-numGhostElems), 1, boundaryEWType, 0, 124, MPI_COMM_WORLD, &status);           // EAST Boundary
    MPI_Recv(grid, 1, boundaryNSType, 0, 125, MPI_COMM_WORLD, &status);                     // NORTH Boundary
    MPI_Recv(grid+((yDim-numGhostElems)*xDim), 1, boundaryNSType, 0, 126, MPI_COMM_WORLD, &status);   // SOUTH Boundary
  }
  return 0;
}

