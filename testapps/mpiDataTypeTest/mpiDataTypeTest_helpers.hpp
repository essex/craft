int initGrid(int * grid, const int xDim , const int yDim, const int val=-1);
int printGrid(const int * const grid, const int xDim , const int yDim);
int printGridAllProc(const std::string s, const int * const grid, const int xDim ,const  int yDim, const MPI_Comm comm);
int defineDerivedDataType( int blockCount, int blocklength, int stride, MPI_Datatype oldtype, MPI_Datatype* newtype);


int defineDerivedDataType( int blockCount, int blocklength, int stride, MPI_Datatype oldtype, MPI_Datatype* newtype){
  MPI_Type_vector(blockCount, blocklength, stride, oldtype, newtype);
  MPI_Type_commit(newtype);
  return 0;
}

int initGrid(int * grid, const int xDim , const int yDim, const int val)
{
  int numprocs, myrank; 
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  for(int j=0; j<yDim; ++j){
    for(int i=0; i<xDim; ++i){
      if(val == -1){      // initialize by grid position values
        grid[j*xDim+i] = (j*xDim+i);
      }
      else{
        grid[j*xDim+i] = val;
      }
    }
  }
  return 0;
}

int printGrid(const int * const grid, const int xDim , const int yDim)
{
  for(int j=0; j<yDim; ++j){
    for(int i=0; i<xDim; ++i){
      printf ("%d\t", grid[j*xDim+i]);
    }
    printf("\n");
  }
  return 0;
}

int printGridAllProc(const std::string s, const int * const grid, const int xDim ,const  int yDim, const MPI_Comm comm)
{
  int numprocs, myrank; 
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Barrier(comm);
  craftDbg(1, "%s", s.c_str());
  MPI_Barrier(comm);
  for (int p=0; p<numprocs; ++p){
    if(p==myrank){
      printf("grid for proc: %d\n", p);
      printGrid(grid, xDim, yDim);
    }
    MPI_Barrier(comm);
    usleep(50000);
  }
  return 0;
}


