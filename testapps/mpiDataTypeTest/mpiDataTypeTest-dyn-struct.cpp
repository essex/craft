// ==================================================================================== 
//  This code tests the CRAFT checkpoints in the case, when MPI_Type_create_struct 
//  is used to define the data-type. The data-type defined has a dynamic allocation 
//  of memory.
// ====================================================================================
// mpirun -np 4 ./mpiDataTypeTest-dyn-struct.bin
//
//
#include <mpi.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <future>
#include <omp.h>
#include <stddef.h>

#include <craft.h>

using namespace std;
using namespace cpOpt;

class Partstruct;
int createMpiPartDataType(Partstruct * particle, MPI_Datatype * mpiParticleDataType, MPI_Comm comm);
int printParticle(const std::string s, Partstruct * particle, MPI_Comm comm);

class Partstruct
{
  public:
    Partstruct(const char a_, const int nB_, const int nC_){
      a   = a_;
      nB  = nB_;
      nC  = nC_;
      b   = new int[nB];
      c   = new double[nC];
    }
    ~Partstruct(){
      delete[] b;
      delete[] c;
    }
    int nB, nC;
    char a;
    int * b;
    double * c;
};


int initPartstruct(Partstruct * particle, int val=-1){
  for(int j=0; j<(particle->nB); ++j){
    if(val==-1){       // default init
      particle->b[j] = (int) j;
    }
    else{
      particle->b[j] = (int) val;
    }
  }
  for(int j=0; j<(particle->nC); ++j){
    if(val==-1){       // default init
      particle->c[j] = (double) j;
    }
    else{
      particle->c[j] = (double) val;
    }
  }
  return 0;
}

int main(int argc, char* argv[])
{
  int provided;
  MPI_Init_thread(&argc, &argv, MPI_THREAD_MULTIPLE, &provided);
  printf("thread support level %d\n", provided);
{
  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);

  Checkpoint myCP( "cpMpiDataTypeTest-struct", MPI_COMM_WORLD);

  int nB_Base=1, nC_Base=1;
  int offset = myrank;
  int nB = nB_Base+offset, nC = nC_Base+offset;

  // ===== define particle ===== // 
  Partstruct *p = new Partstruct('x', nB, nC); 
  initPartstruct(p, 9);
  printParticle("== INITIAL PATICLE VALES ==", p, MPI_COMM_WORLD);

  // ===== define derived MPI datatypes ===== // 
  craftDbg(1,"== CREATING MPI-DATATYPE ==");
  MPI_Datatype mpiParticleDataType;
  createMpiPartDataType( p, &mpiParticleDataType, MPI_COMM_WORLD);
 
  // ===== adding particles to checkpoint ===== // 
  CRAFT_CHECK_ERROR(  myCP.add("partdatatype-struct", p, &mpiParticleDataType) ) ;
  CRAFT_CHECK_ERROR(  myCP.commit() ); 
  MPI_Barrier(MPI_COMM_WORLD);

  // ===== wirte CP ===== // 
  CRAFT_CHECK_ERROR(myCP.update());
  CRAFT_CHECK_ERROR(myCP.write());
  CRAFT_CHECK_ERROR(myCP.wait());
  MPI_Barrier(MPI_COMM_WORLD);

  // ===== re-init ===== // 
  initPartstruct(p, 0);
  printParticle("== AFTER INIT AGAIN ==", p, MPI_COMM_WORLD);

  // ===== read CP ===== // 
  CRAFT_CHECK_ERROR(myCP.read());
  printParticle("== AFTER READ AGAIN ==", p, MPI_COMM_WORLD);

  MPI_Barrier(MPI_COMM_WORLD);
}
    MPI_Finalize();
    return EXIT_SUCCESS;
}

int printParticle(const std::string s, Partstruct * particle, MPI_Comm comm){
  MPI_Barrier(comm);
  craftDbg(1, "%s", s.c_str());
  int myrank, numprocs;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &numprocs);
  for(int i=0; i<numprocs; ++i)
  {
    if(myrank == i){
      printf ("printing particle values\n");
      printf ("a: %c", particle->a);
      printf ("\n");
      for(int j=0; j<(particle->nB); ++j){
        printf("b[]: %d\t", particle->b[j]);
      }
      printf ("\n");
      for(int j=0; j<(particle->nC); ++j){
        printf("c[]: %f\t", particle->c[j]);
      }
      printf ("\n");
    }
    MPI_Barrier(comm);
  } 
  return 0;
}

int createMpiPartDataType(Partstruct * particle, MPI_Datatype * mpiParticleDataType, MPI_Comm comm)
{
  int myrank, numprocs, ret;
  MPI_Comm_rank(comm, &myrank);
  MPI_Comm_size(comm, &numprocs);

  MPI_Datatype type[3] = { MPI_CHAR, MPI_INT, MPI_DOUBLE };
  int blocklen[3] = { 1, (particle->nB), (particle->nC)};
  
  MPI_Aint elemAddress[4];
  MPI_Address(particle, &elemAddress[0]);
  MPI_Address(&(particle->a), &elemAddress[1]);
  MPI_Address(particle->b, &elemAddress[2]);
  MPI_Address(particle->c, &elemAddress[3]);
  
  MPI_Aint disp[3];
  disp[0] = elemAddress[1] - elemAddress[0];
  disp[1] = elemAddress[2] - elemAddress[0];
  disp[2] = elemAddress[3] - elemAddress[0];

//  craftDbg(1,"== Displacement VAL with MPI_ADDRESS is ==");
//  craftDbg(1,"b[0] = %d, c[0] = %f ", particle->b[0], particle->c[0]);
//  craftDbg(1,"dispp[0] is %d", dispp[0]);
//  craftDbg(1,"dispp[1] is %d", dispp[1]);
//  craftDbg(1,"dispp[2] is %d", dispp[2]);

  ret = MPI_Type_create_struct(3, blocklen, disp, type, mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_create_struct is not successful\n");

  ret = MPI_Type_commit(mpiParticleDataType);
  if(ret != MPI_SUCCESS) craftDbg(1, "MPI_Type_commit is not successful\n");
  
  int mpiDTSize;
  MPI_Type_size(*mpiParticleDataType, &mpiDTSize);
  craftDbg(1,"createMpiPartDataType: mpiDTSize is %d", mpiDTSize);
 
  return EXIT_SUCCESS;
}
