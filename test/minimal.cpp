// ============== 
// USAGE Example:
// ============== 
// ./minimal.bin -cpfreq 10 -niter 40
//
// for parallel fault tolerant with dynamic process recovery(AFT) run:
// mpirun -am ft-enable-mpi -np 4 -npernode 2 ./minimal.bin -niter 50 -cpfreq 10
//
#include <mpi.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

#include <craft.h>
using namespace std;
using namespace cpOpt;


int read_params(int argc, char* argv[] , CpOptions * myCpOpt){
	char * tmp = new char[256];
  int myrank=-1;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
	//=========== Reading commnad line arguments with flags ===============//
	for (int i = 1; i < argc; ++i) {
		if ((!strcmp(argv[i], "-niter"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setnIter( atoi(tmp) );
			if(myrank==0) std::cout << "nIter " << myCpOpt->getnIter() << std::endl;
		}
		if ((!strcmp(argv[i], "-cpfreq"))) {
			sprintf(tmp, "%s" ,argv[++i]);
			myCpOpt->setCpFreq( atoi(tmp) );
      if(myrank==0) std::cout << "cpfreq " << myCpOpt->getCpFreq() << std::endl;
    }
  }
  delete[] tmp;
}

int main(int argc, char* argv[])
{

  MPI_Init(&argc, &argv);
  //  MPI_Errhandler_set(MPI_COMM_WORLD, MPI_ERRORS_RETURN);

  int myrank, numprocs;
  MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
  MPI_Comm FT_Comm;
  MPI_Comm_dup(MPI_COMM_WORLD, &FT_Comm);
  // ===== AFT-Zone BEGIN ==== //

#ifdef AFT
  printf("=== AFT is true in app. === \n");
  AFT_BEGIN(FT_Comm, &myrank, argv);    
#endif 

  CpOptions * myCpOpt = new CpOptions[1];
  read_params(argc, argv, myCpOpt); 

  size_t n = 5;
  int myint = 0;
  double mydouble = 0.0123;
  double * myarray   = new double[n];
  for(int i=0; i<n;++i){
    myarray[i]=0.1;
  }

  int iteration = 1; 
  {
    int rc;
    Checkpoint myCP( "minimalCP", FT_Comm);
    CpAbleProp mypropSB("SERIALIO, BIN"); 
    CpAbleProp mypropSA("SERIALIO, ASCII"); 
    CpAbleProp mypropMB("MPIIO, BIN"); 

    CRAFT_CHECK_ERROR(myCP.add("myint", &myint));
    CRAFT_CHECK_ERROR(myCP.add("mydouble", &mydouble));
    CRAFT_CHECK_ERROR(myCP.add("iteration", &iteration));
    CRAFT_CHECK_ERROR(myCP.add("myarray", myarray, &n, mypropSB));
    CRAFT_CHECK_ERROR(myCP.commit()); 

/*  NOTE: switched off for make-test
    if( myCP.needRestart() == true) 
    {
      if(myrank==0) printf("%d:RESTART ------>  true \n", myrank);
      CRAFT_CHECK_ERROR(myCP.read());
      iteration++;
      if(myrank==0) printf("%d:iteration = %d \n", myrank, iteration);
    }
*/
    for(; iteration <= myCpOpt->getnIter() ; iteration++)
    {
      myint++;
      mydouble++;
      for(size_t j = 0; j < 4 ; ++j){
        for(size_t i = 0; i < n ; ++i){
          myarray[i] += 1.0;
        }
      }

      myCP.updateAndWrite(iteration, myCpOpt->getCpFreq());
      MPI_Barrier(FT_Comm);
      if(myrank==0) { printf("%d:=== iter: %d , myint: %d \t \n", myrank, iteration, myint-1);}
      usleep(300000);
    }
  
    CRAFT_CHECK_ERROR(myCP.read()); 
  }

#ifdef AFT
    AFT_END()
#endif

    MPI_Finalize();
    return EXIT_SUCCESS;
}


