#ifndef __MINIMAL_CRAFTEXTEXAMPLE_HPP__
#define __MINIMAL_CRAFTEXTEXAMPLE_HPP__
#include <craft.h>
#include <cstring>
#include <unistd.h>

#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <future>
#include <omp.h>

//===== Varibale at users disposal ===== 
//  if ( craftMakeAsyncCopy         == true)                  // tells if the environment variable is set to true or false.
//  if ( cpAbleProp.getIOType()     == MPIIO)                	// Parallel PFS IO
//  if ( cpAbleProp.getIOType()     == SERIALIO )						  // either in case of SCR / SERIAL IO: individual PFS checkpoints
//  if ( cpAbleProp.getFileFormat() == BIN)                   // write in Binary format.  default = BIN
//  if ( cpAbleProp.getFileFormat() == ASCII)                 // write in ASCII format.  default = BIN
// NOTE: to benefit from these checks, user must initialize the CpBase using cpAbleProp object. Which can be done in the initializer of the extention checkpointable objects. See how the constructor of DataType1 is changed.
// cpMpiComm

// ====================== CRAFT EXTENSION METHOD 2 ===================== //
class DataType1: public CpBase            // The case where original class could be changed.
{
public:
    DataType1(
              int a=1, 
              CpAbleProp cpAbleProp_=CpAbleProp()
             )
              :a_(a),
               CpBase(cpAbleProp_)
    {
      craftDbg(4, "DataType1::DataType1()");
      if( craftMakeAsyncCopy == true){
	      craftDbg(4, "DataType1: craftMakeAsyncCopy is true");
        writePtr = &asynData;
      }
      else {
        writePtr = &a_;
      }
      MPI_Comm_rank(cpMpiComm, &myrank);
      os = myrank * sizeof(int);   // writePtr;
    }
    ~DataType1(){
      craftDbg(4, "DataType1::~DataType1()");
    }
    int getVal(){ return a_; }
    int setVal(int val)
    { 
      a_ = val; 
      return 0; 
    }

private:
  int a_;
  int asynData;
  int * writePtr;

  MPI_Offset os; 
  int myrank;

  int update(){
    if( craftMakeAsyncCopy == true){
      craftDbg(5, "cpDataType1::update()");
      asynData = a_;
    }
    else 
    {
      craftDbg(5, "cpDataType1::update() is not needed");
    }
    return EXIT_SUCCESS;
  }
	int write(const std::string filename){  // each process has same filename for MPIIO, different filename for SERIALIO
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      assert (cpAbleProp.getFileFormat() == BIN);
      craftDbg(5, "DataType1::write() with MPIIO: I write at %s", (filename).c_str());
      
      CRAFT_MPI_File_write_POD(filename, writePtr, os, cpMpiComm); 
      return EXIT_SUCCESS;
    }
    else      // SERIAL-IO 
    {
      if(cpAbleProp.getFileFormat() == BIN)
      { 
        craftDbg(5, "DataType1::write() with SERIALIO, BIN I write at %s", (filename).c_str());
        CRAFT_SERIAL_BIN_File_write_POD(filename, writePtr); 
        return EXIT_SUCCESS;
      }
      if(cpAbleProp.getFileFormat() == ASCII)
      {
        craftDbg(5, "DataType1::write() with SERIALIO, ASCII I write at %s", (filename).c_str());
        CRAFT_SERIAL_ASCII_File_write_POD(filename, writePtr); 
        return EXIT_SUCCESS;
      }
    }
    return EXIT_FAILURE;
  }

	int read(const std::string filename){
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      assert (cpAbleProp.getFileFormat() == BIN);
      craftDbg(5, "DataType1::read() with MPIIO: I read at %s", (filename).c_str());
      CRAFT_MPI_File_read_POD(filename, &a_, os, cpMpiComm); 
      return EXIT_SUCCESS;
    }
    else      // SERIAL-IO 
    {
      if(cpAbleProp.getFileFormat() == BIN)
      { 
        craftDbg(5, "DataType1::read() with SERIALIO, BIN I read at %s", (filename).c_str());
        CRAFT_SERIAL_BIN_File_read_POD(filename, &a_); 
        return EXIT_SUCCESS;
      }
      if(cpAbleProp.getFileFormat() == ASCII)
      {
        craftDbg(5, "DataType1::read() with SERIALIO, ASCII I read at %s", (filename).c_str());
        CRAFT_SERIAL_ASCII_File_read_POD(filename, &a_); 
        return EXIT_SUCCESS;
      }
    }
    return EXIT_FAILURE;
  } 
};

// ====================== CRAFT EXTENSION METHOD 3 ===================== //
class DataType2                        // The case where original class could NOT be changed.
{
public:
  DataType2(int a=300){ a_=a;
    craftDbg(4, "DataType2::DataType2()");
  } 

  ~DataType2(){
    craftDbg(4, "DataType2::~DataType2");
  }
  int getVal(){ return a_; }
  int setVal(int val)
  { 
    a_ = val; 
    return 0; 
  }
private:
  int a_;
};

class cpDataType2: public CpBase{
public:
  cpDataType2( DataType2 * const dataPtr_, 
              const MPI_Comm cpMpiComm_, 
              CpAbleProp cpAbleProp_=CpAbleProp() 
             ): dataPtr (dataPtr_),
                CpBase  (cpAbleProp_)
  {
    craftDbg(4, "cpDataType2::cpDataType2()");
    if( craftMakeAsyncCopy == true){
	    craftDbg(4, "DataType2: craftMakeAsyncCopy is true");
      asynData = new DataType2(dataPtr->getVal());
      writePtr = asynData;
    }
    else {
      writePtr = dataPtr;
    }

    MPI_Comm_rank(cpMpiComm, &myrank);
    os = myrank * sizeof(int);   // writePtr;

  }
  ~cpDataType2(){
    craftDbg(4, "cpDataType2::~cpDataType2()");
    delete asynData;
  }

private:

  DataType2 * dataPtr;
  DataType2 * asynData;
  DataType2 * writePtr;

  MPI_Offset os; 
  int myrank;

  int update(){
    if( craftMakeAsyncCopy == true){
      craftDbg(5, "cpDataType2::update()");
      asynData->setVal(dataPtr->getVal());
    }
    else 
    {
      craftDbg(5, "cpDataType2::update() is not needed");
    }
    return EXIT_SUCCESS;
  }

	int write(const std::string filename){

    int temp = asynData->getVal();                                                  // get the data from the original object

    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      assert (cpAbleProp.getFileFormat() == BIN);
      craftDbg(5, "DataType2::write() with MPIIO: I write at %s", (filename).c_str());
      CRAFT_MPI_File_write_POD(filename, &temp, os, cpMpiComm); 
      return EXIT_SUCCESS;
    }
    else      // SERIAL-IO 
    {
      if(cpAbleProp.getFileFormat() == BIN)
      { 
        craftDbg(5, "DataType2::write() with SERIALIO, BIN I write at %s", (filename).c_str());
        CRAFT_SERIAL_BIN_File_write_POD(filename, &temp); 
        return EXIT_SUCCESS;
      }
      if(cpAbleProp.getFileFormat() == ASCII)
      {
        craftDbg(5, "DataType2::write() with SERIALIO, ASCII I write at %s", (filename).c_str());
        CRAFT_SERIAL_ASCII_File_write_POD(filename, &temp); 
        return EXIT_SUCCESS;
      }
    }
    return EXIT_FAILURE;
  }

	int read(const std::string filename){
    int temp;
    if(cpAbleProp.getIOType() == MPIIO)                                           	// Parallel PFS IO
    {
      assert (cpAbleProp.getFileFormat() == BIN);
      craftDbg(5, "DataType2::read() with MPIIO: I read at %s", (filename).c_str());
      CRAFT_MPI_File_read_POD(filename, &temp, os, cpMpiComm); 
      dataPtr->setVal(temp); 
      return EXIT_SUCCESS;
    }
    else      // SERIAL-IO 
    {
      if(cpAbleProp.getFileFormat() == BIN)
      { 
        craftDbg(5, "DataType2::read() with SERIALIO, BIN I read at %s", (filename).c_str());
        CRAFT_SERIAL_BIN_File_read_POD(filename, &temp); 
        dataPtr->setVal(temp); 
        return EXIT_SUCCESS;
      }
      if(cpAbleProp.getFileFormat() == ASCII)
      {
        craftDbg(5, "DataType2::read() with SERIALIO, ASCII I read at %s", (filename).c_str());
        CRAFT_SERIAL_ASCII_File_read_POD(filename, &temp); 
        dataPtr->setVal(temp); 
        return EXIT_SUCCESS;
      }
    }
    return EXIT_FAILURE;
  }

};

#endif
