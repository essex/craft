#! /bin/bash

sed -i '/SCR_Init()/iMPI_Comm mcwCopy;\nMPI_Comm_dup(MPI_COMM_WORLD, &mcwCopy);' ./src/scr_have_restart.c
sed -i 's/SCR_Init()/SCR_Init(\&mcwCopy)/g' ./src/scr_have_restart.c

sed -i '/SCR_Init()/iMPI_Comm mcwCopy;\nMPI_Comm_dup(MPI_COMM_WORLD, &mcwCopy);' ./examples/*
sed -i 's/SCR_Init()/SCR_Init(\&mcwCopy)/g' ./examples/*

sed -i '/SCR_Init()/iMPI_Comm mcwCopy;\nMPI_Comm_dup(MPI_COMM_WORLD, &mcwCopy);' ./src/scrf.c
sed -i 's/SCR_Init()/SCR_Init(\&mcwCopy)/g' ./src/scrf.c

sed -i 's/SCR_Init(void)/SCR_Init(MPI_Comm * comm)/g' ./src/scr.h
sed -i '/SCR_Init/i#include <mpi.h>' ./src/scr.h
sed -i 's/SCR_Init()/SCR_Init(MPI_Comm * comm)/g' ./src/scr.c

sed -i '/MPI_Comm_dup(MPI_COMM_WORLD/d' src/scr.c
sed -i '/MPI_Comm_rank(scr_comm_world, &scr_my_rank_world);/iMPI_Comm_dup(* comm,  &scr_comm_world);\nMPI_Comm_set_errhandler( scr_comm_world, MPI_ERRORS_RETURN );' src/scr.c
