#!/bin/bash
if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters. USAGE: signalForCP -<which-mpi>"
    exit
fi

#In case of intelmpi
if [ "$1" == "-intelmpi" ]; then
  echo "Application will be signaled as an IntelMPI compiled application"
  MPIPID=`pgrep mpiexec`  # even if job is started from mpirun, the signal should be send to mpiexec
  echo "MPIPID is $MPIPID"
  kill -s SIGALRM $MPIPID
fi

#In case of Openmpi. 
if [ "$1" == "-openmpi" ]; then
  echo "Application will be signaled as an OpenMPI compiled application"
  MPIPID=`pgrep mpi`      # signal could either be send to mpiexec or mpirun, both have the same affect.
  echo "MPIPID is $MPIPID"
  kill -s SIGALRM $MPIPID
fi

# TODO: There will be a slight-bug, if the user has an intel compiled code. and gives '-openmpi' flag. 
#       When intelMPI compiled application is run with mpirun, there runs mpirun and mpiexec.hydra both at the same time. Thus, -openmpi will cause the signal to mpirun, which should not be the case.
#       Perhaps an automatic detection of compiler is useful.

